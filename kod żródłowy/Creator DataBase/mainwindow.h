#ifndef MAINWINDOW_H
#define MAINWINDOW_H
#include <QMainWindow>
#include <QJsonDocument>
#include <QFileDialog>
#include <memory>
#include "database.h"
#include "card.h"
#include "jnsonshortedition.h"



namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    enum SaveFormat {
           Json, Binary
       };
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();
    QString filenameJSON;
    QString filenameDB;

private slots:

    void on_buttonCreate_clicked();
    void on_buttonDB_clicked();
    void on_buttonJSON_clicked();

private:
    Ui::MainWindow *ui;
};

#endif // MAINWINDOW_H
