#ifndef JNSONSHORTEDITION_H
#define JNSONSHORTEDITION_H
#include <QJsonObject>
#include "card.h"
#include <vector>
/*!
 *  @brief Klasa reprezentuje pojecynacza edycje wraz z informacjami o niej, oraz posiada vector kart należących do edycji.
 */
class Edition
{
public:
    Edition(QJsonObject obj);
    void setObject(QJsonObject obj);
    QString getName() const {return this->name ;}


    QString getCode() const {return this->code ;}
    QString getGatherCode() const {return this->gatherCode ;}
    QString getOldCode() const {return this->oldCode ;}
    QString getReleaseDate() const {return this->releaseDate ;}
    QString getBorder() const {return this->border;}
    QString getType() const {return this->type ;}
    QString getBlock() const {return this->block ;}
    bool getOnlineOnly() const {return this->onlineOnly ;}
    Card getCard(int number) const {return this->cards.at(number) ;}
    int getQuantityCards(){return cards.size();}
        QString name;
private:

    QString code;
    QString gatherCode;
    QString oldCode;
    QString releaseDate;
    QString border;
    QString type;
    QString block;
    bool onlineOnly ;
    std::vector<Card> cards;

};

#endif // JNSONSHORTEDITION_H
