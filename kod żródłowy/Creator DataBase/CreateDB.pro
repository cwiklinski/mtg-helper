#-------------------------------------------------
#
# Project created by QtCreator 2014-05-22T22:02:51
#
#-------------------------------------------------

QT       += core gui sql

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = CreateDB
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    database.cpp \
    jnsonshortedition.cpp \
    card.cpp

HEADERS  += mainwindow.h \
    database.h \
    jnsonshortedition.h \
    card.h

FORMS    += mainwindow.ui
QMAKE_CXXFLAGS += -std=gnu++0x
