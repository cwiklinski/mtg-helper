#ifndef DATABASE_H
#define DATABASE_H
#include <QSqlDatabase>
#include<QDebug>
#include<QMessageBox>
#include<QSqlQuery>

/**
 * @brief Klasa służąca do komunikacji z baza danych oraz inicjalizacja rekordow
 *
 */
class DataBase
{
public:
    /**
     * @brief Kontruktor ustawiajacy parametry polaczenia z baza danych
     *
     * @param file Lokalizacja bazy danych.
     */
    DataBase(QString file);
     QSqlDatabase *db; /** Wskaźnik na polączenei z bazą danych obiektu. */
     /**
      * @brief Funkcja sprawdzająca czy ilość kart i edycji w strukrzurze jest identyczna z iloscia rekordow z bazy danych
      *
      * @param qEdition   Pożądana ilosc rekodrow w tabeli editions
      * @param qCollection Pożądana ilosc rekodrow w tabeli allCards
      * @return bool false - gdy ilość danych nie jest identyczne. True jeśli jest
      */
     bool cheack(int qEdition,int  qCollection);
     /**
      * @brief
      * Tworzy tabele: rarity, vat, editions, boxCard, allCard, shopCard, BoxList
      *
      */
     void createTable();
     /**
      * @brief
      * Wsadza pojedynczy rekord do bazy danych
      * @param name  Imie edycji (np. 5E).
      * @param longname  Długie imie edycji (np fifty editions).
      */
     void insertEditionToDb(QString name, QString longname, QString releaseDate);
     /**
      * @brief  Wypelnia tabele:
      *     boxList << box1, box2,box3
      *     vat << 23,7,8
      *     rarity<< NULL, common, uncommon, rare
      *
      */
     void insertDefaultToDb();
     /**
      * @brief Wrzuca do bazy danych pojednycza karte
      *
      * @param name
      * @param Edition
      * @param manacost
      * @param type
      * @param pt
      * @param idRarity
      * @param picURL
      * @param text
      */
     void insertCardToDb(QString name,QString Edition,QString manacost,QString type,QString pt,QString rarityName, QString picURL,QString text, QString artist);
     /**
      * @brief Bezpiecze otwiera baza danych.
      *
      */
     void openDB();
     /**
      * @brief Bezpiecznie zamyka baze danych
      *
      */
     void closeDB();
private:
     QString fileDB; /** bref Ścieżka do pliku zawierającego baze danych */
     QString getIdRarity(QString rarityName);

};

#endif // DATABASE_H
