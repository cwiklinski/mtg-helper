#include "card.h"
#include <QJsonObject>
Card::Card()
{
}
void Card::read(const QJsonObject &json)
{
    name=json["name"].toString();
    manaCost=json["manaCost"].toString();
    type =json["type"].toString();
    rarity =json["rarity"].toString();
    text =json["text"].toString();
    power =json["power"].toString();
    toughness =json["toughness"].toString();
    layout =json["layout"].toString();
    artist =json["artist"].toString();
    border=json["border"].toString();
}
