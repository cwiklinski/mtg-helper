#include "database.h"

DataBase::DataBase(QString file)
{
    this->fileDB = file;
}
void DataBase::openDB( ){



    db = new QSqlDatabase();

    *db = QSqlDatabase::addDatabase("QSQLITE", QLatin1String( "OneCard-connection" ));
    db->setDatabaseName(fileDB);
    if (!db->open()) {

        QMessageBox msgBox;
        msgBox.setText("ERROR: Cannot open Database");
        msgBox.exec();
    }


}
void  DataBase::closeDB(){


    QString connection;
    connection=db->connectionName();


    db->close();
    *db = QSqlDatabase();
    QSqlDatabase::database().removeDatabase(connection);


}

void DataBase::createTable(){

    this->db->exec("PRAGMA synchronous = OFF ;");
    this->db->exec("PRAGMA journal_mode = MEMORY;");
    this->db->exec("CREATE TABLE rarity"
                   "("
                   " id INTEGER  PRIMARY KEY AUTOINCREMENT,"
                   "name TEXT UNIQUE"
                   ");");
    this->db->exec("CREATE TABLE VAT"
                   "("
                   "id INTEGER  PRIMARY KEY AUTOINCREMENT,"
                   "value INTEGER NOT NULL"
                   ");");

    this->db->exec("CREATE TABLE `collectionList`"
                   "("
                   "id INTEGER  PRIMARY KEY AUTOINCREMENT,"
                   "name TEXT NOT NULL,"
                   "description TEXT"

                   ");");
    this->db->exec("CREATE TABLE editions"
                   "("
                   "id INTEGER PRIMARY KEY AUTOINCREMENT,"
                   "name TEXT UNIQUE NOT NULL,"
                   "longname TEXT UNIQUE NOT NULL,"
                   "releaseDate TIMESTAMP"
                   ");");

    this->db->exec("CREATE TABLE allCards"
                   "("
                   "id  INTEGER PRIMARY KEY   AUTOINCREMENT,"
                   "name TEXT,"
                   "picURL TEXT,"
                   "id_edition INT,"
                   "manacost TEXT,"
                   "type TEXT,"
                   "pt TEXT,"
                   "id_rarity INTEGER,"
                   "text TEXT,"
                   "priceNet NUMERIC(5,2) DEFAULT 0, "
                   "artist TEXT,"
                   "FOREIGN KEY(id_edition) REFERENCES editions(id),"
                   "FOREIGN KEY(id_rarity) REFERENCES rarity(id) "
                   ");");


    this->db->exec("CREATE TABLE `collectionCards`"
                   "("
                   "id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,"
                   "id_allCards,"
                   "quantity INTEGER NOT NULL DEFAULT 0,"
                   "discount FLOAT DEFAULT 0,"
                   "id_collectionList INTEGER NOT NULL,"
                   "description TEXT,"
                   "FOREIGN KEY(id_allCards) REFERENCES allCards(id)"
                   "FOREIGN KEY(id_collectionList) REFERENCES collectionList(id)"
                   ");");




    this->db->exec("CREATE TABLE storeCards"
                   "("
                   "id INTEGER PRIMARY KEY AUTOINCREMENT,"
                   "id_allCards INTEGER,"
                   "quantity INTEGER DEFAULT 0, "
                   "description TEXT,"
                   "FOREIGN KEY(id_allCards) REFERENCES allCards(id) "
                   ")");

this->db->exec("CREATE TABLE soldCards (id PRIMARY KEY, id_allCards INT, priceNet FLOAT, vat INT, discount INT, quantity INT, date TIMESTAMP DEFAULT CURRENT_TIMESTAMP)");



}



QString DataBase::getIdRarity(QString rarityName)
{

    QSqlQuery *query = new QSqlQuery(*db);
    query->prepare("INSERT INTO rarity(name) VALUES(:rarity);");
    query->bindValue(":rarity", rarityName);
    query->exec();

    query->prepare("SELECT id FROM rarity WHERE name =  :rarityName;");
    query->bindValue(":rarityName", rarityName);
    query->exec();
    query->first();
    QString ret = query->value(0).toString();

    delete query;
    return ret;
}

void DataBase::insertEditionToDb(QString name, QString longname, QString releaseDate){

    QSqlQuery *query = new QSqlQuery(*db);
    query->prepare("INSERT INTO editions(name, longname,  releaseDate ) VALUES (:name, :longname,  :releaseDate);");
    query->bindValue(":name", name);
    query->bindValue(":longname", longname);
    query->bindValue(":releaseDate", releaseDate);

     if(!query->exec())
         qDebug() << "problem z edycja: "<<name << longname <<releaseDate;

     query->lastQuery();
    delete query;



}


void DataBase::insertCardToDb(QString name,QString Edition,QString manacost,QString type,QString pt,QString rarityName, QString picURL,QString text, QString artist){

    QString idRarity = getIdRarity(rarityName);

    QSqlQuery *query = new QSqlQuery(*db);
    query->prepare("SELECT id FROM editions WHERE name = :Edition ;");
    query->bindValue(":Edition", Edition);
    query->exec();

    query->first();
    QString idEdition = query->value(0).toString();


    query->prepare("INSERT INTO allCards(name,id_edition,manacost,type,pt,id_rarity, text, picURL, artist) VALUES(:name,:id_edition,:manacost,:type,:pt,:id_rarity, :text, :picURL, :artist)");
    query->bindValue(":name", name);
    query->bindValue(":id_edition", idEdition);
    query->bindValue(":manacost", manacost);
    query->bindValue(":type", type);
    query->bindValue(":pt", pt);
    query->bindValue(":id_rarity", idRarity);
    query->bindValue(":text", text);
    query->bindValue(":picURL", picURL);
    query->bindValue(":artist", artist);

    if(!query->exec())
        qDebug() << "problem z karta: "<<name << "z edycji"<<idEdition<< "  ";
   delete query;



}


void DataBase::insertDefaultToDb(){
    db->exec("INSERT INTO collectionList(name) VALUES('collection1');");
    db->exec("INSERT INTO collectionList(name) VALUES(collection2');");
    db->exec("INSERT INTO collectionList(name) VALUES('collection3');");
    db->exec("INSERT INTO collectionList(name) VALUES('collection4');");

    db->exec("INSERT INTO vat(value) VALUES(23);");

}

bool DataBase::cheack(int qEdition, int qCollection){


    QString query1 = "SELECT count(*) From editions;";
    QSqlQuery resp = db->exec(query1);
    resp.next();
    if(resp.value(0).toInt() != qEdition )
        return false;

    query1 = "SELECT count(*) From allcards;";
    resp = db->exec(query1);
    resp.next();
    if(resp.value(0).toInt() != qCollection)
        return false;
    return true;


}
