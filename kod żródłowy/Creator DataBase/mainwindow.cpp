#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <vector>
#include <QEventLoop>
/*!
 * Konstruktor głownego okna.
 */
MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    ui->label4->setVisible(false);
}
/*!
 * Destruktor
 */
MainWindow::~MainWindow( )
{
    delete ui;
}


/*!
 *  Wciśnięscie przycisku "Select DataBase"
 */
void MainWindow::on_buttonDB_clicked()
{
    filenameDB = QFileDialog::getSaveFileName(this,
                                              tr("Save "), "C:/", tr("Database file (*.db3)"));

    if(!filenameDB.isEmpty()){
        ui->label3->setEnabled(true);
        ui->buttonCreate->setEnabled(true);
    }
}

/*!
 * Wcieśnięcie przycisky "Create DataBase"
 */

void MainWindow::on_buttonCreate_clicked()
{




    ui->label4->setText("Loading");
    ui->label4->setVisible(true);
    ui->buttonCreate->setEnabled(false);
    ui->buttonDB->setEnabled(false);
    ui->buttonJSON->setEnabled(false);
   QCoreApplication::processEvents();
    QFile fileJSON(filenameJSON);
//    QFile fileJSON("C:\\Users\\Rakieta\\Downloads\\AllSets (1).json");

    if (!fileJSON.open(QIODevice::ReadOnly))
    {
        qWarning("Couldn't open save file.");

    }

    QByteArray saveData = fileJSON.readAll();

    //         QJsonDocument loadDoc(saveFormat == Json
    //                ? QJsonDocument::fromJson(saveData)
    //                : QJsonDocument::fromBinaryData(saveData));

    QJsonDocument loadDoc(  QJsonDocument::fromJson(saveData));

    QJsonObject json = loadDoc.object();
    std::vector<Edition> cointenerMtG;
    QStringList shortEdition(json.keys());
    QStringList::const_iterator constIterator;


    for(constIterator=shortEdition.begin() ; constIterator != shortEdition.end() ;constIterator++ )  // iter po qstring list
        cointenerMtG.push_back( Edition(json[*constIterator].toObject()));


    DataBase *DB = new DataBase(filenameDB);
    DB->openDB();

    DB->createTable();
    DB->insertDefaultToDb();
    static int counterCards(0);
    QString backslash;
    DB->db->transaction();
    for(unsigned int i = 0; i < cointenerMtG.size(); i++)
    {

        DB->insertEditionToDb(cointenerMtG.at(i).getCode(), cointenerMtG.at(i).getName(), cointenerMtG.at(i).getReleaseDate() );

        for(int j = 0 ; j < cointenerMtG.at(i).getQuantityCards(); j++){
            qDebug() << counterCards;
            counterCards++;
            if(cointenerMtG.at(i).getCard(j).getPower() == "" )
                backslash="";
            else
                backslash="/";
            DB->insertCardToDb(cointenerMtG.at(i).getCard(j).getName(),
                               cointenerMtG.at(i).getCode(),
                               cointenerMtG.at(i).getCard(j).getManaCost(),
                               cointenerMtG.at(i).getCard(j).getType(),
                               cointenerMtG.at(i).getCard(j).getPower() + backslash+ cointenerMtG.at(i).getCard(j).getToughness(),
                               cointenerMtG.at(i).getCard(j).getRarity(),
                               "PICURL",
                               cointenerMtG.at(i).getCard(j).getText(),
                               cointenerMtG.at(i).getCard(j).getArtist());
        }

    }

    DB->db->commit();
    QMessageBox msgBox;

    if(!DB->cheack(cointenerMtG.size(), counterCards))
    {
        msgBox.setText("Database is not completed. Try again.");
        msgBox.exec();
    }else{
        msgBox.setText("Database is completed.");
    }
    msgBox.exec();
    ui->label4->setText("Done");

}

/*!
 * Wciśnieście przycisku "Select JSON"
 */
void MainWindow::on_buttonJSON_clicked()
{
    filenameJSON = QFileDialog::getOpenFileName(this,
                                                tr("Open json"), "C:/", tr("JSON file (*.json)"));
    if(!filenameJSON.isEmpty()){
        ui->label2->setEnabled(true);
        ui->buttonDB->setEnabled(true);

    }
}
