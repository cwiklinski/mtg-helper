#ifndef CARD_H
#define CARD_H
#include <QVariant>
#include <QDebug>
/*!
 * @brief Klasa reprezentuje pojecynacza karte Magic the Gathering.
 */
class Card
{
public:
    Card();
     void read(const QJsonObject &json);

     QString getLayout() const{return this->layout;}
     QString getName()const{return this->name;}
     QString getManaCost()const{return this->manaCost;}
     QString getType()const{return this->type;}
     QString getRarity()const{return this->rarity; }
     QString getText()const{return this->text;}
     QString getArtist()const{return this->artist;}
     QString getPower()const{return this->power;}
     QString getToughness()const{return this->toughness;}
     QString getBorder()const{return this->border;}
private:
     QString layout;
     QString name;
     //    QString names[2];
     QString manaCost;
     //    cmc
     //    colors;
     QString type;
     //    supertypes
     //    types
     //    subtypes

     QString  rarity;
     QString   text;
     //    flavor
     QString artist;
     //    number
     QString   power ;
     QString   toughness ;
     //    multiverseid
   QString border;


};

#endif // CARD_H
