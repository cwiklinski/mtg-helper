#include "jnsonshortedition.h"
#include <QDebug>
#include "card.h"
#include <QJsonArray>
#include "memory"
Edition::Edition(QJsonObject obj)
{
        setObject(obj);
}
   void Edition::setObject(QJsonObject obj)
   {

using namespace std;

       this->name = obj["name"].toString();
       this->code = obj["code"].toString();
       this->gatherCode=obj["gatherCode"].toString();
       this->oldCode = obj["oldCode"].toString();
       this->releaseDate= obj["releaseDate"].toString();
       this->border = obj["border"].toString();
       this->type=obj["type"].toString();
       this->block = obj["block"].toString();
       this->onlineOnly = obj["onlineOnly"].toBool();


       QJsonArray  cardsArray = obj["cards"].toArray();
       for (int cardsArrayIter = 0; cardsArrayIter < cardsArray.size(); cardsArrayIter++) {
           QJsonObject cardObject = cardsArray[cardsArrayIter].toObject();

           shared_ptr<Card> sharedCard (new Card());
           sharedCard->read( cardObject);
           cards.push_back(*sharedCard);

       }



   }
