#ifndef COLLECTIONCARD_H
#define COLLECTIONCARD_H
#include "database.h"
#include "cardinfo.h"
#include "collection.h"
#include <memory>


/**
 * @brief
 * Klasa przechowująca informacje o jednym rekordzie z tabeli collectionCards.
 */
class CollectionCard
{
public:
    /**
     * @brief
     * Konstruktor tworzący pusty kontener na karte z kolekcji.
     */
    CollectionCard();
    /**
     * @brief
     * Konstruktor tworzący pusty kontener na karte z kolekcji z gotowym połączeniem z bazą danych.
     * @param _dataBase
     */
    CollectionCard(Database &_dataBase);
    /**
     * @brief
     * Konstruktor przygotuwujący karte.
     * @param id id z tabeli collectionCards
     * @param _dataBase
     */
    CollectionCard(QVariant id, Database &_dataBase);

    /**
     * @brief
     *
     * @param id
     * @return bool
     */
    bool setId(QVariant id)                  {this->id.setValue(id);                            return true;}
    /**
     * @brief
     *
     * @param quantity
     * @return bool
     */
    bool setQuantity (QVariant quantity )    {this->quantity.setValue(quantity);                return true;}
    /**
     * @brief
     *
     * @param sale
     * @return bool
     */
    bool setDiscount(QVariant sale)              {this->mdiscount.setValue(sale);                        return true;}
    /**
     * @brief
     * Ustawia informacje na temat karty
     * @param id id z tabeli allCards
     * @return bool
     */
    bool setCardInfo(QVariant id)            {cardInfo =new CardInfo(id, *this->dataBase);      return true;}
    /**
     * @brief
     * Ustawia kolekcje
     * @param id id z tabeli collection
     * @return bool false jeśli niepowodzenie
     */
    bool setCollection(QVariant id)                 {collection = new Collection(id, *this->dataBase); return true;}
    /**
     * @brief
     *
     * @param description
     * @return bool
     */
    bool setDescription(QVariant description){this->description.setValue(description);          return true;}

    /**
     * @brief
     *
     * @return QVariant
     */
    QVariant getId()             const{return this->id;}
    /**
     * @brief
     *
     * @return QVariant
     */
    QVariant getQuantity()       const{return this->quantity;}
    /**
     * @brief
     *
     * @return QVariant
     */
    QVariant getDiscount()           const{return this->mdiscount;}
    /**
     * @brief
     *
     * @return QVariant
     */
    QVariant getDestription()    const{return this->description;}
    /**
     * @brief
     *
     * @return CardInfo
     */
    CardInfo* getCardInfo()      const{return this->cardInfo;}
    /**
     * @brief
     *
     * @return Collection
     */
    Collection* getCollection()  const{return this->collection;}

    /**
     * Usuwa karte z kolekcji
     * @param id_collectionList id z tabeli collectionList
     */
    void deleteCard();
    /**
     * @brief
     * Przesówa kartedo innej kolekcji.
     */
    void moveToCollection(QVariant id_collectionList);
    /**
     * @brief
     * Wsadza karte do tabeli kolekcji
     */
    void insertToCollectionTbl();
    /**
     * @brief
     * Sprzedaje karte
     * @param quantity ilość sztuk
     */
    void sellCard(QString quantity);
private:
    QVariant id; /**id z tabeli collectionCards */
    QVariant quantity{0}; /** ilość*/
    QVariant mdiscount{0}; /**Zniżka */
    QVariant description; /**Opis*/
    Collection *collection; /**Kolekcja w której znajduję sie karta*/
    CardInfo *cardInfo; /**Informacje na temat karty */
    Database *dataBase; /** */

    /**
     * @brief
     * Zapytanie sql do bazy danych.
     * @param id id z tabeli collectionCards
     * @return QSqlQuery odpoweidz z bazy danych
     */
    QSqlQuery& queryID(QString id);
    /**
     * @brief
     *
     * @param query odpowiedz z bazy danych.
     * @return bool
     */
    bool queryToVariable(QSqlQuery &query);

};

#endif // COLLECTIONCARD_H
