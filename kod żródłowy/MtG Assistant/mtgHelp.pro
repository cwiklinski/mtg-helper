#-------------------------------------------------
#
# Project created by QtCreator 2014-06-22T20:45:06
#
#-------------------------------------------------

QT       += core gui sql network xml
greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = mtgHelp
TEMPLATE = app




SOURCES += main.cpp\
        mainwindow.cpp \
    cardinfo.cpp \
    database.cpp \
    edition.cpp \
    rarity.cpp \
    vat.cpp \
    listeditions.cpp \
    listrarity.cpp \
    columnfilter.cpp \
    dialogdevice.cpp \
    socket.cpp \
    tableviewsoldcards.cpp \
    dialogsoldcards.cpp \
    proxymodelsoldcards.cpp \
    dialogeksporttocsv.cpp \
    downloadmanage.cpp \
    parserxmlfromdevice.cpp \
    tableviewdevicecards.cpp \
    cardsfromxmparsing.cpp \
    collection.cpp \
    collectioncard.cpp \
    listcollection.cpp \
    proxymodelcollectioncards.cpp \
    proxymodelstorecards.cpp \
    storecard.cpp \
    tabelviewstorecards.cpp \
    tableviewcollectioncardsmodel.cpp \
    dialogdetailscard.cpp \
    dialogstorecardsell.cpp \
    dialogaddnewcardtocollection.cpp

HEADERS  += mainwindow.h \
    cardinfo.h \
    database.h \
    edition.h \
    rarity.h \
    vat.h \
    listeditions.h \
    listrarity.h \
    columnfilter.h \
    dialogdevice.h \
    socket.h \
    tableviewsoldcards.h \
    dialogsoldcards.h \
    proxymodelsoldcards.h \
    dialogeksporttocsv.h \
    downloadmanage.h \
    parserxmlfromdevice.h \
    tableviewdevicecards.h \
    cardsfromxmparsing.h \
    collection.h \
    collectioncard.h \
    dialogstorecardsell.h \
    listcollection.h \
    proxymodelcollectioncards.h \
    proxymodelstorecards.h \
    storecard.h \
    tabelviewstorecards.h \
    tableviewcollectioncardsmodel.h \
    dialogdetailscard.h \
    dialogaddnewcardtocollection.h

FORMS    += mainwindow.ui \
    columnfilter.ui \
    dialogaddnewcardtobox.ui \
    dialogdevice.ui \
    dialogsoldcards.ui \
    dialogeksporttocsv.ui \
    dialogdetailscard.ui \
    dialogstorecardsell.ui

RESOURCES += \
    mtgHelp.qrc

QMAKE_CXXFLAGS += -std=gnu++0x
