#ifndef DATABASE_H
#define DATABASE_H
#include <QSqlDatabase>
#include <QDebug>
#include <QCoreApplication>
#include <QDir>

/**
 * @brief
 * Klasa umożliwająca komunikacje z baza danych
 */
class Database
{
public:
    /**
     * @brief
     * Kontruktor
     */
    Database();
    /**
     * @brief
     * Destruktor
     */
    ~Database();

    QSqlDatabase *db; /** */
    /**
     * @brief
     * Otwarcie połączenia z bazą danych.
     */
    void openDB();
    /**
     * @brief
     * Zamknięcie połączenia z bazą danych.
     */
    void closeDB();
};

#endif // DATABASE_H
