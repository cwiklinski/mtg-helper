#include "cardinfo.h"
#include <QDebug>

CardInfo::CardInfo(){}
CardInfo::CardInfo(QVariant id,  Database &objDataBase ) : database(&objDataBase)
{
    queryToVariable(queryID(id.toString()));
}

CardInfo::CardInfo(int id,  Database &objDataBase ) : database(&objDataBase)
{
    queryToVariable(queryID(QString::number(id)));
}

CardInfo::CardInfo(QString id, Database &objDataBase) : database(&objDataBase)
{
    queryToVariable(queryID(id));
}

CardInfo::CardInfo(QString name, QString edition,  Database &objDataBase, int flag ) : database(&objDataBase)
{
    QSqlQuery *query = new QSqlQuery(*database->db);
    query->prepare(QString("SELECT id,name,picURL, id_edition, manacost, type,id_rarity, text, priceNet FROM allCards WHERE name = :name AND id_edition = :id_edition  ") );
    Edition *tempEdition = new Edition(edition, *database, flag);
    query->bindValue(":id_edition", tempEdition->getId().toString());
    delete tempEdition;
    query->bindValue(":name", name);
    query->exec();
    this->queryToVariable(*query);
    delete query;
}




CardInfo::~CardInfo()
{
}

bool CardInfo::queryToVariable(QSqlQuery &query)
{
    int debQueryRespCout = 0;
    vat = new Vat(1, *database);
    while (query.next())
    {
        setId(query.value(0));
        setName(query.value(1));
        setPicURL(query.value(2));
        setEdition(query.value(3));
        setManacost(query.value(4));
        setType(query.value(5));
        setRarity(query.value(6));
        setText( query.value(7));
        setPriceNetto(query.value(8));
        debQueryRespCout++;
        if(debQueryRespCout>1){
            qDebug() << "Wiecej niż jedna odpowiedz na zapytanie" << query.lastQuery();
            return false;
        }
    } //end while loop
    QSqlQuery *temp = &query;
    delete temp;
    if(debQueryRespCout < 1){
        qDebug() << "Zero odopwiedzi na zapytanie" ;
        return false;
    }
    return true;
}




QSqlQuery &CardInfo::queryID(QString id)
{
    QSqlQuery *query = new QSqlQuery(*database->db);
    query->prepare("SELECT id,name,picURL, id_edition, manacost, type,id_rarity, text, priceNet FROM allCards WHERE id = :id ;");
    query->bindValue(":id", id);
    query->exec();
    return *query;
}

void CardInfo::updatePrice()
{
    QSqlQuery *query = new QSqlQuery(*database->db);
    query->prepare("UPDATE allCards SET priceNet = :priceNet WHERE id = :id ;");
    query->bindValue(":id", getId().toString());
    query->bindValue(":priceNet", getPriceNet().toString());
    query->exec();
}
