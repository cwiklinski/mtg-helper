#include "socket.h"

Socket::Socket(QObject *parent) :
    QObject(parent)
{
}

void Socket::connect(QString IP, QString port){
    socket = new QTcpSocket(this);

    socket->connectToHost(IP,port.toInt());
    if(socket->waitForConnected(3000))
    {
        qDebug() << "Poloaczono";
    }



}

bool Socket::sendC01()
{

        socket->write("##C01###");
        socket->waitForBytesWritten();
        socket->waitForReadyRead();
        qDebug() << "Reading"<<socket->bytesAvailable();



        QString respServ01(socket->readAll());
        if(respServ01 == "##S01###")
            return true;


        return false;


}

bool Socket::sendC02()
{

        socket->write("##C02###");
        socket->waitForBytesWritten();
        socket->waitForReadyRead();
        qDebug() << "Reading"<<socket->bytesAvailable();

        QString respServ02(socket->readAll());
        if(respServ02 == "##S02###")
            return true;


        return false;


}

QString Socket::readLinkToXml()
{

        socket->waitForReadyRead();
        qDebug() << "Reading"<<socket->bytesAvailable();
        QByteArray respFromServ = socket->readAll();

        QString linkToXml(respFromServ);
        linkToXml.remove(0,2);
        linkToXml.remove(linkToXml.length()-3,3);
        return linkToXml;
}

