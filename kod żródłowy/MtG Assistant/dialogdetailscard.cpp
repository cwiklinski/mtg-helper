#include "dialogdetailscard.h"
#include "ui_dialogdetailscard.h"
#include <windows.h>
#include <QPicture>
#include <QMessageBox>
#include "downloadmanage.h"
#include <QCoreApplication>
#include <QGraphicsPixmapItem>
#include <QPixmap>

DialogDetailShopCard::DialogDetailShopCard(int row, Database &dataBase, QWidget *parent ) :
    QDialog(parent), dataBase(&dataBase),
    ui(new Ui::DialogDetailsCard)
{
    int id = row+1;
    shopCard = new StoreCard(id, *this->dataBase);
    ui->setupUi(this);
    SetWindow();
    ui->lineVAT->setValidator( new QIntValidator(0, 100, this) );

}
DialogDetailShopCard::DialogDetailShopCard(): ui(new Ui::DialogDetailsCard)
{
    ui->setupUi(this);
}

DialogDetailShopCard::~DialogDetailShopCard()
{
    delete ui;
}

void DialogDetailShopCard::SetWindow()
{
    setWindowTitle("Store: " + shopCard->getCardInfo()->getName().toString());
    ui->Name->setText("Name: " + shopCard->getCardInfo()->getName().toString());
    ui->Edition->setText("Edition: " + shopCard->getCardInfo()->getEdition()->getLongname().toString());
    ui->lineQuantity->setValue(  shopCard->getQuantity().toInt());
    ui->lineVAT->setText(shopCard->getCardInfo()->getVat()->getValue().toString());
    ui->linePriceNet->setText( shopCard->getCardInfo()->getPriceNet().toString());
    ui->linePriceGross->setText(shopCard->getCardInfo()->getPriceGross().toString());
    ui->lineDescription->setText( shopCard->getDestription().toString());

    if (shopCard->getQuantity().toInt()<=0)
        ui->sellCard->setEnabled(false);
    QString lPathToPic(qApp->applicationDirPath().replace(QString("/"),QString("\\")) + "\\images\\cards\\" + shopCard->getCardInfo()->getEdition()->getName().toString() + "\\" + shopCard->getCardInfo()->getName().toString() + ".jpg");
    QPixmap *lPic  = new QPixmap();
    if( lPic->load(lPathToPic)){
        ui->PIC->setPixmap(*lPic);
    }else{
        DownloadManager *dm = new DownloadManager();
        dm->setFileName(shopCard->getCardInfo()->getEdition()->getName().toString() + "\\" + shopCard->getCardInfo()->getName().toString() );
        connect(dm, SIGNAL(overDownload(QString )), this, SLOT(setPicture()) ) ;
        dm->setFile(shopCard->getCardInfo()->getPicURL().toString() ,DownloadManager::ePicFile);
    }


}

void DialogDetailShopCard::setPicture()
{
   QString lPathToPic(qApp->applicationDirPath().replace(QString("/"),QString("\\")) + "\\images\\cards\\" + shopCard->getCardInfo()->getEdition()->getName().toString() + "\\" + shopCard->getCardInfo()->getName().toString() + ".jpg");
   QPixmap *lPic = new QPixmap(lPathToPic);
   QImage img(lPathToPic);
   ui->PIC->setPixmap(*lPic);
}


void DialogDetailShopCard::on_sellCard_clicked()
{  
    dialogstoreCardSell    *itc= new dialogstoreCardSell  (*this->shopCard, *dataBase, this);
    itc->show();
    close();
}


void DialogDetailShopCard::on_checkBoxEditable_clicked(bool checked)
{
    if(checked == true)
    {
        ui->lineQuantity->setEnabled(true);
        ui->linePriceGross->setEnabled(true);
        ui->linePriceNet->setEnabled(true);
        ui->lineDescription->setEnabled(true);
    }else if( checked == false){
        ui->lineQuantity->setEnabled(false);

        ui->linePriceGross->setEnabled(false);
        ui->linePriceNet->setEnabled(false);
        ui->lineDescription->setEnabled(false);
    }
}

bool DialogDetailShopCard::doYouChangedMe()
{
    if(ui->lineQuantity->text()==shopCard->getQuantity().toString() || (ui->lineQuantity->text()=="0" && shopCard->getQuantity().toString()=="") )
            if(ui->linePriceNet->text().toFloat()==shopCard->getCardInfo()->getPriceNet().toFloat()|| (ui->linePriceNet->text()=="0"&&shopCard->getCardInfo()->getPriceNet().toString()==""))
                if(ui->linePriceGross->text().toFloat()==shopCard->getCardInfo()->getPriceGross().toFloat() || (ui->linePriceGross->text()=="0" && shopCard->getCardInfo()->getPriceGross().toString()== ""))
                    if(ui->lineDescription->toPlainText()==shopCard->getDestription().toString())
                        return false;
    return true;
}

void DialogDetailShopCard::save()
{
    shopCard->setQuantity(ui->lineQuantity->text());
    shopCard->getCardInfo()->setPriceNetto(ui->linePriceNet->text());
    shopCard->setDestription(ui->lineDescription->toPlainText());
    shopCard->upClassToDb();
    shopCard->getCardInfo()->updatePrice();

    if(ui->lineQuantity->value() <= 0)
        ui->sellCard->setEnabled(false);
    else
        ui->sellCard->setEnabled(true);
}



void DialogDetailShopCard::on_lineVAT_editingFinished()
{
    float brutto =  ui->linePriceNet->text().toFloat() * ui->lineVAT->text().toFloat()/100.0   + ui->linePriceNet->text().toFloat() ;
    ui->linePriceGross->setText(QString::number(brutto, 2, 2));
}


void DialogDetailShopCard::on_buttonSave_clicked()
{
    QMessageBox msgBox(this);
    if( doYouChangedMe()){
        msgBox.setText("The card's detail has been modified.");
        msgBox.setInformativeText("Do you want to save your changes?");
        msgBox.setStandardButtons(QMessageBox::Save | QMessageBox::Discard | QMessageBox::Cancel);
        int ret = msgBox.exec();

        switch (ret) {
        case QMessageBox::Save:
            // Save was clicked
            save();
            break;
        case QMessageBox::Discard:
            // Don't Save was clicked
            break;
        case QMessageBox::Cancel:
            // Cancel was clicked
            break;
        default:
            // should never be reached
            break;
        }
    }else{

        msgBox.setText("The card's detail NOT has been modified.");
        msgBox.setInformativeText("The card's detail NOT has been modified.");
        msgBox.setStandardButtons(QMessageBox::Ok);
        msgBox.exec();
    }
}

void DialogDetailShopCard::on_buttonCancel_clicked()
{
    QMessageBox msgBox;
    if(doYouChangedMe()){
        msgBox.setText("The card's detail  has been modified. Are you sure?");
        msgBox.setInformativeText("The card's detail  has been modified.");
        msgBox.setStandardButtons(QMessageBox::Ok | QMessageBox::Cancel);
        int ret = msgBox.exec();
        if(ret == QMessageBox::Ok)
            this->close();
    }else{
        this->close();
    }
}

void DialogDetailShopCard::on_linePriceGross_textChanged(const QString &arg1)
{
    float brutto =  ui->linePriceGross->text().toFloat() / (ui->lineVAT->text().toFloat()/100+1);
    ui->linePriceNet->setText(QString::number(brutto, 2, 2));
}

void DialogDetailShopCard::on_linePriceNet_textEdited(const QString &arg1)
{
    float brutto =  ui->linePriceNet->text().toFloat() * ui->lineVAT->text().toFloat()/100 + ui->linePriceNet->text().toFloat();
    ui->linePriceGross->setText(QString::number(brutto, 2, 2));
}
