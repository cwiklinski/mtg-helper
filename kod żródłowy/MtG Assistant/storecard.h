#ifndef SHOPCARD_H
#define SHOPCARD_H
#include <QVariant>
#include "vat.h"
#include "cardinfo.h"



/**
 * @brief
 * Klasa opisująco jedna karte ze sklepu
 */
class StoreCard
{
public:
    static int const CardInfoID_cardinfo = 1; /**Flaga */
    static int const CardInfoID = 2; /**Flaga */
    /**
     * @brief
     * Konstruktor tworzący pustt kontener na karte
     */
    StoreCard();
    /**
     * @brief
     * Konstruktor tworzy karte na podwstaiw podanych argumentów
     * @param _id Id karty w zależności od flagi. Id karty z tabbeli storeCards lub id z tabeli allCards
     * @param objDataBase otwarta baza damych
     * @param flag
     */
    StoreCard(QString _id,Database &objDataBase, int flag = CardInfoID_cardinfo  );
    /**
     * @brief
     * Konstruktor tworzy karte na podwstaiw podanych argumentów
     * @param _id Id karty w zależności od flagi. Id karty z tabbeli storeCards lub id z tabeli allCards
     * @param objDataBase otwarta baza damych
     * @param flag
     */
    StoreCard(QVariant _id,Database &objDataBase,int flag = CardInfoID_cardinfo);
    /**
     * @brief
     * Konstruktor tworzy karte na podwstaiw podanych argumentów
     * @param _id Id karty w zależności od flagi. Id karty z tabbeli storeCards lub id z tabeli allCards
     * @param objDataBase otwarta baza damych
     * @param flag
     */
    StoreCard(int _id,Database &objDataBase, int flag = CardInfoID_cardinfo);


    /**
     * @brief
     *
     * @return QVariant
     */
    QVariant getId()         const{return this->id;}
    /**
     * @brief
     *
     * @return QVariant
     */
    QVariant getQuantity()     const{return this->quantity;}
    /**
     * @brief
     *
     * @return QVariant
     */
    QVariant getDestription()const{return this->description;}
    /**
     * @brief
     *
     * @return CardInfo
     */
    CardInfo* getCardInfo()  const{return this->cardInfo;}

    /**
     * @brief
     *
     * @param id
     * @return bool
     */
    bool setId         (QVariant id)         {this->id = id; return true; }
    /**
     * @brief
     *
     * @param quantity
     * @return bool
     */
    bool setQuantity    (QVariant quantity)     {this->quantity = quantity; return true; }
    /**
     * @brief
     *
     * @param description
     * @return bool
     */
    bool setDestription(QVariant description){this->description = description; return true; }
    /**
     * @brief
     *
     * @param id
     * @return bool
     */
    bool setCardInfo   (QVariant id)         {this->cardInfo = new CardInfo(id, *dataBase); return true;}



    /**
     * @brief
     *  Wrzuca karte do tabeli storeCard. Jeśli taka karta istnieję to wykonany jest update
     */
    void upClassToDb();
    /**
     * @brief
     * Wypełnienie klasy na podstawie id karty.
     * @param _id
     * @param flag
     */
    void chooseQuery(QString _id, int flag);
    /**
     * @brief
     * Sprzedaż karty
     * @param pNumber liczba kart do sprzedania
     * @param pDiscount zniżka
     */
    void sell(int pNumber, QString pDiscount="0");

private:
    Database *dataBase; /**< TODO */
    /**
     * @brief
     * Zwrócona odpoweidz z bazy danych umieszczana jest w odpoweidnich zmiennych
     * @param query
     * @return bool
     */
    bool queryToVariable(QSqlQuery &query);
    /**
     * @brief
     * Pytanie o karte z tabeli storeCards na podstawie id
     * @param id id sklepowej karty
     */
    void queryID(QString id);
    /**
     * @brief
     *Pytanie o karte z tabeli storeCards na podstawie id_allCards
     * @param id_cardinfo
     */
    void queryID_cardinfo(QString id_cardinfo);

    QVariant id; /**id sklepowe karty */
    QVariant quantity; /**< TODO */

    QVariant priceNetto; /**Cena netto */
    QVariant description; /**opis */
    CardInfo *cardInfo; /**Obiekt przechowujące ogólne dane o karcie */



};

#endif // SHOPCARD_H
