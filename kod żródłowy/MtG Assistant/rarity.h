#ifndef RARITY_H
#define RARITY_H
#include<QSqlQuery>
#include "database.h"
#include <QDebug>


/**
 * @brief
 * Klasa przechowująca jedno rarity.
 */
class Rarity
{


public:
    static int const RarityID = 1; /**Flaga*/
    static int const RarityName = 2; /**Flaga */

    /**
     * @brief
     * Konstruktor
     */
    Rarity();
    /**
     * @brief
     * Destruktor
     */
    ~Rarity();
    /**
     * @brief
     * Kontruktor wypełniający klase danymi.
     * @param value id lub nazwa karty w zależności od ustawionej flagi
     * @param objDataBase otwarty obiekt bazy danych
     * @param flag Flaga
     */
    Rarity(QString value, Database &objDataBase, int flag = RarityID);
    /**
     * @brief
     * Kontruktor wypełniający klase danymi.
     * @param value id lub nazwa karty w zależności od ustawionej flagi
     * @param objDataBase otwarty obiekt bazy danych
     * @param flag Flaga
     */
    Rarity(QVariant value, Database &objDataBase,int flag = RarityID);
    /**
     * @brief
     * Kontruktor wypełniający klase danymi.
     * @param value id lub nazwa karty w zależności od ustawionej flagi
     * @param objDataBase otwarty obiekt bazy danych
     * @param flag Flaga
     */
    Rarity(int id, Database &objDataBase);

    /**
     * @brief
     *
     * @return QVariant
     */
    QVariant getId()    const{return this->id;}
    /**
     * @brief
     *
     * @return QVariant
     */
    QVariant getText()  const{return this->text;}

    /**
     * @brief
     *
     * @param id
     * @return bool
     */
    bool setId  (QVariant id)       {this->id.setValue(id); return true;}
    /**
     * @brief
     *
     * @param text
     * @return bool
     */
    bool setText(QVariant text)     {this->text.setValue(text);return true;}


    /**
     * @brief
     *
     * @param value
     * @param flag
     */
    void chooseQuery(QString value, int flag);


private:  
    QVariant id; /**Id */
    QVariant text; /**Tekst*/

    Database *dataBase; /**Połącznie z baza danych dal klasy */
    /**
     * @brief
     * Zapytanie do bazy na podstawie id z tabeli rarity
     * @param id
     */
    void queryID(QString id);
    /**
     * @brief
     * Zapytanie do bazy na podstawie name z tabeli rarity
     * @param name
     */
    void queryName(QString name);
    /**
     * @brief
     * Odpowiedz z bazy danych przenisiona do członkóW klasy.
     * @param query
     * @return bool
     */
    bool queryToVariable(QSqlQuery &query);
};

#endif // RARITY_H
