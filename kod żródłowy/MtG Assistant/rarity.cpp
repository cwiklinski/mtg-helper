#include "rarity.h"

Rarity::Rarity(){}

Rarity::~Rarity(){}

Rarity::Rarity(QString value, Database &objDataBase, int flag){
    dataBase = &objDataBase;
    this->chooseQuery(value, flag);
}

Rarity::Rarity(QVariant value, Database &objDataBase,int flag){
    dataBase = &objDataBase;
    this->chooseQuery(value.toString(), flag);
}

Rarity::Rarity(int id, Database &objDataBase){
    dataBase = &objDataBase;
    this->queryID(QString::number(id));
}

bool Rarity::queryToVariable(QSqlQuery &query){
    int debQueryRespCout=0;
    while (query.next())
    {
        this->id.setValue(query.value(0));
        this->text = (query.value(1).toString());
        debQueryRespCout++;
        if(debQueryRespCout>1){
            qDebug() << "Wiecej niż jedna odpowiedz na zapytanie"
                     << query.lastQuery();
            return false;
        }
    }
    if(debQueryRespCout < 1){
        qDebug() << "Zero odopwiedzi na zapytanie"
                 << query.lastQuery();
        return false;
    }
    return true;
}

void Rarity::chooseQuery(QString value, int flag){
    if(flag == Rarity::RarityID){
        this->queryID(value);
    }
    else if(flag == Rarity::RarityName){
        this->queryName(value);
    }
}

void Rarity::queryID(QString id){  
    QSqlQuery *query = new QSqlQuery(*dataBase->db);
    query->prepare("SELECT id,name FROM rarity WHERE id = :id ;");
    query->bindValue(":id", id);
    query->exec();
    this->queryToVariable(*query);
    delete query;
}

void Rarity::queryName(QString name){
    QSqlQuery *query = new QSqlQuery(*dataBase->db);
    query->prepare("SELECT id,name FROM rarity WHERE name = :name ;");
    query->bindValue(":name", name);
    query->exec();
    queryToVariable(*query);
    delete query;
}
