#ifndef LISTEDITIONS_H
#define LISTEDITIONS_H
#include "edition.h"
#include <QList>
#include "database.h"
/**
 * @brief
 * Klasa zawiera liste edycji
 */
class ListEditions
{
public:
    /**
     * @brief
     * Kontruktor uzupełniajacy klase o liste edycji
     * @param objDatabase
     */
    ListEditions(Database &objDatabase);
    /**
     * @brief
     * Odświeża liste edycji
     * @return bool
     */
    bool refreshEditionList();
    /**
     * @brief
     *
     * @return QStringList Zwraca liste skrótów edycji
     */
    QStringList getEditionNameList();
    /**
     * @brief
     *
     * @return QStringList Zwraca liste edycji
     */
    QStringList getEditionLongNameList();
    /**
     * @brief
     *
     * @param id id edycji z tabeli edition
     * @return Edition Zwraca edycjie
     */
    Edition& getEdition(QVariant id){return listEditions[id.toInt()-1];}

private:
    int size; /***/
    Database *dataBase; /***/
    Edition *listEditions; /** */


};

#endif // LISTEDITIONS_H
