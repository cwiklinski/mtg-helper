#ifndef TABLEVIEWBOXCARDSMODEL_H
#define TABLEVIEWBOXCARDSMODEL_H

#include <QSqlQueryModel>
#include "database.h"

/**
 * @brief
 * Klasa do prezenacja kart z kolekcji.
 */
class TableViewCollectionCardsModel : public QSqlQueryModel
{
      Q_OBJECT
public:
    /**
     * @brief
     * Konstruktor. Uzupełnia tabele wywołująć funkcje refresh()
     * @param parent
     */
    TableViewCollectionCardsModel(QObject *parent = 0);

//    QVariant data(const QModelIndex &item, int role) const;
//    QVariant headerData(int section, Qt::Orientation orientation, int role) const;
    /**
     * @brief
     * Reimplementacja funkcji. Zwraca ilość kolumn
     * @param index
     * @return int ilość kolumn
     */
    int	columnCount(const QModelIndex & index = QModelIndex()) const;
//      Qt::ItemFlags flags(const QModelIndex &index) const;

/**
 * @brief
 * Odświeża widok tabeli na podstawie bazy danych.
 * @param dataBase
 */
void refresh(Database &dataBase);
};

#endif // TABLEVIEWBOXCARDSMODEL_H
