#include "listrarity.h"

ListRarity::ListRarity(Database &objDatabase): dataBase(&objDatabase)
{
    this->refreshRarityList();
}

bool ListRarity::refreshRarityList()
{
    QSqlQuery *query = new QSqlQuery(*dataBase->db);
    query->exec("SELECT count(*) FROM rarity;");
    query->executedQuery();
    query->first();
    this->size = query->value(0).toInt();
    delete query;
    listRarity= new Rarity[this->size];
    for(int i =1 ; i <= this->size ; i++){
        listRarity[i-1] = Rarity(QString::number(i),*this->dataBase);
    }
}

QStringList ListRarity::getRarityList(){
    QStringList *list = new QStringList;
    for(int i = 0; i<size; i++)
        list->append(this->listRarity[i].getText().toString());
    return *list;
}

