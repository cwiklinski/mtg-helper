#ifndef PROXYMODELBOXCARDS_H
#define PROXYMODELBOXCARDS_H

#include <QSortFilterProxyModel>
#include  "collectioncard.h"
#include "storecard.h"
/**
 * @brief
 * Klasa filtrująca tabele kolekcjo
 */
class ProxyModelCollectionCards : public QSortFilterProxyModel
{
    Q_OBJECT
public:
    /**
     * @brief
     * Jawny konstruktor
     * @param parent
     */
    explicit ProxyModelCollectionCards(QObject *parent = 0);

    bool checkW; /**Czy wyświetlić karty białe? */
    bool checkU; /** Czy wyświetlić karty niebieskie?  */
    bool checkG; /**Czy wyświetlić karty zielone?  */
    bool checkR; /**Czy wyświetlić karty czerwone?  */
    bool checkB; /**Czy wyświetlić karty czarne?  */
    bool checkArtifact; /**Czy wyświetlić karty artyfaktyczne?  */
    bool checkLand{true}; /**Czy wyświetlić karty land? */
    bool checkCardInShop; /**Czy wyświetlić karty znajdujące się s sklepie  */
    QString rarity; /**Wyświetlić karty z rarity. */
    QString editionName; /**Wyświetlić karty o nazwie edycji */
    QString editionLongname; /**yświetlić karty o długiej nazwie edycji */
    QString collectionName; /**< TODO */
    /**
     * @brief
     * Wykasuj zaznaczone karty checkBoxem.
     * @param dataBase
     */
    void deleteCard(Database &dataBase);

    bool zmiennnaPom; /** */
    /**
     * @brief
     * Zwraca dane do wyświetlenia w tabeli.
     * @param index
     * @param role
     * @return QVariant
     */
    QVariant data(const QModelIndex &index, int role) const;
    unsigned int columnFilter; /**< TODO */
    bool *checkBoxTable; /**< TODO */
    /**
     * @brief
     * Ustawia wszystkie checkBoxy na false
     */
    void  setCheckboxCointener();
    /**
     * @brief
     * Ustawia wszystkie checkBoxy na false.
     * @param value wartość na jaką zostaną ustawion checkBoxy
     */
    void setCheckboxCointener(bool value);
    /**
     * @brief
     * Przenieś zaznaczony karty do sklepu
     * @param dataBase
     */
    void moveToShop(Database &dataBase);
    /**
     * @brief
     * Przenosi zaznaczone karty do kolekcji.
     * @param idCollection docelowa kolekcja do któRej trafią karty.
     * @param dataBase
     */
    void moveToCollection(QVariant idCollection, Database &dataBase);

    /**
     * @brief
     * Ustawia dane dla tabeli
     * @param index
     * @param value
     * @param role
     * @return bool
     */
    bool  setData(const QModelIndex &index, const QVariant &value, int role);
    /**
     * @brief
     * Reimplementacja funkcji
     * @param index
     * @return Qt::ItemFlags
     */
    Qt::ItemFlags flags(const QModelIndex &index) const;
    //      int columnCount(const QModelIndex & index = QModelIndex()) const;
    /**
     * @brief
     * Sprzedaje zaznaczone karty
     * @param dataBase
     */
    void sellCard(Database &dataBase);
    /**
     * @brief
     * Przygotowuje status bar wyświetlając sumarycznie ceny oraz ilosc zaznaczonych karty
     * @return float
     */
    float *stateBar();

protected:
    /**
     * @brief
     * Reimplementowana funkcja filtrujaca
     * @param sourceRow
     * @param sourceParent
     * @return bool
     */
    bool filterAcceptsRow(int sourceRow,const QModelIndex &sourceParent) const;
    /**
     * @brief
     * Reimplementowana funkcja ustawiająca nagłówki
     * @param section
     * @param orientation
     * @param role
     * @return QVariant
     */
    QVariant headerData(int section, Qt::Orientation orientation, int role) const;

signals:
    /**
     * @brief
     * Wywoływana gdy zaszłą jakakolwiek zmiana.
     */
    void dataChanged();



};


#endif // PROXYMODELBOXCARDS_H
