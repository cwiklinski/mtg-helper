#ifndef INSERTTOCARD_H
#define INSERTTOCARD_H
#include <QDebug>
#include <QSqlQuery>
#include "database.h"
#include <QDialog>
#include "listcollection.h"
#include "storecard.h"
#include "collectioncard.h"
#include <QNetworkReply>
#include <QNetworkAccessManager>
#include <QUrl>
#include <QNetworkRequest>



namespace Ui {
class  dialogstoreCardSell;
}

/**
 * @brief
 * Klasa odpowiedziana za wyświetlanie okana sklepowej karty
 */
class dialogstoreCardSell : public QDialog , public Database
{
    Q_OBJECT

public:
    /**
     * @brief
     * Konstruktor kreujący okno
     * @param _shopCard
     * @param dataBase
     * @param parent
     */
    explicit  dialogstoreCardSell(StoreCard &_shopCard, Database &dataBase, QWidget *parent = 0);
    /**
     * @brief
     * Destruktor
     */
    ~ dialogstoreCardSell();

private slots:

    void setPicture();

    /**
     * @brief
     * Zmiana ilości uruchamia tą funkcje. Zmienia ona ceny wyświetlane w oknie
     * @param arg1
     */
    void on_spinBoxQuantity_valueChanged(const QString &arg1);
    /**
     * @brief
     * Przycisk przenieć do kolekcj ("Move")
     */
    void on_buttonInsertIntoCollection_clicked();
    /**
     * @brief
     * Przycisk "Sell"
     */
    void on_buttonSell_clicked();
    /**
     * @brief
     * Przycisk "Cancel"
     */
    void on_buttonCancel_clicked();
    /**
     * @brief
     * CheckBox odpowiedizalny za obniżke.
     * @param checked
     */
    void on_groupBoxSale_toggled(bool checked);

    /**
     * @brief
     * Ustawienie spinBoxa odpoweidzialnego za określenei obniżki.
     * @param arg1
     */
    void on_spinBoxDiscount_valueChanged(double arg1);

private:
    StoreCard *shopCard; /**Karta prezentowana w oknie*/
    Ui::dialogstoreCardSell *ui; /** */
    Database *dataBase; /** */

};

#endif // INSERTTOCARD_H
