#include "tableviewsoldcards.h"

TableViewSoldCards::TableViewSoldCards(QObject *parent) :  QSqlQueryModel(parent)
{

}
void TableViewSoldCards::refresh(Database &dataBase){
    setQuery("SELECT allCards.name,"
             "editions.longname,"
             "soldCards.quantity,"
             "soldCards.vat ,"
             "soldCards.priceNet *soldCards.quantity,"
             "ROUND(soldCards.priceNet+ soldCards.priceNet * soldCards.vat* soldCards.quantity /100 ,2) , "
             "soldCards.discount,"
             "soldCards.priceNet *soldCards.quantity -  (soldCards.priceNet *soldCards.quantity)* soldCards.discount/100,"
             "ROUND(soldCards.priceNet+ soldCards.priceNet * soldCards.vat* soldCards.quantity /100  - ((soldCards.priceNet+ soldCards.priceNet * soldCards.vat* soldCards.quantity /100) * soldCards.discount/100) ,2) ,"
             "soldCards.date "
             "FROM  soldCards LEFT JOIN  allcards  ON soldCards.id_allCards = allCards.id LEFT JOIN editions ON allCards.id_edition = editions.id;", *dataBase.db);
    while (canFetchMore()) fetchMore();
}
