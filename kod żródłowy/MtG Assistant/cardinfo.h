#ifndef CARDINFO_H
#define CARDINFO_H
#include <QVariant>
#include <QSqlQuery>
#include <QDebug>
#include "database.h"
#include "edition.h"
#include "rarity.h"
#include  "vat.h"




/**
 * @brief
 * Klasa przechowująca rekord tabeli allCards. Zmienne klasy mEdition oraz mRarity zawierają informacje po połączeniu tabel edition oraz rarity.
 */
class CardInfo
{

public:  
    /**
     * @brief
     * Konstruktor
     */
    CardInfo();
    /**
     * @brief
     * Konstruktor
     * @param name
     * @param edition
     * @param objDataBase
     * @param flag
     */
    CardInfo(QString name, QString edition, Database &objDataBase , int flag = Edition::EditionID );
    /**
     * @brief
     * Konstruktor
     * @param id
     * @param objDataBase
     */
    CardInfo(int id,  Database &objDataBase);
    /**
     * @brief
     * Konstruktor
     * @param id
     * @param objDataBase
     */
    CardInfo(QString id, Database &objDataBase);
    /**
     * @brief
     * Konstruktor
     * @param id
     * @param objDataBase
     */
    CardInfo(QVariant id,  Database &objDataBase);
    /**
     * @brief
     * Destruktor
     */
    ~CardInfo();

    /**
     * @brief
     * Zwraca Id karty z tabeli allCards.
     * @return QVariant
     */
    QVariant getId()        const {return this->id;}
    /**
     * @brief
     * Zwraca nazwe karty
     * @return QVariant
     */
    QVariant getName()      const {return this->name;}
    /**
     * @brief
     * Zwraca adres http karty
     * @return QVariant
     */
    QVariant getPicURL()    const {return QVariant("http://mtgimage.com/set/" + getEdition()->getName().toString() + "/" + getName().toString() + ".hq.jpg")/*this->picURL*/;}
    /**
     * @brief
     * Zwraca koszt many karty.
     * @return QVariant
     */
    QVariant getManacost()  const {return this->manacost;}
    /**
     * @brief
     * Zwraca typ karty. Np. Land, Creature - Elf, Instant.
     * @return QVariant
     */
    QVariant getType()      const {return this->type;}
    /**
     * @brief
     * Zwraca tekst karty.
     * @return QVariant
     */
    QVariant getText()      const {return this->text;}
    /**
     * @brief
     * Zwraca caeneNetto karty
     * @return QVariant
     */
    QVariant getPriceNet()  const {return this->priceNet.toFloat();}
    /**
     * @brief
     * Zwraca cene brutoo karty.
     * @return QVariant
     */
    QVariant getPriceGross()const {return QString::number(this->priceNet.toFloat()+this->priceNet.toFloat()*getVat()->getValue().toFloat()/100.0,'f',2);}
    /**
     * @brief
     * Zwraca obiekt klasy Edition. Znajdują się w nim: id, nazwa, oraz długa nazwa z tabeli Edition.
     * @return Edition
     */
    Edition* getEdition()   const {return this->edition;}
    /**
     * @brief
     *  Zwraca obiekt klasy Rarity. Znajdują się w nim: id  oraz wartosc z tabeli Rarity.
     * @return Rarity
     */
    Rarity* getRarity()     const {return this->rarity;}
    /**
     * @brief
     *  Zwraca obiekt klasy Vat. Znajdują się w nim: id, nazwa, oraz długa nazwa z tabeli Edition.
     * @return Vat
     */
    Vat*     getVat()       const {return this->vat;}

    /**
     * @brief
     *
     * @param id
     * @return bool
     */
    bool setId        (QVariant id)           {this->id.setValue(id);                return true;}
    /**
     * @brief
     *
     * @param name
     * @return bool
     */
    bool setName      (QVariant name)         {this->name.setValue(name);            return true;}
    /**
     * @brief
     *
     * @param picURL
     * @return bool
     */
    bool setPicURL    (QVariant picURL)       {this->picURL.setValue(picURL);        return true;}
    /**
     * @brief
     *
     * @param manacost
     * @return bool
     */
    bool setManacost  (QVariant manacost)     {this->manacost.setValue(manacost);    return true;}
    /**
     * @brief
     *
     * @param type
     * @return bool
     */
    bool setType      (QVariant type)         {this->type.setValue(type);            return true;}
    /**
     * @brief
     *
     * @param text
     * @return bool
     */
    bool setText      (QVariant text)         {this->text.setValue(text);            return true;}
    /**
     * @brief
     *
     * @param priceNetto
     * @return bool
     */
    bool setPriceNetto(QVariant priceNetto)   {this->priceNet.setValue(priceNetto);  return true;}
    /**
     * @brief
     *
     * @param id
     * @return bool
     */
    bool setEdition   (QVariant id)           {edition = new Edition(id, *database); return true;}
    /**
     * @brief
     *
     * @param id
     * @return bool
     */
    bool setRarity    (QVariant id)           {rarity  = new Rarity (id, *database); return true;}
    /**
     * @brief
     *
     * @param id
     * @return bool
     */
    bool setVat       (QVariant id)           {this->vat = new Vat(id,*database );   return true;}

    /**
     * @brief
     *
     */
    void updatePrice();

private:
    /**
     * @brief
     *
     * @param id
     * @return QSqlQuery
     */
    QSqlQuery& queryID(QString id);
    /**
     * @brief
     *
     * @param query
     * @return bool
     */
    bool queryToVariable(QSqlQuery &query);
    Database *database; /**< TODO */

    QVariant id; /**< TODO */
    QVariant name; /**< TODO */
    QVariant picURL; /**< TODO */
    QVariant manacost; /**< TODO */
    QVariant type; /**< TODO */
    QVariant text; /**< TODO */
    QVariant priceNet; /**< TODO */
    Edition *edition; /**< TODO */
    Rarity *rarity; /**< TODO */
    Vat *vat; /** Obiekt klasy Vat. Znajdują się w nim: id, nazwa, oraz długa nazwa z tabeli Edition.*/
};


#endif // CARDINFO_H
