#include "parserxmlfromdevice.h"
#include <QDebug>
ParserXmlFromDevice::ParserXmlFromDevice()
{
    collection = new CardsFromXmParsing();
}



/**
 * @brief Konstruktor wywoołuje meotede readFile
 *
 * @param fileXml Ścieżka do pliku xml
 */
ParserXmlFromDevice::ParserXmlFromDevice(QString fileXml)
{
    qDebug() << "ParserXmlFromDevice::ParserXmlFromDevice = " + fileXml;
collection = new CardsFromXmParsing();
   this->readFile(fileXml);


}


/**
 * @brief Funkcja inicjalizujaca parsowanie.
 *
 */
void ParserXmlFromDevice::start(){
    this->readingDocRoot();
    this->readData();

}




 /**
  * @brief
  * Funcja otwierajaca plik oras inicjalizujaca obiekt klasy QDomDocument, który służy do parsowania
  * @param fileName
  * @return bool false - problem z otwarciem pliku
  */
 bool ParserXmlFromDevice::readFile(const QString &fileName)
 {
     QFile file(fileName);
     if (!file.open(QFile::ReadOnly | QFile::Text)) {
         std::string wyjatek = "Nie otworzono pliku. Sprawdz prawa dostepu."; /*<TODO Jakos to trzeba obsłużyć inaczej*/
                         throw wyjatek;
         return false;
     }

     xmlBOM.setContent(&file);
     file.close();
 }

 /**
  * @brief Funcja czyta typ bazy. Należy rozbudować w celu zwracania wartości przez funkcje.
  *
  */
 void ParserXmlFromDevice::readingDocRoot(){


     // Extract the root markup
     root=xmlBOM.documentElement();

     // Get root names and attributes
     QString Type=root.tagName();

     // Display root data
     qDebug() << "Type  = " << Type.toStdString().c_str() ;

 }

 /**
  * @brief Funkcja czytajaca dane. Jeśli natknie się na tag name sets lub cards wywołuje odpowiednie metody.
  *
  */
 void ParserXmlFromDevice::readData(){
    // Get the first child of the root (Markup COMPONENT is expected)
      QDomElement  cards=root.firstChild().toElement();
//     QDomElement cards = sets.nextSibling().toElement();
     // Loop while there is a child
     while(!cards.isNull())
     {
         // Check if the child tag name is COMPONENT
         if (cards.tagName()=="card")
         {
              this->readDataCards(cards);
         }
         // Next component
         cards = cards.nextSibling().toElement();   //<TODO aby potrzebene?
     }
 }


 /**
  * @brief Funkcja wprowadza do struktury cards dane znajdując się miedzy <cards></cards>
  * Struktura wypełniana jest nullami tak aby wszystkie pola struktury posiadały identyczny rozmiar.
  * Dane między znacznikami <color></color> nie są pobierane(Jednak istenieje taka możliwość).
  * @param cards Element z tag name = cards
  */
 void ParserXmlFromDevice::readDataCards(QDomElement cards){

    QDomElement card= cards.firstChild().toElement();
//     while(!card.isNull())
//     {

//        if (card.tagName()=="card")
//        {


            // Get the first child of the component
//            QDomElement Child=card.firstChild().toElement();



            // Read each child of the component node
            while (!card.isNull())
            {

                // Read Name and value
                qDebug() << card.tagName();
               if (card.tagName()=="id")  this->collection->id.append(card.firstChild().toText().data());
               if (card.tagName()=="name")  this->collection->name.append(card.firstChild().toText().data());
               if (card.tagName()=="edition")  this->collection->edition.append(card.firstChild().toText().data());

                // Next child
                card = card.nextSibling().toElement();
            }

// }


    //}
}


CardsFromXmParsing ParserXmlFromDevice::returnData()
{
    return *collection;
}
