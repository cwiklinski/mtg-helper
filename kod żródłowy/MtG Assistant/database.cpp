#include "database.h"
#include<QMessageBox>
#include<QSqlQuery>
#include <QSqlError>

Database::Database()
{
    db = NULL;
}
Database::~Database(){}

void Database::openDB()
{
    db = new QSqlDatabase();
    *db = QSqlDatabase::addDatabase("QSQLITE", QLatin1String( "OneCard-connection" ));

    db->setDatabaseName(qApp->applicationDirPath()
                        + "/CardList.db3");
qDebug() << qApp->applicationDirPath()
            + "/CardList.db3";
    if (!db->open()) {
        QSqlError *err = new QSqlError(db->lastError());
        QMessageBox msgBox;
        msgBox.setText("ERROR: Cannot open Database in path:" + qApp->applicationDirPath()
                       +  "/CardList.db3 \n" "Error details:" + err->text() );
        msgBox.exec();
    }

}
void  Database::closeDB()
{
    QString connection;
    connection=db->connectionName();
    db->close();
    *db = QSqlDatabase();
    QSqlDatabase::database().removeDatabase(connection);
}

