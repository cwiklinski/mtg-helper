#include "listcollection.h"
ListCollection::ListCollection(){}

ListCollection::ListCollection(Database &objDatabase) : dataBase(&objDatabase)
{
    downloadSizeCollectionList();
    refreshList();
}

void ListCollection::downloadSizeCollectionList()
{
    QSqlQuery *query = new QSqlQuery(*dataBase->db);
    query->exec("SELECT count(*) FROM collectionList;");
    query->first();
    this->size = query->value(0).toInt();
    listBoxes = new Collection[this->size];
    delete query;
}


void ListCollection::refreshList()
{
    QSqlQuery *query = new QSqlQuery(*dataBase->db);
    query->exec("SELECT id ,name FROM collectionList;");
    int i(0);
    while (query->next()) {
        listBoxes[i] = Collection(*dataBase);
        listBoxes[i].setId(query->value(0));
        listBoxes[i].setName(query->value(1));
        i++;
    }
    delete query;
}

QStringList ListCollection::getCollectionList()
{
    QList<QString>  boxList;
    QSqlQuery *query = new QSqlQuery(*dataBase->db);
    query->exec("SELECT id ,name FROM collectionList;");

    while (query->next()) {
        boxList<<  query->value(1).toString();
    }
    delete query;
    return boxList;
}

QVariant ListCollection::getIdCollection(QVariant name)
{
    QSqlQuery *query = new QSqlQuery(*dataBase->db);
    QVariant id;
    query->prepare("SELECT id  FROM collectionList WHERE name =:name ;");
    query->bindValue(":name", name.toString());
    query->exec();

    if(!query->first())
        return -1;
    id = query->value(0);
    delete query;

    return id;
}

bool ListCollection::newCollection(QVariant name)
{


    if(getIdCollection(name).toInt()<=0)
    {
        QSqlQuery *query = new QSqlQuery(*dataBase->db);
        QVariant id;
        query->prepare("INSERT INTO collectionList(name) VALUES(:name);");
        query->bindValue(":name", name.toString());
        query->exec();
    }
    else
    {
        QMessageBox msgBox;
        msgBox.setText("Collection with this name arledy exist");
        msgBox.setStandardButtons(QMessageBox::Close);
        msgBox.exec();
        return false;
    }
    return true;
}

bool ListCollection::dropCollection(QVariant name)
{ 
    QSqlQuery *query = new QSqlQuery(*dataBase->db);
    query->prepare("SELECT count(*) FROM collectionCards WHERE id_collectionList = :id_collectionList ;");
    query->bindValue(":id_collectionList", getIdCollection(name).toString());
    query->exec();
    query->first();

    if(query->value(0).toInt())
    {
        QMessageBox msgBox;
        msgBox.setText("Collection is not empty. Remove cards.");
        msgBox.setStandardButtons(QMessageBox::Close);
        msgBox.exec();
    }else
    {
        query->finish();
        query->prepare("DELETE FROM collectionList WHERE name =:name ;");
        query->bindValue(":name", name.toString());
        query->exec();
        delete query;
        return true;
    }
    return false;
}
