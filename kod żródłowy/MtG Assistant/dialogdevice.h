#ifndef DIALOGDEVICE_H
#define DIALOGDEVICE_H

#include <QDialog>
#include <QTcpSocket>
#include <QDebug>
#include  "socket.h"
#include "downloadmanage.h"
#include "parserxmlfromdevice.h"
#include "tableviewdevicecards.h"
#include "database.h"

namespace Ui {
class DialogDevice;
}

/**
 * @brief
 * Klasa tworzy okno do komunikacji z urządzeniem
 */
class DialogDevice : public QDialog
{
    Q_OBJECT

public:
    /**
     * @brief
     * Konstruktor
     * @param dataBaseObj
     * @param parent
     */
    explicit DialogDevice(Database &dataBaseObj,  QWidget *parent = 0);
    /**
     * @brief
     * Destruktor
     */
    ~DialogDevice();

private slots:
    void openFileAndPutIntoTable(QString pFile);
    void replyFinishedForStart(QNetworkReply* reply);
    /**
     * @brief
     * Wciśnięcie przycisku pobrania kart z urządzenia
     */
    void on_pushButtGetCollection_clicked();
    /**
     * @brief
     * Wciśniecie przycisku otworzenia XML z dysku
     */
    void on_pushButtOpenXmlFile_clicked();
    /**
     * @brief
     * Wciśnięcie przycisku przeniesienia kart do sklepu
     */
    void on_pushButtMoveToShop_clicked();
    /**
     * @brief
     * Wciśnięcie przycisku przeniesienia do kolekcji
     */
    void on_pushButtMoveToBox_clicked();


    void on_pushButtonClose_clicked();

private:
   QEventLoop loop;
    void sendStatusRequest();
    void sendLastRequestAndDOwnloadXmlFile();
    QString mAddressToDevice;
    QNetworkAccessManager *manager;
    Ui::DialogDevice *ui; /***/
    Database *dataBase; /* */
    TableViewDeviceCards *mTableViewDeviceCards; /**tabele kart pobranych z urzadzenia */
};

#endif // DIALOGDEVICE_H
