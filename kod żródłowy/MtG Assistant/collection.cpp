#include "collection.h"
Collection::Collection(){}

Collection::Collection(QVariant id,  Database &pDatabase)
    :database(&pDatabase)
{
    queryToVariable(queryID(id.toString()));
}

Collection::Collection(Database &pDatabase) :database(&pDatabase)
{
}
bool Collection::queryToVariable(QSqlQuery &query){
    int debQueryRespCout = 0;
    while (query.next())
    {
        setId(query.value(0));
        setName(query.value(1));
        debQueryRespCout++;
        if(debQueryRespCout>1){
            qDebug() << "Wiecej niż jedna odpowiedz na zapytanie" << query.lastQuery();
            return false;
        }
    } //end while loop

    if(debQueryRespCout < 1){
        qDebug() << "Zero odopwiedzi na zapytanie" << query.lastQuery();
        return false;
    }
    QSqlQuery *temp = &query;
    delete temp;
    return true;
}



QSqlQuery &Collection::queryID(QString id)
{
    QSqlQuery *query = new QSqlQuery(*this->database->db);
    query->prepare("SELECT id,name FROM collectionList WHERE id = :id ;");
    query->bindValue(":id", id);
    query->exec();
    return *query;
}
