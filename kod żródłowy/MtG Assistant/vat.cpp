#include "vat.h"

Vat::Vat()
{
}
Vat::Vat(QVariant arg,Database &objDataBase, int flag){
    dataBase = &objDataBase;
    this->chooseQuery(arg.toString(), flag);
}
Vat::Vat(QString  arg,Database &objDataBase, int flag){
    dataBase = &objDataBase;
    this->chooseQuery(arg, flag);
}
Vat::Vat(int      arg, Database &objDataBase, int flag){
    dataBase = &objDataBase;
    this->chooseQuery(QString::number(arg), flag);
}

void Vat::chooseQuery(QString arg, int flag){

    if(flag == VatID){
        this->queryID(arg);
    }else if (flag == VatValue){
        this->queryValue(arg);
    }else{
        qDebug() << "Wybrano nie poprawna flage";
    }

}

void Vat::queryID(QString id){   
    QSqlQuery *query = new QSqlQuery(*dataBase->db);
    query->prepare("SELECT id, value FROM vat WHERE id = :id ;");
    query->bindValue(":id", id);
    query->exec();
    this->queryToVariable(*query);
    delete query;
}
void Vat::queryValue(QString value){
    QSqlQuery *query = new QSqlQuery(*dataBase->db);
    query->prepare("SELECT id, value FROM vat WHERE value = :value ;");
    query->bindValue(":value", value);
    query->exec();
    this->queryToVariable(*query);
    delete query;
}

bool Vat::queryToVariable(QSqlQuery &query){
    int debQueryRespCout=0;
    while (query.next())
    {
        this->id.setValue(query.value(0));
        this->value.setValue(query.value(1));
        debQueryRespCout++;
        if(debQueryRespCout>1){
            qDebug() << "Wiecej niż jedna odpowiedz na zapytanie"
                     << query.executedQuery();
            return false;
        }
    }
    if(debQueryRespCout < 1){
        qDebug() << "Zero odopwiedzi na zapytanie"
                 << query.executedQuery();
        return false;
    }
    return true;
}

void Vat::uptadeToDB()
{
    QSqlQuery *query = new QSqlQuery(*dataBase->db);
    query->prepare("UPDATE  vat SET value = :value WHERE id = :id ;");
    query->bindValue(":id", getId().toString());
    query->bindValue(":value", getValue().toString());
    query->exec();
}

