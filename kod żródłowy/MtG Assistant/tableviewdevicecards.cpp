#include <memory>
#include "tableviewdevicecards.h"
#include "storecard.h"
#include "listcollection.h"
#include "collectioncard.h"
TableViewDeviceCards::TableViewDeviceCards(QObject *parent) :
    QAbstractTableModel(parent)
{
//    cards= new CardsFromXmParsing();
}

int TableViewDeviceCards::rowCount(const QModelIndex &parent) const
{
    return cards.name.size();
}

int TableViewDeviceCards::columnCount(const QModelIndex &parent) const
{
    return 3;
}

QVariant TableViewDeviceCards::data(const QModelIndex &index, int role) const
{
    QVariant res;

    if (role == Qt::DisplayRole)
    {
        int row = index.row();
        int col = index.column();

        QString text;
        if(col == 0)
          return this->cards.id.at(row).toString();
        if(col == 1)
          return this->cards.name.at(row).toString();
        if(col == 2)
          return this->cards.edition.at(row).toString();


}
    return QVariant();
}
    void TableViewDeviceCards::setCardsToDisplay(CardsFromXmParsing pCards)
    {
        cards = pCards;
    }
QVariant  TableViewDeviceCards::headerData(int section, Qt::Orientation orientation, int role) const{


    if (role != Qt::DisplayRole)
        return QVariant();

    if (orientation == Qt::Horizontal) {
        switch (section) {
        case 0:
            return tr("Id");
        case 1:
            return tr("Name");
        case 2:
            return tr("Edition");


        default:
            return QVariant();
        }
    }
    return QVariant();
}


void TableViewDeviceCards::moveToCollection(QString pCollectionName, Database &dataBase)
{

    ListCollection *lListCollection= new ListCollection(dataBase);
    QString idCollection =  lListCollection->getIdCollection(pCollectionName).toString();


    for(int i=0 ; i<rowCount(); i ++)
    {
//        if(checkBoxTable[i])
//        {
            QVariant name = data(index(i, 1), 0 );
            QVariant edition = data(index(i, 2), 0 );
//wariant1
            CardInfo *lCardInfo = new CardInfo(name.toString(), edition.toString(), dataBase, 2);
            CollectionCard *lCollectionCard = new CollectionCard(dataBase);
               lCollectionCard->setCardInfo(lCardInfo->getId());
               delete lCardInfo;
               lCollectionCard->setDiscount(0);
               lCollectionCard->setQuantity(1);
               lCollectionCard->setCollection(idCollection);
               lCollectionCard->insertToCollectionTbl();
               delete lCollectionCard;



//        }
    }

}



void TableViewDeviceCards::moveToStore(Database &dataBase)
{


 qDebug() << "wejscie";
 QVariant name ;
 QVariant edition;
CardInfo *lCardInfo;
StoreCard *shopCard;
    for(int i=0 ; i<rowCount(); i ++)
    {
        qDebug() << "petla";
//        if(checkBoxTable[i])
//        {
            name = TableViewDeviceCards::data(index(i, 1), 0 );
            edition = TableViewDeviceCards::data(index(i, 2), 0 );
            qDebug() << name << edition;

  //wariant2
            lCardInfo = new CardInfo(name.toString(), edition.toString(), dataBase ,2);
            qDebug()<< "id Card Info: "<< lCardInfo->getId();
              qDebug()<<lCardInfo->getName();

            shopCard = new StoreCard(lCardInfo->getId(), dataBase);
     qDebug()<<shopCard ->getCardInfo()->getName().toString() << "ze shpocard";
     qDebug()<<shopCard ->getCardInfo()->getId().toString() << "ze shpocard ID";
            shopCard->setQuantity(shopCard->getQuantity().toInt()+1);
            shopCard->setDestription("pusto");
            shopCard->upClassToDb();
            delete lCardInfo;
            delete shopCard;
//        }
    }
}

void TableViewDeviceCards::clearTable(){
    cards.edition.clear();
    cards.id.clear();
    cards.name.clear();
}
