
#include "dialogstorecardsell.h"
#include "ui_dialogstorecardsell.h"
#include "downloadmanage.h"

dialogstoreCardSell::dialogstoreCardSell(StoreCard &_shopCard, Database &dataBase, QWidget *parent ) :
    QDialog(parent), dataBase(&dataBase), shopCard(&_shopCard), ui(new Ui::dialogstoreCardSell )
{
    ui->setupUi(this);
    setWindowTitle(shopCard->getCardInfo()->getName().toString());

    if (shopCard->getQuantity().toInt()<=0)
    {
        ui->buttonSell->setEnabled(false);
        ui->buttonInsertIntoCollection->setEnabled(false);
    }
    ui->labelName ->setText (shopCard->getCardInfo()->getName().toString());
    ui->labelEdition ->setText(shopCard->getCardInfo()->getEdition()->getLongname().toString());
    ui->spinBoxQuantity ->setMaximum(shopCard->getQuantity().toInt());
    ui->linePriceNet->setText(shopCard->getCardInfo()->getPriceNet().toString());
    ui->linePriceGross->setText(shopCard->getCardInfo()->getPriceGross().toString());
    this->on_spinBoxQuantity_valueChanged("1");

    ListCollection *listBoxes = new ListCollection(dataBase);

    QStringList list(listBoxes->getCollectionList());
    ui->comboSelectBox->addItems(list);


    QString lPathToPic(qApp->applicationDirPath().replace(QString("/"),QString("\\")) + "\\images\\cards\\" + shopCard->getCardInfo()->getEdition()->getName().toString() + "\\" + shopCard->getCardInfo()->getName().toString() + ".jpg");
    QPixmap lPic;
    if( lPic.load(lPathToPic)){
        ui->PIC->setPixmap(lPic);
    }else{
        DownloadManager *dm = new DownloadManager();
        dm->setFileName(shopCard->getCardInfo()->getEdition()->getName().toString() + "\\" + shopCard->getCardInfo()->getName().toString() );
        connect(dm, SIGNAL(overDownload(QString )), this, SLOT(setPicture()) ) ;
        dm->setFile(shopCard->getCardInfo()->getPicURL().toString() ,DownloadManager::ePicFile);
    }
     ui->PIC->setScaledContents(true);
}

void dialogstoreCardSell::setPicture()
{
    QPixmap lPic;
    QString lPathToPic(qApp->applicationDirPath().replace(QString("/"),QString("\\")) + "\\images\\cards\\" + shopCard->getCardInfo()->getEdition()->getName().toString() + "\\" + shopCard->getCardInfo()->getName().toString() + ".jpg");
    lPic.load(lPathToPic);
    ui->PIC->setPixmap(lPic);
}

dialogstoreCardSell::~dialogstoreCardSell()
{
    delete ui;
}

//void dialogstoreCardSell::on_groupBoxDiscount_clicked(bool checked)
//{
//    qDebug() << "test";
//    if(checked){
//        ui->spinBoxDiscount->setEnabled(true);
////        ui->lineFinalPriceNet->setEnabled(true);
////        ui-> linePriceGrossAfterDiscount->setEnabled(true);
//    }else{

//        ui->spinBoxDiscount->setEnabled(false);
////        ui->linePriceNetAfterDiscount->setEnabled(false);
////        ui-> linePriceGrossAfterDiscount->setEnabled(false);
//    }
//}

void dialogstoreCardSell::on_spinBoxQuantity_valueChanged(const QString &arg1)
{
    float res = arg1.toFloat()*shopCard->getCardInfo()->getPriceNet().toFloat();
    ui->lineFinalPriceNet->setText(QString::number(res, 2, 2));
    res = arg1.toFloat()*shopCard->getCardInfo()->getPriceGross().toFloat();
   ui->lineFinalPriceGross->setText(QString::number(res, 2, 2));
}

//void dialogstoreCardSell::on_spinBoxDiscount_editingFinished()
//{
//    float res =ui->lineFinalPriceGross->text().toFloat() - (ui->lineFinalPriceGross->text().toFloat() * ui->spinBoxDiscount->value()/100.0 );
//    ui->linePriceGrossAfterDiscount->setText(QString::number(res, 2, 2));
//    res =ui->lineFinalPriceNet->text().toFloat() - ( ui->lineFinalPriceNet->text().toFloat() * ui->spinBoxDiscount->value()/100.0) ;
//    ui->linePriceNetAfterDiscount->setText(QString::number(res, 2, 2));
//}

//void dialogstoreCardSell::on_linePriceNetAfterDiscount_editingFinished()
//{

//    float res = ui->lineFinalPriceNet->text().toFloat() + ui->lineFinalPriceNet->text().toFloat() * shopCard->getCardInfo()->getVat()->getValue().toFloat()/100;
//    ui->linePriceGrossAfterDiscount->setText(QString::number(res, 2, 2));
//    res =100 -  (ui->lineFinalPriceNet->text().toFloat()* 100/ui->lineFinalPriceNet->text().toFloat());
//    ui->spinBoxDiscount->setValue(res);
//}

//void dialogstoreCardSell::on_linePriceGrossAfterDiscount_editingFinished()
//{
//    float res = 100 - (   ui->lineFinalPriceNet->text().toFloat()*100.0/ui->lineFinalPriceGross->text().toFloat());
//    ui->spinBoxDiscount->setValue(res);
//    res =  ui->lineFinalPriceNet->text().toFloat() / (shopCard->getCardInfo()->getVat()->getValue().toFloat()/100 +1) ;
//    ui->linePriceNet->setText(QString::number(res, 2, 2));
//}

void dialogstoreCardSell::on_buttonInsertIntoCollection_clicked()
{
    int amountInShopAfterSell = shopCard->getQuantity().toInt() - ui->spinBoxQuantity->text().toInt();
    shopCard->setQuantity(amountInShopAfterSell);
    shopCard->upClassToDb();

    CollectionCard *insertingCardToBox = new CollectionCard(*dataBase);
    ListCollection *listBoxes = new ListCollection(*dataBase);

    insertingCardToBox->setQuantity(ui->spinBoxQuantity->text());
    insertingCardToBox->setCardInfo(shopCard->getCardInfo()->getId());
    insertingCardToBox->setCollection(
                listBoxes->getIdCollection(
                    ui->comboSelectBox->currentText()));
    insertingCardToBox->setDiscount(ui->spinBoxDiscount->text());
    insertingCardToBox->insertToCollectionTbl();

    delete insertingCardToBox;
    delete listBoxes;
    close();
}



void dialogstoreCardSell::on_buttonSell_clicked()
{
        shopCard->sell(ui->spinBoxQuantity->text().toInt(), ui->spinBoxDiscount->text());
        close();
}

void dialogstoreCardSell::on_buttonCancel_clicked()
{
    close();
}



void dialogstoreCardSell::on_groupBoxSale_toggled(bool checked)
{
        if(checked){
            ui->spinBoxDiscount->setEnabled(true);
    //        ui->lineFinalPriceNet->setEnabled(true);
    //        ui-> linePriceGrossAfterDiscount->setEnabled(true);
        }else{

            ui->spinBoxDiscount->setEnabled(false);
    //        ui->linePriceNetAfterDiscount->setEnabled(false);
    //        ui-> linePriceGrossAfterDiscount->setEnabled(false);
        }
}

void dialogstoreCardSell::on_spinBoxDiscount_valueChanged(double arg1)
{
        float res =ui->lineFinalPriceGross->text().toFloat() - (ui->lineFinalPriceGross->text().toFloat() * ui->spinBoxDiscount->value()/100.0 );
        ui->linePriceGrossAfterDiscount->setText(QString::number(res, 2, 2));
        res =ui->lineFinalPriceNet->text().toFloat() - ( ui->lineFinalPriceNet->text().toFloat() * ui->spinBoxDiscount->value()/100.0) ;
        ui->linePriceNetAfterDiscount->setText(QString::number(res, 2, 2));
}
