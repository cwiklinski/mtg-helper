#ifndef BOX_H
#define BOX_H
#include <QVariant>
#include<QSqlQuery>
#include "dataBase.h"
#include "memory"

/**
 * @brief
 * Klasa reprezentująca pojedynczy rekord z tabeli collection
 */
class Collection
{
public:
    /**
     * @brief
     * Konstrktor
     */
    Collection();
    /**
     * @brief
     * Konstruktor z podaniem obiektu bazy danych z którego będzie korzystać klasa. Baza musi być otwarta.
     * @param pDatabase
     */
    Collection(Database &pDatabase);
    /**
     * @brief
     * Konstruktor z uzupełnieniem obiektu.
     * @param id
     * @param pDatabase
     */
    Collection(QVariant id,  Database &pDatabase);

    /**
     * @brief
     *
     * @param id
     * @return bool
     */
    bool setId  (QVariant id)  {this->id.setValue(id); return true;}
    /**
     * @brief
     *
     * @param name
     * @return bool
     */
    bool setName(QVariant name){this->name.setValue(name); return true;}

    /**
     * @brief
     *
     * @return QVariant
     */
    QVariant getId()   const{return this->id;}
    /**
     * @brief
     *
     * @return QVariant
     */
    QVariant getName() const{return this->name;}

private:
    QVariant id; /**id kolekcji */
    QVariant name; /**nazwa kolekcji */

    Database *database; /***/

    /**
     * @brief
     *
     * @param id id z tabeli collection
     * @return QSqlQuery zwraca odpoweidz od bazy danych
     */
    QSqlQuery& queryID(QString id);
    /**
     * @brief
     * Funkcja uzupełnia klase o odpowiedz z bazy danych
     * @param query
     * @return bool
     */
    bool queryToVariable(QSqlQuery &query);



};

#endif // BOX_H
