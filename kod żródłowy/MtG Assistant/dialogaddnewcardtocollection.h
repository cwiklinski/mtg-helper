#ifndef DIALOGADDNEWCARDTOCOLLECTION_H
#define DIALOGADDNEWCARDTOCOLLECTION_H
#include "proxymodelstorecards.h"
#include "tabelviewstorecards.h"
#include "database.h"
#include "listcollection.h"
#include "collectioncard.h"
#include <QDialog>


namespace Ui {
class DialogAddNewCardToCollection;
}

/**
 * @brief
 * Klasa tworząca okno umożliwiajace dodawanie kart do kolekcji
 */
class DialogAddNewCardToCollection : public QDialog
{
    Q_OBJECT

public:

    /**
     * @brief
     * Kontruktor
     * @param dataBase
     * @param parent
     */
    explicit DialogAddNewCardToCollection(Database &dataBase, QWidget *parent = 0);
    /**
     * @brief
     * Destruktor
     */
    ~DialogAddNewCardToCollection();
     ProxyModelStoreCards *proxyModel; /**< TODO */
     TabelViewStoreCards *model; /**< TODO */
     Database *dataBase; /**< TODO */
private slots:
    /**
     * @brief
     * Wciśnięcie przycisku "Finish"
     */
    void on_buttFinish_clicked();
    /**
     * @brief
     * Zmiana nazwy wyszukiwanej karty
     */
    void nameFilterChanged();

    /**
     * @brief
     * Wciśnięcie przycisku "Add card:
     */
    void on_buttAddCard_clicked();

private:
    Ui::DialogAddNewCardToCollection *ui; /***/

signals:

    void dataChanged();
};

#endif // DIALOGADDNEWCARDTOCOLLECTION_H
