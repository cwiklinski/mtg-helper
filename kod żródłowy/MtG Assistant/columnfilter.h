#ifndef COLUMNFILTER_H
#define COLUMNFILTER_H

#include <QDialog>

namespace Ui {
class ColumnFilter;
}

/**
 * @brief
 * Klasa odpowiedzialna za wyświetlania okna umożliwiającego wybór kolumn prezentowanych w tabeli collection cards.
 */
class ColumnFilter : public QDialog
{
    Q_OBJECT

public:
    /**
     * @brief
     * Konstruktor
     * @param columnFilter
     * @param parent
     */
    explicit ColumnFilter(unsigned int &columnFilter, QWidget *parent = 0);
    /**
     * @brief
     * Destruktor
     */
    ~ColumnFilter();

private slots:
    /**
     * @brief
     * Wciśnięcie przycisku accept
     */
    void on_buttonBox_accepted();

private:
    unsigned int *filter; /** */
    Ui::ColumnFilter *ui; /** */
};

#endif // COLUMNFILTER_H
