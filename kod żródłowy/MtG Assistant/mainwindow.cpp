#include "mainwindow.h"
#include "ui_mainwindow.h"
#include"listcollection.h"
#include "ListEditions.h"
#include "listrarity.h"
#include "collectioncard.h"
#include "memory.h"
#include  <QSqlRelationalTableModel>
#include "tabelViewStoreCards.h"
#include "tableViewCollectionCardsmodel.h"
 #include <QSqlError>
MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{  
    ui->setupUi(this);
    setWindowTitle("MtG assistant");
    showMaximized();
    setStyle();
    dataBase->openDB();
    try{
        initialization();

    }catch(...)
    {

    }
}

MainWindow::~MainWindow()
{
    dataBase->closeDB();
    delete ui;
}

void MainWindow::nameFilterChanged()
{ 
    proxyModelStoreCards->checkW = ui->checkBoxW->isChecked();
    proxyModelStoreCards->checkG = ui->checkBoxG->isChecked();
    proxyModelStoreCards->checkU = ui->checkBoxU->isChecked();
    proxyModelStoreCards->checkR = ui->checkBoxR->isChecked();
    proxyModelStoreCards->checkB = ui->checkBoxB->isChecked();
    proxyModelStoreCards->checkArtifact = ui->checkBoxArtifact->isChecked();
    proxyModelStoreCards->checkLand = ui->checkBoxLand->isChecked();
    proxyModelStoreCards->rarity = ui->comboBoxRarity->currentText();
    proxyModelStoreCards->editionLongname = ui->comboBoxEdition->currentText();
    proxyModelStoreCards->checkCardInShop = ui->checkBoxCardInStore->isChecked();

    QRegExp regExp(ui->lineFindNameInStore->text().toUpper(),  Qt::CaseSensitive , QRegExp::RegExp);
    proxyModelStoreCards->setFilterRegExp(regExp);
}


void MainWindow::nameFilterChangedInCollection()
{   
    proxyModelCollectionCards->checkW = ui->checkBoxWInBox->isChecked();
    proxyModelCollectionCards->checkG = ui->checkBoxGInBox->isChecked();
    proxyModelCollectionCards->checkU = ui->checkBoxUInBox->isChecked();
    proxyModelCollectionCards->checkR = ui->checkBoxRInBox->isChecked();
    proxyModelCollectionCards->checkB = ui->checkBoxBInBox->isChecked();
    proxyModelCollectionCards->checkArtifact = ui->checkBoxArtifactInBox->isChecked();
    proxyModelCollectionCards->checkLand = ui->checkBoxLandInBox->isChecked();
    proxyModelCollectionCards->rarity = ui->comboBoxRarityInBox->currentText();
    proxyModelCollectionCards->editionLongname = ui->comboBoxEditionInBox->currentText();
    proxyModelCollectionCards->collectionName = ui->comboBoxSelectCollection->currentText();

    QRegExp regExp(ui->lineFindNameInCollection->text().toUpper(),  Qt::CaseSensitive , QRegExp::RegExp);
    proxyModelCollectionCards->setFilterRegExp(regExp);
}



void MainWindow::on_tableViewStoreCards_doubleClicked(const QModelIndex &index)
{    
    QModelIndex indexSource(proxyModelStoreCards->mapToSource(index));
    int const zeroElementArray = 1;
    StoreCard *cardToDisplay= new StoreCard(indexSource.row()+zeroElementArray ,*dataBase);
    dialogstoreCardSell   *itc= new dialogstoreCardSell  (*cardToDisplay, *dataBase,this);
    itc->exec();
    modelStore->refresh(*dataBase);
    modelCollection->refresh(*dataBase);
}

void MainWindow::on_buttMoveToCollection_clicked()
{

    ListCollection *chooseBox = new ListCollection(*dataBase);
    QString text = QInputDialog::getItem(this, "Choose collection", "Choose collection", chooseBox->getCollectionList(),0,false);
    QVariant idBox = chooseBox->getIdCollection(text);
    proxyModelCollectionCards->moveToCollection(idBox, *dataBase);
    delete chooseBox;
    modelCollection->refresh(*dataBase);
}

void MainWindow::on_buttMarkAll_clicked()
{
    proxyModelCollectionCards->setCheckboxCointener(true);
    modelCollection->refresh(*dataBase);
}

void MainWindow::on_buttUnMarkAll_clicked()
{
    proxyModelCollectionCards->setCheckboxCointener(false);
    modelCollection->refresh(*dataBase);
}

void MainWindow::on_buttRemove_clicked()
{
    QMessageBox msgBox(this);
    msgBox.setText("Remove?");
    msgBox.setInformativeText("Are you sure?");
    msgBox.setStandardButtons(QMessageBox::Yes | QMessageBox::No);
    msgBox.setDefaultButton(QMessageBox::Save);
    int ret = msgBox.exec();
    if(ret ==QMessageBox::Yes ){
        proxyModelCollectionCards->deleteCard(*dataBase);
        modelCollection->refresh(*dataBase);
    }
}

void MainWindow::on_buttSell_clicked()
{
    QMessageBox msgBox(this);
    msgBox.setText("Sell?");
    msgBox.setInformativeText("Are you sure?");
    msgBox.setStandardButtons(QMessageBox::Yes | QMessageBox::No);
    msgBox.setDefaultButton(QMessageBox::Save);
    int ret = msgBox.exec();
    if(ret ==QMessageBox::Yes ){
        proxyModelCollectionCards->sellCard(*dataBase);
        modelCollection->refresh(*dataBase);
        proxyModelCollectionCards->setCheckboxCointener(false);
    }
}

void MainWindow::on_buttMoveToStore_clicked()
{
    QMessageBox msgBox(this);
    msgBox.setText("Move to store?");
    msgBox.setInformativeText("Are you sure?");
    msgBox.setStandardButtons(QMessageBox::Yes | QMessageBox::No);
    msgBox.setDefaultButton(QMessageBox::Save);
    int ret = msgBox.exec();
    if(ret ==QMessageBox::Yes ){
        dataBase->db->transaction();
        proxyModelCollectionCards->moveToShop(*dataBase);
        dataBase->db->commit();
        modelCollection->refresh(*dataBase);
        modelStore->refresh(*dataBase);
        proxyModelCollectionCards->setCheckboxCointener(false);
    }
}


void MainWindow::on_buttColumnFilter_clicked()
{
    unsigned int columnFilter = 0;
    if(!ui->tableViewCollectionCards->isColumnHidden(1))
        columnFilter |= 1 ;

    if(!ui->tableViewCollectionCards->isColumnHidden(2))
        columnFilter |= 2 ;
    if(!ui->tableViewCollectionCards->isColumnHidden(4))
        columnFilter |= 4 ;
    if(!ui->tableViewCollectionCards->isColumnHidden(5))
        columnFilter |= 8 ;
    if(!ui->tableViewCollectionCards->isColumnHidden(6))
        columnFilter |= 16 ;
    if(!ui->tableViewCollectionCards->isColumnHidden(7))
        columnFilter |= 32 ;
    if(!ui->tableViewCollectionCards->isColumnHidden(8))
        columnFilter |= 64;
    if(!ui->tableViewCollectionCards->isColumnHidden(9))
        columnFilter |= 128;
    if(!ui->tableViewCollectionCards->isColumnHidden(10))
        columnFilter |= 256;
    if(!ui->tableViewCollectionCards->isColumnHidden(11))
        columnFilter |= 512 ;
    if(!ui->tableViewCollectionCards->isColumnHidden(12))
        columnFilter |= 1024;
    if(!ui->tableViewCollectionCards->isColumnHidden(13))
        columnFilter |= 2048 ;
    if(!ui->tableViewCollectionCards->isColumnHidden(14))
        columnFilter |= 4096;
    qDebug() <<    &columnFilter;
    ColumnFilter *columnFilterDialog = new ColumnFilter(columnFilter);
    if(columnFilterDialog->exec()){


        if(columnFilter & 1 )
            ui->tableViewCollectionCards->hideColumn(1);
        else ui->tableViewCollectionCards->showColumn(1);
        if(columnFilter & 2 )
            ui->tableViewCollectionCards->hideColumn(2);
        else ui->tableViewCollectionCards->showColumn(2);
        if(columnFilter & 4 )
            ui->tableViewCollectionCards->hideColumn(4);
        else ui->tableViewCollectionCards->showColumn(4);
        if(columnFilter & 8 )
            ui->tableViewCollectionCards->hideColumn(5);
        else ui->tableViewCollectionCards->showColumn(5);
        if(columnFilter & 16 )
            ui->tableViewCollectionCards->hideColumn(6);
        else ui->tableViewCollectionCards->showColumn(6);
        if(columnFilter & 32 )
            ui->tableViewCollectionCards->hideColumn(7);
        else ui->tableViewCollectionCards->showColumn(7);
        if(columnFilter & 64 )
            ui->tableViewCollectionCards->hideColumn(8);
        else ui->tableViewCollectionCards->showColumn(8);
        if(columnFilter & 128 )
            ui->tableViewCollectionCards->hideColumn(9);
        else ui->tableViewCollectionCards->showColumn(9);
        if(columnFilter & 256 )
            ui->tableViewCollectionCards->hideColumn(10);
        else ui->tableViewCollectionCards->showColumn(10);
        if(columnFilter & 512 )
            ui->tableViewCollectionCards->hideColumn(11);
        else ui->tableViewCollectionCards->showColumn(11);
        if(columnFilter & 1024 )
            ui->tableViewCollectionCards->hideColumn(12);
        else ui->tableViewCollectionCards->showColumn(12);
        if(columnFilter & 2048 )
            ui->tableViewCollectionCards->hideColumn(13);
        else ui->tableViewCollectionCards->showColumn(13);
        if(columnFilter & 4096 )
            ui->tableViewCollectionCards->hideColumn(14);
        else ui->tableViewCollectionCards->showColumn(14);


    }

}



void MainWindow::on_buttNewCollection_clicked()
{
    std::unique_ptr<ListCollection> chooseBox (new ListCollection(*dataBase));
    QString text = QInputDialog::getText(this, "Create new collection", "Enter the name of the collection:");

    if(!text.isEmpty())
    {
        chooseBox->newCollection(text);
        ui->comboBoxSelectCollection->addItem(text);
        modelCollection->refresh(*dataBase);
    }else
    {
        QMessageBox msgBox(this);
        msgBox.setText("You can not create collection without name.");
        msgBox.exec();
    }
}

void MainWindow::on_buttRemoveCollection_clicked()
{
    ListCollection *chooseBox = new ListCollection(*dataBase);
    bool ok = false;
    QString text = QInputDialog::getItem(this, "Select collection to remove", "Select collection to remove (remember, the collection can not contain cards)", chooseBox->getCollectionList(),0,true, &ok);
    if(ok){
        chooseBox->dropCollection(text);
        ui->comboBoxSelectCollection->clear();
        ListCollection *listBoxes = new ListCollection(*dataBase);
        QStringList listBoxString = (listBoxes->getCollectionList());
        listBoxString.prepend("ALL");
        ui->comboBoxSelectCollection->addItems(listBoxString);
        delete listBoxes;
    }
    delete chooseBox;
}

void MainWindow::on_buttAddNewCardToCollection_clicked()
{
    DialogAddNewCardToCollection *dialog = new DialogAddNewCardToCollection(*dataBase,this); //tutauj postanie obiekt dialog
    connect(dialog,SIGNAL(dataChanged()), this, SLOT(on_newCardIsAdded()));
    dialog->exec();
    delete dialog;
    modelCollection->refresh(*dataBase);
}


void MainWindow::initialization() {


    std::unique_ptr<ListEditions> listEditions(new ListEditions(*dataBase));
    QStringList listEditionsString = (listEditions->getEditionLongNameList());
    listEditionsString.prepend("");
    ui->comboBoxEdition->addItems(listEditionsString);

    std::unique_ptr<ListRarity> listRarity ( new ListRarity(*dataBase));
    QStringList listRarityString = listRarity->getRarityList();
    listRarityString.prepend("");
    ui->comboBoxRarity->addItems(listRarityString);

    ///////////collection////////////////////
    ui->comboBoxEditionInBox->addItems(listEditionsString);
    ui->comboBoxRarityInBox->addItems(listRarityString);

    std::unique_ptr<ListCollection> listCollection (new ListCollection(*dataBase));
    QStringList listBoxString = (listCollection->getCollectionList());
    listBoxString.prepend("ALL");
    ui->comboBoxSelectCollection->addItems(listBoxString);
    /////////////////////////////////////store///////////////////////
    modelStore = new  TabelViewStoreCards();
    modelStore->refresh(*dataBase);
    qDebug()<< modelStore->lastError();

    proxyModelStoreCards  = new  ProxyModelStoreCards(this);
    proxyModelStoreCards ->setSourceModel(modelStore);
    proxyModelStoreCards ->setDynamicSortFilter(true);

    ui->tableViewStoreCards->setModel(proxyModelStoreCards);
    ui-> tableViewStoreCards->setSortingEnabled(true);
    ui->tableViewStoreCards->setSelectionBehavior(QAbstractItemView::SelectRows);
    ui->tableViewStoreCards->horizontalHeader()->setStretchLastSection(true);
    ui->tableViewStoreCards->verticalHeader()->hide();
    ui->tableViewStoreCards->setEditTriggers(QAbstractItemView::NoEditTriggers);
    ui->tableViewStoreCards->setSelectionMode(QAbstractItemView::SingleSelection);
    ui->tableViewStoreCards->hideColumn(0); //schowaj card id
    ui->tableViewStoreCards->setContextMenuPolicy(Qt::CustomContextMenu);
    ui->tableViewStoreCards->resizeColumnsToContents();
    ui->tableViewStoreCards->horizontalHeader()->setHighlightSections(false);

    connect(ui->lineFindNameInStore, SIGNAL(textChanged(QString)),
            this, SLOT(nameFilterChanged() ));
    connect(ui->checkBoxW, SIGNAL(clicked()),
            this, SLOT(nameFilterChanged() ));
    connect(ui->checkBoxU, SIGNAL(clicked()),
            this, SLOT(nameFilterChanged() ));
    connect(ui->checkBoxG, SIGNAL(clicked()),
            this, SLOT(nameFilterChanged() ));
    connect(ui->checkBoxR, SIGNAL(clicked()),
            this, SLOT(nameFilterChanged() ));
    connect(ui->checkBoxB, SIGNAL(clicked()),
            this, SLOT(nameFilterChanged() ));
    connect(ui->checkBoxArtifact, SIGNAL(clicked()),
            this, SLOT(nameFilterChanged() ));
    connect(ui->checkBoxLand, SIGNAL(clicked()),
            this, SLOT(nameFilterChanged() ));
    connect(ui->comboBoxEdition, SIGNAL(currentIndexChanged(int)),
            this, SLOT(nameFilterChanged() ));
    connect(ui->comboBoxRarity, SIGNAL(currentIndexChanged(int)),
            this, SLOT(nameFilterChanged() ));
    connect(ui->checkBoxCardInStore, SIGNAL(clicked()),
            this, SLOT(nameFilterChanged() ));
    ///////////////////////////collections///////////////////////////////
    modelCollection = new  TableViewCollectionCardsModel();
    modelCollection->refresh(*dataBase);
    proxyModelCollectionCards  = new  ProxyModelCollectionCards(this);
    proxyModelCollectionCards ->setSourceModel(modelCollection);
    proxyModelCollectionCards ->setDynamicSortFilter(true);
    proxyModelCollectionCards->setCheckboxCointener();

    ui->tableViewCollectionCards->setModel(proxyModelCollectionCards);
    ui-> tableViewCollectionCards->setSortingEnabled(true);
    ui->tableViewCollectionCards->setSelectionBehavior(QAbstractItemView::SelectRows);
    ui->tableViewCollectionCards->horizontalHeader()->setStretchLastSection(true);
    ui->tableViewCollectionCards->verticalHeader()->hide();
    ui->tableViewCollectionCards->setEditTriggers(QAbstractItemView::NoEditTriggers);
    ui->tableViewCollectionCards->setSelectionMode(QAbstractItemView::SingleSelection);
    ui->tableViewCollectionCards->hideColumn(15); //schowa box id
    ui->tableViewCollectionCards->hideColumn(14); //schowaj text karty
    ui->tableViewCollectionCards->hideColumn(1);// schowaj skrocona nazwa edycji
     ui->tableViewCollectionCards->resizeColumnsToContents();
     ui->tableViewCollectionCards->horizontalHeader()->setHighlightSections(false);

    connect(ui->lineFindNameInCollection, SIGNAL(textChanged(QString)),
            this, SLOT(nameFilterChangedInCollection() ));
    connect(ui->checkBoxWInBox, SIGNAL(clicked()),
            this, SLOT(nameFilterChangedInCollection() ));
    connect(ui->checkBoxUInBox, SIGNAL(clicked()),
            this, SLOT(nameFilterChangedInCollection() ));
    connect(ui->checkBoxGInBox, SIGNAL(clicked()),
            this, SLOT(nameFilterChangedInCollection() ));
    connect(ui->checkBoxRInBox, SIGNAL(clicked()),
            this, SLOT(nameFilterChangedInCollection() ));
    connect(ui->checkBoxBInBox, SIGNAL(clicked()),
            this, SLOT(nameFilterChangedInCollection() ));
    connect(ui->checkBoxArtifactInBox, SIGNAL(clicked()),
            this, SLOT(nameFilterChangedInCollection() ));
    connect(ui->checkBoxLandInBox, SIGNAL(clicked()),
            this, SLOT(nameFilterChangedInCollection() ));
    connect(ui->comboBoxEditionInBox, SIGNAL(currentIndexChanged(int)),
            this, SLOT(nameFilterChangedInCollection() ));
    connect(ui->comboBoxRarityInBox, SIGNAL(currentIndexChanged(int)),
            this, SLOT(nameFilterChangedInCollection() ));
    connect(ui->comboBoxSelectCollection, SIGNAL(currentIndexChanged(int)),
            this, SLOT(nameFilterChangedInCollection() ));
    connect(ui->tableViewStoreCards, SIGNAL(customContextMenuRequested(const QPoint&)),
            this, SLOT(ShowContextMenu(const QPoint&)));
    // status Bar
    connect(proxyModelCollectionCards, SIGNAL(dataChanged()),
            this, SLOT(statusBar()));
    statusBar();

}

void MainWindow::on_buttDeviceConn_clicked()
{
    DialogDevice *dialogDevice = new DialogDevice(*dataBase, this);
    dialogDevice->exec();
    delete dialogDevice ;
}

void MainWindow::statusBar()
{
    float *array = proxyModelCollectionCards->stateBar();
    ui->lineEditBoxesQuantity->setText(QString::number(array[0]));
    ui->lineEditBoxesPriceNetto->setText(QString::number(array[1]));
    ui->lineEditBoxesSaleNetto->setText(QString::number(array[2]));
    ui->lineEditBoxesPriceBrutto->setText(QString::number(array[3]));
    ui->lineEditBoxesSaleBrutto->setText(QString::number(array[4]));
}
void MainWindow::ShowContextMenu(const QPoint& pos) // this is a slot
{
    QPoint globalPos = ui->tableViewStoreCards->mapToGlobal(pos);

    QMenu myMenu;
    myMenu.addAction("Sell/Insert to collection");
    myMenu.addAction("Edit state");
 QModelIndex indexSource(proxyModelStoreCards->mapToSource(ui->tableViewStoreCards->currentIndex()));
    if(QAction* selectedItem = myMenu.exec(globalPos))
{
    int const zeroElementArray(1);
    if (selectedItem->text()=="Edit state")
    {
        DialogDetailShopCard *dc = new DialogDetailShopCard(indexSource.row()/*+zeroElementArray*/, *dataBase , this);
        dc->exec();
    }
    else if(selectedItem->text()=="Sell/Insert to collection")
    {
        StoreCard *cardToDisplay= new StoreCard(indexSource.row()+zeroElementArray ,*dataBase);
       dialogstoreCardSell    *itc= new dialogstoreCardSell  (*cardToDisplay, *dataBase, this);
        itc->exec();
    }
    }
    modelStore->refresh(*dataBase);
    modelCollection->refresh(*dataBase);
}



void MainWindow::on_actionClose_triggered()
{
    close();
}

void MainWindow::on_actionRemoveCollection_triggered()
{
    on_buttRemoveCollection_clicked();
}

void MainWindow::on_actionNewCollection_triggered()
{
    on_buttNewCollection_clicked();
}

void MainWindow::on_actionCollectionFilter_triggered()
{
    on_buttColumnFilter_clicked();
}

void MainWindow::on_actionAdd_cards_from_device_triggered()
{
    on_buttDeviceConn_clicked();
}

void MainWindow::on_actionAbout_triggered()
{
    QMessageBox msgBox;
    msgBox.about(this,"MtgHelper v1.0 about", " MtgHelper v1.0 with SQLite 3.5.1 and Qt 5.2.1 \n \n Application create with use: Qt 5.2.1 QT Creator 3.0.1. \n\n Autor: Wojciech Ćwiklińśki cwiklinski@gmail.com \n With special thanks to my promotor dr inż. Artur Opaliński");
}

void MainWindow::on_actionView_history_sold_cards_triggered()
{
    DialogSoldCards *dialogSoldCards = new DialogSoldCards(*dataBase, this);
        dialogSoldCards->exec();
}

void MainWindow::on_actionVat_triggered()
{


    Vat *vat = new Vat(1, *dataBase);
     int vatNew = QInputDialog::getInt(this, "Change vat ", "Put the vat value[%]", vat->getValue().toInt(),0, 100);
    vat->setValue(vatNew);
    vat->uptadeToDB();
    modelCollection->refresh(*dataBase);
    modelStore->refresh(*dataBase);
}

void MainWindow::on_actionCreate_csv_file_triggered()
{
    std::unique_ptr<DialogEksportToCsv> dialog(new DialogEksportToCsv(*dataBase, this));
    dialog->exec();
}

void MainWindow::setStyle()
{
    QFile file("./style.txt");

    if(file.open(QIODevice::ReadOnly))
    {

        QTextStream instream(&file);
        QString line;
        QString style;
        do {
            line = ( instream.readLine());
            style.append(line);
        } while (!line.isNull());
        file.close();
        setStyleSheet(style);
    }else{
    QMessageBox msgBox(this);
    msgBox.setText("Error of style file.");
    msgBox.exec();
    }

}

void MainWindow::on_newCardIsAdded()
{
    modelCollection->refresh(*dataBase); modelCollection->refresh(*dataBase);
}

void MainWindow::on_actionShow_short_edtion_name_triggered(bool checked)
{
    if(checked)
        ui->tableViewStoreCards->showColumn(2);
    else
        ui->tableViewStoreCards->hideColumn(2);
}




