#ifndef MAINWINDOW_H
#define MAINWINDOW_H
#include"cardinfo.h"
#include "storecard.h"
#include "listcollection.h"
#include <QSortFilterProxyModel>
#include "database.h"
#include "proxymodelcollectioncards.h"
#include "proxymodelstorecards.h"
#include <QMainWindow>
#include "listeditions.h"
#include "listrarity.h"
#include "dialogdetailscard.h"
#include <QPointer>
#include "tabelviewstorecards.h"
#include"tableviewcollectioncardsmodel.h"
 #include <QInputDialog>
#include "dialogaddnewcardtocollection.h"
#include "columnfilter.h"
#include "dialogdevice.h"
#include "dialogsoldcards.h"
#include "dialogeksporttocsv.h"
#include <memory>
namespace Ui {
class MainWindow;
}

/**
 * @brief
 * Klasa głownego okna
 */
class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    /**
     * @brief
     * Jawny konrtuktor
     * @param parent
     */
    explicit MainWindow(QWidget *parent = 0);
    /**
     * @brief
     * Destruktor
     */
    ~MainWindow();
    ProxyModelStoreCards *proxyModelStoreCards;
    ProxyModelCollectionCards *proxyModelCollectionCards;
    TabelViewStoreCards *modelStore;
    TableViewCollectionCardsModel *modelCollection;

private:
    Ui::MainWindow *ui;
    Database *dataBase;
    /**
     * @brief
     * Iniicjalizuje elementy okna.
     */
    void initialization();
    /**
     * @brief
     * Ustawia style na podstawie pliku QSS.
     */
    void setStyle();

private slots:
    /**
     * @brief
     * Zmiana imienia karty w filtrze. Dotyczy zakłądnki "Store Cards"
     */
    void nameFilterChanged();
    /**
     * @brief
     * Zmiana imienia karty w filtrze. Dotyczy zakłądnki "Collection Cards"
     */
    void nameFilterChangedInCollection();
    /**
     * @brief
     * Ustawia status bar w zakldace "Collection Cards"
     */
    void statusBar();
    /**
     * @brief
     * Pokazuje menu kontekstowe po wciśnięciu lewego klawisza myszy w zakłądce "Store Cards"
     * @param pos
     */
    void ShowContextMenu(const QPoint& pos);

    /**
     * @brief
     * Wywyołuje okno do sprzedania karty przy wciśnięciu pozycji w zakłądce "Store Cards"
     * @param index
     */
    void on_tableViewStoreCards_doubleClicked(const QModelIndex &index);
    /**
     * @brief
     * Przycisk w zakładce "Collection Cards"
     */
    void on_buttMoveToCollection_clicked();
    /**
     * @brief
     * Przycisk w zakładce "Collection Cards"
     */
    void on_buttMarkAll_clicked();
    /**
     * @brief
     * Przycisk w zakładce "Collection Cards"
     */
    void on_buttUnMarkAll_clicked();
    /**
     * @brief
     * Przycisk w zakładce "Collection Cards"
     */
    void on_buttRemove_clicked();
    /**
     * @brief
     * Przycisk w zakładce "Collection Cards"
     */
    void on_buttSell_clicked();
    /**
     * @brief
     * Przycisk w zakładce "Collection Cards"
     */
    void on_buttMoveToStore_clicked();
    /**
     * @brief
     * Przycisk w zakładce "Collection Cards"
     */
    void on_buttColumnFilter_clicked();
    /**
     * @brief
     * Przycisk w zakładce "Collection Cards"
     */
    void on_buttNewCollection_clicked();
    /**
     * @brief
     * Przycisk w zakładce "Collection Cards"
     */
    void on_buttRemoveCollection_clicked();
    /**
     * @brief
     * Przycisk w zakładce "Collection Cards"
     */
    void on_buttAddNewCardToCollection_clicked();
    /**
     * @brief
     * Przycisk w zakładce "Collection Cards"
     */
    void on_buttDeviceConn_clicked();
    /**
     * @brief
     * Przycisk w menu
     */
    void on_actionClose_triggered();
    /**
     * @brief
     * Przycisk w menu
     */
    void on_actionRemoveCollection_triggered();
    /**
     * @brief
     * Przycisk w menu
     */
    void on_actionNewCollection_triggered();
    /**
     * @brief
     * Przycisk w menu
     */
    void on_actionCollectionFilter_triggered();
    /**
     * @brief
     * Przycisk w menu
     */
    void on_actionAdd_cards_from_device_triggered();
    /**
     * @brief
     * Przycisk w menu
     */
    void on_actionAbout_triggered();
    /**
     * @brief
     * Przycisk w menu
     */
    void on_actionView_history_sold_cards_triggered();
    /**
     * @brief
     * Przycisk w menu
     */
    void on_actionVat_triggered();
    /**
     * @brief
     * Przycisk w menu
     */
    void on_actionCreate_csv_file_triggered();
    /**
     * @brief
     * Przycisk w menu
     * @param checked
     */
    void on_actionShow_short_edtion_name_triggered(bool checked);
    /**
     * @brief
     * Przycisk w menu
     */
    void on_newCardIsAdded();


};

#endif // MAINWINDOW_H
