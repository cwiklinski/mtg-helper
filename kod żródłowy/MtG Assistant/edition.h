#ifndef EDITION_H
#define EDITION_H
#include <QVariant>
#include <QSqlQuery>
#include "database.h"
#include <QDebug>
/**
 * @brief Klasa opdisująca jeden rekord tabeli Edition.
 *
 */
class Edition
{    
public:

   /**
    * @brief
    * Konstruktor
    */
   Edition(){}
   /**
    * @brief
    * Destruktor
    */
   ~Edition(){}
   /**
    * @brief
    * Kontruktor uzupełnia obiekt na podstawie podanego id
    * @param id id z tabeli ediotns
    * @param objDataBase
    */
   Edition(int id, Database &objDataBase);
   /**
    * @brief
    * Kontruktor uzupełnia obiekt na podstawie podanego argumentu value.
    * @param value id edycji lub nazwa edycji(zarówo krótka jak i długa)
    * @param objDataBase otwarte połaczneie z baza danych
    * @param flag Flaga
    */
   Edition(QVariant value, Database &objDataBase, int flag =Edition::EditionID);
   /**
    * @brief
    * Kontruktor uzupełnia obiekt na podstawie podanego argumentu value.
    * @param value id edycji lub nazwa edycji(zarówo krótka jak i długa)
    * @param objDataBase otwarte połaczneie z baza danych
    * @param flag Flaga
    */
   Edition(QString value, Database &objDataBase,  int flag =Edition::EditionID);//Edition::EditionName );   posiadam konstruktor ktory jako pierwszy argument przyjmuje QString. Kompilator informuje o bledzie dwueznacznosci.

   /**
    * @brief
    *
    * @return QVariant
    */
   QVariant getId()      const{return this->id;}
   /**
    * @brief
    *
    * @return QVariant
    */
   QVariant getName()    const{return this->name;}
   /**
    * @brief
    *
    * @return QVariant
    */
   QVariant getLongname()const{return this->longname;}

   /**
    * @brief
    *
    * @param id
    * @return bool
    */
   bool setId(QVariant id)            {this->id.setValue(id);       return true;}
   /**
    * @brief
    *
    * @param name
    * @return bool
    */
   bool setName(QVariant name)        {this->id.setValue(name);     return true;}
   /**
    * @brief
    *
    * @param longname
    * @return bool
    */
   bool setLongname(QVariant longname){this->id.setValue(longname); return true;}

   /**
    * @brief
    *
    * @param value
    * @param flag
    */
   void chooseQuery(QString value, int flag);

   static int const EditionName = 1; /** flaga */
   static int const  EditionLongName = 2; /**flaga  */
   static int const EditionID =3; /**flaga  */

private:
    Database *dataBase; /** */
    /**
     * @brief
     * Pytanie do bazy sql pobirające dane edycji na podstawie id
     * @param id
     */
    void queryID(QString id);
    /**
     * @brief
     * Pytanie do bazy sql pobirające dane edycji na podstawie nazwy
     * @param name
     */
    void queryName(QString name);
    /**
     * @brief
     * Pytanie do bazy sql pobirające dane edycji na podstawie długiej nazwy
     * @param longName
     */
    void queryLongName(QString longName);
    /**
     * @brief
     * Odpoweidz z bazy danych przeniesiona do ziennych klasy
     * @param query
     * @return bool
     */
    bool queryToVariable(QSqlQuery &query);

    QVariant id; /**id z tabeli editions */
    QVariant name; /** nazwa z tabeli editions */
    QVariant longname; /** długa nazwa z tabeli editions */
};
#endif // EDITION_H
