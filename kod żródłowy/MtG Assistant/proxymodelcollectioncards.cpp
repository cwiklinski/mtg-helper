#include "proxymodelcollectioncards.h"
#include <QtGui>
ProxyModelCollectionCards::ProxyModelCollectionCards(QObject *parent) :
    QSortFilterProxyModel(parent), checkW(true), checkU(true),
    checkG(true), checkR(true), checkB(true), checkLand(true),
    checkArtifact(true), editionName(""), editionLongname(""), rarity(""),
    collectionName("ALL"),columnFilter(0)
{
}

bool ProxyModelCollectionCards::filterAcceptsRow(int sourceRow,
                                          const QModelIndex &sourceParent) const
{
    QModelIndex index0 = sourceModel()->index(sourceRow, 0, sourceParent);
    QModelIndex index2 = sourceModel()->index(sourceRow, 2, sourceParent);
    QModelIndex index3 = sourceModel()->index(sourceRow, 3, sourceParent);
    QModelIndex index4 = sourceModel()->index(sourceRow, 4, sourceParent);
    QModelIndex index9 = sourceModel()->index(sourceRow, 9, sourceParent);
    QModelIndex index10 = sourceModel()->index(sourceRow, 10, sourceParent);
    QModelIndex index11 = sourceModel()->index(sourceRow, 11, sourceParent);

    if(!checkW){
        if((sourceModel()->data(index10).toString().contains(QString("W"))))
            return  false;
    }  if(!checkG){
        if((sourceModel()->data(index10).toString().contains(QString("G"))))
            return false;
    } if(!checkU){
        if((sourceModel()->data(index10).toString().contains(QString("U"))))
            return  false;
    } if(!checkR){
        if((sourceModel()->data(index10).toString().contains(QString("R"))))
            return  false;
    } if(!checkB){
        if((sourceModel()->data(index10).toString().contains(QString("B"))))
            return false;
    }  if(!checkArtifact){
        if((sourceModel()->data(index9).toString().contains(QString("Artifact"))))
            return  false;
    }if(!checkLand)
        if((sourceModel()->data(index9).toString().contains(QString("Land"))))
            return false;

    if(!(rarity==""))
        if((sourceModel()->data(index11).toString().compare(rarity)))
            return false;
    if(!(editionName==""))
        if((sourceModel()->data(index2).toString().compare(editionName)))
        { return false;}
    if(!(editionLongname==""))
        if((sourceModel()->data(index3).toString().compare(editionLongname)))
        { return false;}
    if(!(collectionName=="ALL"))
        if((sourceModel()->data(index4).toString().compare(collectionName)))
        { return false;}

    return (sourceModel()->data(index0).toString().toUpper().contains(filterRegExp()) );


}

QVariant ProxyModelCollectionCards::data(const QModelIndex &index, int role) const
{
    if (role == Qt::BackgroundColorRole && index.row()%2 > 0)
        return QVariant::fromValue(QColor(214,175,154));

    if(!index.isValid())
        return QVariant();
       if(index.column() == 9 && (role == Qt::CheckStateRole || role == Qt::DisplayRole))
       {
           if(!QSortFilterProxyModel::data(index, role).isNull())
           return QSortFilterProxyModel::data(index, role).toString().append("%");
       }
    if(index.column() == 16 && (role == Qt::CheckStateRole || role == Qt::DisplayRole)) {
        if (role == Qt::CheckStateRole)
            return checkBoxTable[index.row()] ? QVariant(Qt::Checked) : QVariant(Qt::Unchecked);
        else if (role == Qt::DisplayRole)
            return QVariant();
    }
    else
        return QSortFilterProxyModel::data(index,role);
}





bool  ProxyModelCollectionCards::setData(const QModelIndex &index, const QVariant &value, int role)
{
    if(!index.isValid())
        return false;

    if(index.column() == 16 && role==Qt::CheckStateRole)
    {
        if(checkBoxTable[index.row()] == true){
            checkBoxTable[index.row()]  = false;
            emit dataChanged();
            return true;}
        if(checkBoxTable[index.row()]  == false){
            checkBoxTable[index.row()] = true;
            emit dataChanged();
            return true;}
        QVariant data = true;//(value.toInt()==Qt::Checked) ? QVariant(1) : QVariant (0);
        return true; /*QSortFilterProxyModel::setData(index, data, Qt::EditRole);*/
    }
    else
        return QSortFilterProxyModel::setData(index,value,role);

}




Qt::ItemFlags ProxyModelCollectionCards::flags ( const QModelIndex & index ) const {
    if(!index.isValid())
        return Qt::ItemIsEnabled;

    if(index.column() == 16)
        return Qt::ItemIsUserCheckable | Qt::ItemIsSelectable | Qt::ItemIsEnabled ;
    else
        return QSortFilterProxyModel::flags(index);

}


void ProxyModelCollectionCards::setCheckboxCointener(){

    int size = QSortFilterProxyModel::rowCount();
    checkBoxTable = new bool[size];
    for(int i =0 ;i < size; i++)
        checkBoxTable[i] = false;
    emit dataChanged();

}


void ProxyModelCollectionCards::setCheckboxCointener(bool value){

    int size = QSortFilterProxyModel::rowCount();
    checkBoxTable = new bool[size];
    for(int i =0 ;i < size; i++)
        checkBoxTable[i] = value;
    emit dataChanged();


}


QVariant ProxyModelCollectionCards::headerData(int section, Qt::Orientation orientation, int role) const
{
    if (role != Qt::DisplayRole)
        return QVariant();

    if (orientation == Qt::Horizontal) {
        switch (section) {
        case 0:
            return tr("Name");

        case 1:
            return tr("Edition");
        case 2:
            return tr("Edition");
        case 3:
            return tr("Quantity");
        case 4:
            return tr("Collection");
        case 5:
            return tr("Price net");
        case 6:
            return tr("Price gross");
        case 7:
            return tr("Price net after discount");
        case 8:
            return tr("Price gross after discount ");
        case 9:
            return tr("Discount");
        case 10:
            return tr("Manacost");
        case 11:
            return tr("Rarity");
        case 12:
            return tr("Type");
        case 13:
            return tr("P/T");
        case 14:
            return tr("Text");
        case 15:
            return tr("Id");


        default:
            return QVariant();
        }
    }
    return QVariant();
}


void ProxyModelCollectionCards::moveToCollection(QVariant idCollection, Database &dataBase)
{
    for(int i=0 ; i<rowCount(); i ++)
    {
        if(checkBoxTable[i])
        {
            QVariant ID = data(index(i, 15), 0 );
            CollectionCard *boxCard = new CollectionCard(ID, dataBase);
            boxCard->moveToCollection(idCollection);
            delete boxCard;
        }
    }

}

void ProxyModelCollectionCards::deleteCard(Database &dataBase)
{
    for(int i=0 ; i<rowCount(); i ++)
    {
        if(checkBoxTable[i])
        {
            QVariant ID = data(index(i, 15), 0 );
            CollectionCard *boxCard = new CollectionCard(ID, dataBase);
            boxCard->deleteCard();
            delete boxCard;
        }
    }

}

void ProxyModelCollectionCards::sellCard(Database &dataBase)
{
    for(int i=0 ; i<rowCount(); i ++)
    {
        if(checkBoxTable[i])
        {
            QVariant ID = data(index(i, 15), 0 );
            CollectionCard *boxCard = new CollectionCard(ID, dataBase);
            boxCard->sellCard(boxCard->getQuantity().toString());
            boxCard->deleteCard();
            delete boxCard;
        }
    }

}


void ProxyModelCollectionCards::moveToShop(Database &dataBase)
{
    for(int i=0 ; i<rowCount(); i ++)
    {
        if(checkBoxTable[i])
        {
            QVariant ID = data(index(i, 15), 0 );
            CollectionCard *boxCard = new CollectionCard(ID, dataBase);
            StoreCard *shopCard = new StoreCard(boxCard->getCardInfo()->getId(), dataBase);
            shopCard->setQuantity(
                        shopCard->getQuantity().toInt()
                        +boxCard->getQuantity().toInt());
            shopCard->upClassToDb();
            boxCard->deleteCard();
            delete boxCard;
            delete shopCard;
        }
    }

}


float* ProxyModelCollectionCards::stateBar()
{
    float counterQuantity=0, counterPriceNetto=0, counterSaleNetto=0, counterPriceBrutto=0, counterSaleBrutto=0;
    for(int i=0 ; i<rowCount(); i ++)
    {
        if(checkBoxTable[i])
        {
            counterQuantity += data(index(i,3),0).toFloat();
            counterPriceNetto += data(index(i,5),0).toFloat();
            counterSaleNetto+= data(index(i,6),0).toFloat();
            counterPriceBrutto += data(index(i,7),0).toFloat();
            counterSaleBrutto += data(index(i,8),0).toFloat();
        }
    }
    float *ret = new float[5];
    if(counterPriceNetto<0.01)
    {
        ret[0]=counterQuantity;
        ret[1]=0;
        ret[2]=0;
        ret[3]=0;
        ret[4]=0;
        return ret;
    }
    ret[0]=counterQuantity;
    ret[1]=counterPriceNetto;
    ret[2]=counterSaleNetto;
    ret[3]=counterPriceBrutto;
    ret[4]=counterSaleBrutto;

    return ret;
}
