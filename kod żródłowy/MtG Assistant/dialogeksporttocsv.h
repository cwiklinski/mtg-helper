#ifndef DIALOGEKSPORTTOCSV_H
#define DIALOGEKSPORTTOCSV_H

#include <QDialog>
#include <tableviewcollectioncardsmodel.h>
#include <tableviewsoldcards.h>
#include   <tabelviewstorecards.h>
#include <QFileDialog>
namespace Ui {
class DialogEksportToCsv;
}

/**
 * @brief
 * Klasa do exportu kart.
 */
class DialogEksportToCsv : public QDialog
{
    Q_OBJECT

public:
    /**
     * @brief
     * Konstruktor tworzacy okno
     * @param dataBaseObj
     * @param parent
     */
    explicit DialogEksportToCsv( Database &dataBaseObj, QWidget *parent = 0);
    Database *dataBase; /**< TODO */
    /**
     * @brief
     * Destruktor
     */
    ~DialogEksportToCsv();

private slots:
    /**
     * @brief
     *
     */
    void on_buttonBox_accepted();
    /**
     * @brief
     *
     * @param checked
     */
    void on_checkSoldCards_clicked(bool checked);
    /**
     * @brief
     *
     * @param checked
     */
    void on_checkCardsInShop_clicked(bool checked);
    /**
     * @brief
     *
     * @param checked
     */
    void on_checkAllCards_clicked(bool checked);
    /**
     * @brief
     *
     * @param checked
     */
    void on_checkCardsInBox_clicked(bool checked);

private:
    Ui::DialogEksportToCsv *ui; /**< TODO */
    /**
     * @brief
     * Stworzenie pliku csv wszystkich kart
     * @param output
     */
    void createCsvAllCards(QTextStream &output);
    /**
     * @brief
     * Stworzenie pliku csv kart w sklepie
     * @param output
     */
    void createCsvShopCards(QTextStream &output);
    /**
     * @brief
     * Stworzenie pliku csv kart sprzedanych
     * @param output
     */
    void createCsvSoldCards(QTextStream &output);
    /**
     * @brief
     * Stworzenie pliku csv kart z kolekcji.
     * @param output
     * @param nameCollection nazwa kolekcji
     */
    void createCsvCardsInBox(QTextStream &output, QString nameCollection);
};

#endif // DIALOGEKSPORTTOCSV_H
