#include "proxymodelstorecards.h"
#include<QDebug>
#include<QtGui>
ProxyModelStoreCards::ProxyModelStoreCards(QObject *parent) :
    QSortFilterProxyModel(parent), checkW(true), checkU(true),
    checkG(true), checkR(true), checkB(true),
    checkArtifact(true), editionName(""), editionLongname(""), rarity("")
{

}



bool ProxyModelStoreCards::filterAcceptsRow(int sourceRow, const QModelIndex &sourceParent) const
{
    QModelIndex index1 = sourceModel()->index(sourceRow, 1, sourceParent);
    QModelIndex index2 = sourceModel()->index(sourceRow, 2, sourceParent);
    QModelIndex index3 = sourceModel()->index(sourceRow, 3, sourceParent);
    QModelIndex index4 = sourceModel()->index(sourceRow, 4, sourceParent);
    QModelIndex index8 = sourceModel()->index(sourceRow, 8, sourceParent);
    QModelIndex index9 = sourceModel()->index(sourceRow, 9, sourceParent);
    QModelIndex index10 = sourceModel()->index(sourceRow, 10, sourceParent);

    if(!checkW){
        if((sourceModel()->data(index10).toString().contains(QString("W"))))
            return  false;
    }  if(!checkG){
        if((sourceModel()->data(index10).toString().contains(QString("G"))))
            return false;
    } if(!checkU){
        if((sourceModel()->data(index10).toString().contains(QString("U"))))
            return  false;
    } if(!checkR){
        if((sourceModel()->data(index10).toString().contains(QString("R"))))
            return  false;
    } if(!checkB){
        if((sourceModel()->data(index10).toString().contains(QString("B"))))
            return false;
    }  if(!checkArtifact){
        if((sourceModel()->data(index9).toString().contains(QString("Artifact"))))
            return  false;
    }if(!checkLand)
        if((sourceModel()->data(index9).toString().contains(QString("Land"))))
            return false;
    if(checkCardInShop)
        if((sourceModel()->data(index4).toInt() == 0 ))
            return false;
    if(!(rarity==""))
        if((sourceModel()->data(index8).toString().compare(rarity)))
            return false;
    if(!(editionName==""))
        if((sourceModel()->data(index2).toString().compare(editionName)))
        { return false;}
    if(!(editionLongname==""))
        if((sourceModel()->data(index3).toString().compare(editionLongname)))
        { return false;}

    return (sourceModel()->data(index1).toString().toUpper().contains(filterRegExp()) );

}


QVariant  ProxyModelStoreCards::headerData(int section, Qt::Orientation orientation, int role) const
{
    if (role != Qt::DisplayRole)
        return QVariant();

    if (orientation == Qt::Horizontal) {
        switch (section) {
        case 0:
            return tr("Id");
        case 1:
            return tr("Name");
        case 2:
            return tr("Edition");
        case 3:
            return tr("Edition");
        case 4:
            return tr("Qty");/*ntity*/
        case 5:
            return tr("Price net");
        case 6:
            return tr("Price Gross");
        case 7:
            return tr("Description");
        case 8:
            return tr("Rarity ");
        case 9:
            return tr("Type");
        case 10:
            return tr("Manacost");
        case 11:
            return tr("Text");

        default:
            return QVariant();
        }
    }
    return QVariant();
}


QVariant ProxyModelStoreCards::data(const QModelIndex &index, int role) const
{

    if (role == Qt::BackgroundColorRole && index.row()%2 > 0)
        return QVariant::fromValue(QColor(214,175,154));
    return QSortFilterProxyModel::data(index,role);
}
