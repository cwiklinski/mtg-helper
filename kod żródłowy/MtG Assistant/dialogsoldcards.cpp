#include "dialogsoldcards.h"
#include "ui_dialogsoldcards.h"
#include "tableviewsoldcards.h"
#include <QMessageBox>
DialogSoldCards::DialogSoldCards(Database &dataBaseObj,QWidget *parent) :
    QDialog(parent),
    ui(new Ui::DialogSoldCards)
{
Database *a = &dataBaseObj;
    ui->setupUi(this);
    setWindowTitle("Sold cards");

    TableViewSoldCards *tableViewSoldCards = new TableViewSoldCards();
    tableViewSoldCards->refresh(*a);
    proxyModel = new ProxyModelSoldCards();
    proxyModel->setSourceModel(tableViewSoldCards);
    proxyModel->setFilterMinimumDate(ui->calendarMinDate->selectedDate());
    proxyModel->setFilterMaximumDate(ui->calendarMaxDate->selectedDate());
     ui->tableViewSoldCards->setModel(proxyModel);
     ui-> tableViewSoldCards->setSortingEnabled(true);
     ui->tableViewSoldCards->setSelectionBehavior(QAbstractItemView::SelectRows);
     ui->tableViewSoldCards->horizontalHeader()->setStretchLastSection(true);
     ui->tableViewSoldCards->verticalHeader()->hide();
     ui->tableViewSoldCards->setEditTriggers(QAbstractItemView::NoEditTriggers);
     ui->tableViewSoldCards->setSelectionMode(QAbstractItemView::SingleSelection);
     ui->tableViewSoldCards->resizeColumnsToContents();
     ui->tableViewSoldCards->horizontalHeader()->setHighlightSections(false);
     ui->tableViewSoldCards->horizontalHeader()->setHighlightSections(false);

     connect(ui->calendarMaxDate, SIGNAL(selectionChanged()),
             this, SLOT(dataIsChanged()));
     connect(ui->calendarMinDate, SIGNAL(selectionChanged()),
             this, SLOT(dataIsChanged()));
}

DialogSoldCards::~DialogSoldCards()
{
    delete ui;
}
void DialogSoldCards::dataIsChanged()
{
    if(ui->calendarMinDate->selectedDate() > ui->calendarMaxDate->selectedDate())
    {
        ui->calendarMinDate->setSelectedDate(ui->calendarMaxDate->selectedDate());
        QMessageBox msgBox(this);
        msgBox.setText("Max date have to be higher then min date.");
        msgBox.exec();
    }
    proxyModel->setFilterMinimumDate(ui->calendarMinDate->selectedDate());
    proxyModel->setFilterMaximumDate(ui->calendarMaxDate->selectedDate());
}
