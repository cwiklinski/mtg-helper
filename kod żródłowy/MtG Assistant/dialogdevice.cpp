#include <QFileDialog>
#include <QInputDialog.h>
#include <QEventLoop>
#include <memory>
#include "dialogdevice.h"
#include "ui_dialogdevice.h"
#include "cardsfromxmparsing.h"
#include "storecard.h"
#include "collectioncard.h"
#include "listcollection.h"
#include <QNetworkReply>
#ifdef Q_OS_WIN
#include <windows.h> // for Sleep
#endif
DialogDevice::DialogDevice(Database &dataBaseObj,QWidget *parent) :
    QDialog(parent), dataBase(&dataBaseObj),
    ui(new Ui::DialogDevice)
{

   ui->setupUi(this);
   setWindowTitle("Adds cards from device");

   ui-> tableViewXmlCards->setSortingEnabled(true);
   ui->tableViewXmlCards->setSelectionBehavior(QAbstractItemView::SelectRows);
   ui->tableViewXmlCards->horizontalHeader()->setStretchLastSection(true);
   ui->tableViewXmlCards->verticalHeader()->hide();
   ui->tableViewXmlCards->setEditTriggers(QAbstractItemView::NoEditTriggers);
   ui->tableViewXmlCards->setSelectionMode(QAbstractItemView::SingleSelection);
}

DialogDevice::~DialogDevice()
{

    delete ui;
}

void DialogDevice::replyFinishedForStart(QNetworkReply* reply)
{
    QString resp = reply->readAll();
    QMessageBox msgBox(this);
    if(resp =="OK"){
       msgBox.setText("Device working");
       sendStatusRequest();
       msgBox.exec();
    }else if(resp == "Currently working"){
        qDebug() << "Currently working";
        msgBox.setText(resp);
        sendStatusRequest();
        msgBox.exec();
    }else if(resp == "Busy"){
         qDebug() << "Busy";
        sendStatusRequest();
    }else if(resp == "Ready"){
        qDebug() << "Ready";
        sendLastRequestAndDOwnloadXmlFile();
    }else if(resp == "Error"){
        msgBox.setText(resp);
        msgBox.exec();
    }else{
       msgBox.setText("Check connection with device");
       msgBox.exec();
       ui->pushButtMoveToShop->setEnabled(true);
       ui->pushButtMoveToBox->setEnabled(true);
       ui->pushButtOpenXmlFile->setEnabled(true);
       ui->pushButtGetCollection->setEnabled(true);
    }
}

void DialogDevice::sendStatusRequest()
{
    int ms = 5000;
        #ifdef Q_OS_WIN
            Sleep(uint(ms));
        #else
            struct timespec ts = { ms / 1000, (ms % 1000) * 1000 * 1000 };
            nanosleep(&ts, NULL);
        #endif

        QNetworkRequest request;
        request.setUrl(QUrl(mAddressToDevice + "/status"));
        manager->get(request);

}


void DialogDevice::sendLastRequestAndDOwnloadXmlFile()
{
    DownloadManager *dm = new DownloadManager();
    connect(dm, SIGNAL(overDownload(QString )), this, SLOT(openFileAndPutIntoTable(QString)) ) ;
    dm->setFile(mAddressToDevice + "/last", DownloadManager::eXmlFile);
}

void DialogDevice::openFileAndPutIntoTable(QString pFile)
{
    ParserXmlFromDevice *parserXmlFromDevice = new ParserXmlFromDevice( pFile);
    parserXmlFromDevice ->start();
    mTableViewDeviceCards = new TableViewDeviceCards();
    mTableViewDeviceCards->setCardsToDisplay(parserXmlFromDevice->returnData());
    ui->tableViewXmlCards->setModel(mTableViewDeviceCards );
    ui->tableViewXmlCards->resizeColumnsToContents();
    ui->tableViewXmlCards->horizontalHeader()->setHighlightSections(false);
    ui->pushButtMoveToShop->setEnabled(true);
    ui->pushButtMoveToBox->setEnabled(true);
    ui->pushButtOpenXmlFile->setEnabled(true);
    ui->pushButtGetCollection->setEnabled(true);

}
void DialogDevice::on_pushButtGetCollection_clicked()
{
    QMessageBox msgBox(this);
    msgBox.setText("Application started communication with the device");
    msgBox.exec();
    ui->pushButtMoveToShop->setEnabled(false);
    ui->pushButtMoveToBox->setEnabled(false);
    ui->pushButtOpenXmlFile->setEnabled(false);
    ui->pushButtGetCollection->setEnabled(false);
     mAddressToDevice =  ui->lineIp->text()+ ":" + ui->linePort->text() + "/mtg-scanner";
//    Socket *socket = new Socket();
//        socket->connect(ui->lineIp->text(), ui->linePort->text());
//        socket->sendC01();
//        socket->sendC02();
//qDebug() << socket->readLinkToXml();
    ////               socket->disconnect();
    manager = new QNetworkAccessManager(this);
    QNetworkRequest request;
    request.setUrl(QUrl(mAddressToDevice + "/start"));
    connect(manager, SIGNAL(finished(QNetworkReply*)),
            this, SLOT(replyFinishedForStart(QNetworkReply*)));
    manager->get(request);
    loop.exec();

}

void DialogDevice::on_pushButtOpenXmlFile_clicked()
{
    QString filenameXML = QFileDialog::getOpenFileName(this,
        tr("Open xml"), "C:/", tr("Device file (*.xml)"));
    if(filenameXML == "")
        return;
    ParserXmlFromDevice *parserXmlFromDevice = new ParserXmlFromDevice( filenameXML);
    parserXmlFromDevice ->start();

    mTableViewDeviceCards = new TableViewDeviceCards();
    mTableViewDeviceCards->setCardsToDisplay(parserXmlFromDevice->returnData());
    ui->tableViewXmlCards->setModel(mTableViewDeviceCards );
    ui->tableViewXmlCards->resizeColumnsToContents();
}

void DialogDevice::on_pushButtMoveToShop_clicked()
{
    if(mTableViewDeviceCards->rowCount() < 1)
        return;
     mTableViewDeviceCards->moveToStore(*dataBase);
     mTableViewDeviceCards->clearTable();
     QMessageBox msgBox(this);
     msgBox.setText("Successful. ");
     msgBox.exec();
}

void DialogDevice::on_pushButtMoveToBox_clicked()
{
    if(mTableViewDeviceCards->rowCount() <1 )
        return;
    bool ok;
    ListCollection *chooseCollection = new ListCollection(*dataBase);
    QString text = QInputDialog::getItem(this, "Choose collection", "Choose collection", chooseCollection ->getCollectionList(), 0, false, &ok);
    delete chooseCollection;
    if(ok){
    mTableViewDeviceCards->moveToCollection(text, *dataBase);
    mTableViewDeviceCards->clearTable();
    QMessageBox msgBox(this);
    msgBox.setText("Successful. ");
    msgBox.exec();
    }
}


void DialogDevice::on_pushButtonClose_clicked()
{
    loop.exit();
    close();
}
