#include "edition.h"

Edition::Edition(QString value , Database &objDataBase,int flag){
    dataBase = &objDataBase;
    this->chooseQuery(value, flag);
}
Edition::Edition(QVariant value, Database &objDataBase, int flag){
    dataBase = &objDataBase;
    this->chooseQuery(value.toString(), flag);
}

Edition::Edition(int id, Database &objDataBase){
    dataBase = &objDataBase;
    this->queryID(QString::number(id));
}

void Edition::chooseQuery(QString value, int flag){
    if(flag == EditionID)           {this->queryID(value);}
    else if(flag == EditionLongName){this->queryLongName(value);}
    else if(flag == EditionName)    {this->queryName(value);}
    else{qDebug()<<"Wrong flag for constructor class Edition";}
}

void Edition::queryID(QString id){
    QSqlQuery *query = new QSqlQuery(*dataBase->db);
    query->prepare("SELECT id,name, longname FROM editions WHERE id = :id ;");
    query->bindValue(":id", id);
    query->exec();
    this->queryToVariable(*query);
    delete query;
}

void Edition::queryName(QString name){   
    QSqlQuery *query = new QSqlQuery(*dataBase->db);
    query->prepare("SELECT id,name, longname FROM editions WHERE name = :name ;");
    query->bindValue(":name", name);
    query->exec();
    this->queryToVariable(*query);
    delete query;
}

void Edition::queryLongName(QString longName){
    QSqlQuery *query = new QSqlQuery(*dataBase->db);
    query->prepare("SELECT id,name, longname FROM editions WHERE longname = :longName ;");
    query->bindValue(":longName", longName);
    query->exec();
    this->queryToVariable(*query);
    delete query;
}


bool Edition::queryToVariable(QSqlQuery &query)
{
    query.next();
    int debQueryRespCout=0;
    this->id.setValue(query.value(0));
    this->name = (query.value(1).toString());
    this->longname = (query.value(2).toString());
    debQueryRespCout++;
    if(debQueryRespCout>1){
        qDebug() << "Wiecej niż jedna odpowiedz na zapytanie" << query.lastQuery();
        return false;
    }
    if(debQueryRespCout < 1){
        qDebug() << "Zero odopwiedzi na zapytanie" << query.lastQuery();
        return false;
    }
    return true;
}
