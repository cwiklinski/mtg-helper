#ifndef VAT_H
#define VAT_H
#include <QVariant>
#include "database.h"
#include <QSqlQuery>
/**
 * @brief
 *
 */
class Vat
{

public:

//    static int const VatID = 1; /**< TODO */
//    static int const VatValue = 2; /**< TODO */

    static enum Flag
    {
        VatID =1,
        VatValue = 2
    }sFlag;

    /**
     * @brief
     * Konstruktor tworzy pusty obiekt
     */
    Vat();
    /**
     * @brief
     *
     * @param arg  W zależności od ustawienia flag. Domyślnie ID na podstawie zostanie uzupełniona klasa.
     * @param objDataBase
     * @param flag sFlag
     */
    Vat(QVariant arg, Database &objDataBase, int flag = VatID);
    /**
     * @brief
     *
     * @param arg W zależności od ustawienia flag. Domyślnie ID na podstawie zostanie uzupełniona klasa.
     * @param objDataBase
     * @param flag sFlag
     */
    Vat(QString  arg, Database &objDataBase, int flag = VatID);
    /**
     * @brief
     *
     * @param arg W zależności od ustawienia flag. Domyślnie ID na podstawie zostanie uzupełniona klasa.
     * @param objDataBase
     * @param flag sFlag
     */
    Vat(int      arg, Database &objDataBase, int flag = VatID);

    /**
     * @brief
     * Uzupełnia obiekt na podstawie bazy danych
     * @param arg W zależności od ustawienia flag.
     * @param flag sFlag
     */
    void chooseQuery(QString arg, int flag);

    /**
     * @brief
     *
     * @return QVariant
     */
    QVariant getId()   const{return this->id;}
    /**
     * @brief
     *
     * @return QVariant
     */
    QVariant getValue()const{return this->value;}

    /**
     * @brief
     *
     * @param id
     * @return bool
     */
    bool setId(QVariant id)      {this->id.setValue(id);       return true;}
    /**
     * @brief
     *
     * @param value
     * @return bool
     */
    bool setValue(QVariant value){this->value.setValue(value); return true;}
    /**
     * @brief
     *
     */
    void uptadeToDB();
private:
    QVariant id; /**Id z tabeli bazy danych */
    QVariant value; /**Wartość z tabeli Vat */

    Database *dataBase;
    /**
     * @brief
     * Obrania odpowiedz na pytanie
     * @param query
     * @return bool
     */
    bool queryToVariable(QSqlQuery &query);
    /**
     * @brief
     * Komunikuje się i zadaje pytanie bazie danych po uzyskaniu odpowiedzi wywołuje quetyToVariable.
     * @param id id z tabelia Vat
     */
    void queryID(QString id);
    /**
     * @brief
     * Komunikuje się i zadaje pytanie bazie danych po uzyskaniu odpowiedzi wywołuje quetyToVariable.
     * @param value wartość z tabeli Vat
     */
    void queryValue(QString value);
};

#endif // VAT_H
