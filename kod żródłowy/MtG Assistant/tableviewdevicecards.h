#ifndef TABLEVIEWDEVICECARDS_H
#define TABLEVIEWDEVICECARDS_H

#include <QAbstractTableModel>
#include "cardsfromxmparsing.h"
#include "database.h"
/**
 * @brief
 *
 */
class TableViewDeviceCards : public QAbstractTableModel
{
    Q_OBJECT
public:
    /**
     * @brief
     * Konstruktor
     * @param parent
     */
    explicit TableViewDeviceCards(QObject *parent = 0);
    /**
     * @brief
     * Zwraca ilość kolumn
     * @param parent
     * @return int
     */
    int rowCount(const QModelIndex &parent = QModelIndex()) const;
    /**
     * @brief
     * Zwraca ilość kolumn
     * @param parent
     * @return int
     */
    int columnCount(const QModelIndex &parent = QModelIndex()) const;
    /**
     * @brief
     * Przenosi ona wszystkie karty do kolekcji
     * @param pCollectionName docelowa kolekcja
     * @param dataBase otwarta baza danych
     */
    void moveToCollection(QString pCollectionName, Database &dataBase);

    /**
     * @brief
     * Przesówa karty z tabeli do sklepu
     * @param dataBase otwarta baza danych
     */
    void moveToStore(Database &dataBase);
    /**
     * @brief
     * Reimplementacja funkcji. Odpowiada za wyświetlanie danych w tabeli.
     * @param index
     * @param role
     * @return QVariant
     */
    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const;
    /**
     * @brief
     *  Reimplementacja funkcji. Ustawia nagłówki tabeli
     * @param section
     * @param orientation
     * @param role
     * @return QVariant
     */
    QVariant  headerData(int section, Qt::Orientation orientation, int role) const;

    CardsFromXmParsing cards; /**< TODO */
    /**
     * @brief
     * Ustawia karty które będą wyświetlane
     * @param pCards
     */
    void setCardsToDisplay(CardsFromXmParsing pCards);
    /**
     * @brief
     * Czyście tabele.
     */
    void clearTable();
};

#endif // TABLEVIEWDEVICECARDS_H
