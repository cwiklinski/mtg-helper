#include "columnfilter.h"
#include "ui_columnfilter.h"
#include <QDebug>

ColumnFilter::ColumnFilter(unsigned int &columnfilter, QWidget *parent) :
    QDialog(parent), filter(&columnfilter),
    ui(new Ui::ColumnFilter)
{
    setWindowTitle("Boxes column filter");
    ui->setupUi(this);
    if(*filter & 1)
        ui->checkBoxEditionShort->setChecked(true);
    if(*filter & 2)
        ui->checkBoxEditionLong->setChecked(true);
    if(*filter & 4)
        ui->checkBoxCollection->setChecked(true);
    if(*filter & 8)
        ui->checkBoxPriceNet->setChecked(true);
    if(*filter & 16)
        ui->checkBoxPriceGross->setChecked(true);
    if(*filter & 32)
        ui->checkBoxPriceNetAfterSale->setChecked(true);
    if(*filter & 64)
        ui->checkBoxPriceGrossAfterSale->setChecked(true);
    if(*filter & 128)
        ui->checkBoxDiscount->setChecked(true);
    if(*filter & 256)
        ui->checkBoxManacost->setChecked(true);
    if(*filter & 512)
        ui->checkBoxRarity->setChecked(true);
    if(*filter & 1024)
        ui->checkBoxType->setChecked(true);
    if(*filter & 2048)
        ui->checkBoxPT->setChecked(true);
    if(*filter & 4096)
        ui->checkBoxText->setChecked(true);
    qDebug() <<"huhu";
    qDebug() << *filter;

}

ColumnFilter::~ColumnFilter()
{
    delete ui;
}

void ColumnFilter::on_buttonBox_accepted()
{
    qDebug() <<"accept";
    *filter = 0;
    if(!ui->checkBoxEditionShort->isChecked())
        *filter |= 1;
    if(!ui->checkBoxEditionLong->isChecked())
        *filter |= 2;
    if(!ui->checkBoxCollection->isChecked())
        *filter |= 4;
    if(!ui->checkBoxPriceNet->isChecked())
        *filter |= 8;
    if(!ui->checkBoxPriceGross->isChecked())
        *filter |= 16;
    if(!ui->checkBoxPriceNetAfterSale->isChecked())
        *filter |= 32;
    if(!ui->checkBoxPriceGrossAfterSale->isChecked())
        *filter |= 64;
    if(!ui->checkBoxDiscount->isChecked())
        *filter |= 128;
    if(!ui->checkBoxManacost->isChecked())
        *filter |= 254;
    if(!ui->checkBoxRarity->isChecked())
        *filter |= 512;
    if(!ui->checkBoxType->isChecked())
        *filter |= 1024;
    if(!ui->checkBoxPT->isChecked())
        *filter |= 2048;
    if(!ui->checkBoxText->isChecked())
        *filter |= 4096;


}
