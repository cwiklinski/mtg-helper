#ifndef PARSERXMLFROMDEVICE_H
#define PARSERXMLFROMDEVICE_H
#include <QFile>
#include <QVector>
#include <QtXml>
#include "cardsfromxmparsing.h"

/**
 * @brief
 *
 */
class ParserXmlFromDevice
{
public:

    CardsFromXmParsing *collection; /**Obiekt przechowuje karty po sprasowaniu*/

    ParserXmlFromDevice();

    ParserXmlFromDevice(QString filename);
    QFile* file; /**plik xml */
    QDomDocument xmlBOM; /** */
    QDomElement root; /** */


    void readingDocRoot();

    void start();

    void readDataCards(QDomElement cards);

    void readData();

    bool readFile(const QString &fileName);

    CardsFromXmParsing returnData();
};

#endif // PARSERXMLFROMDEVICE_H
