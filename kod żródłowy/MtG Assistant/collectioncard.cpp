#include "collectioncard.h"

#include <QSqlError>
CollectionCard::CollectionCard()
{
}
CollectionCard::CollectionCard(Database &_dataBase): dataBase(&_dataBase)
{
}


CollectionCard::CollectionCard(QVariant id, Database &_dataBase) : dataBase(&_dataBase)
{
    queryToVariable(queryID(id.toString()));
}

bool CollectionCard::queryToVariable(QSqlQuery &query)
{
    int debQueryRespCout = 0;
    while (query.next())
    {
        setCardInfo(query.value(1));
        setCollection(query.value(5));
        setId(query.value(0));
        getCardInfo()->setId(query.value(1));
        setQuantity(query.value(2));
        setDescription(query.value(3));
        setDiscount(query.value(4));
        debQueryRespCout++;
        if(debQueryRespCout>1){
            qDebug() << "Wiecej niż jedna odpowiedz na zapytanie" << query.lastQuery();
            return false;
        }
    } //end while loop
    QSqlQuery *temp = &query;
    delete temp;
    if(debQueryRespCout < 1){
        qDebug() << "Zero odopwiedzi na zapytanie" << query.lastQuery();
        return false;
    }
    return true;
}




QSqlQuery &CollectionCard::queryID(QString id)
{
    QSqlQuery *query = new QSqlQuery(*dataBase->db);
    query->prepare("SELECT id, id_allCards, quantity, description, discount, id_collectionList FROM collectionCards WHERE id = :id ;");
    query->bindValue(":id", id);
    query->exec();
    return *query;
}

void CollectionCard::insertToCollectionTbl()
{
    QSqlQuery *query = new QSqlQuery(*dataBase->db);
    query->prepare("SELECT id, quantity FROM collectionCards WHERE id_allCards = :id_allCards AND  id_collectionList = :id_collectionList AND discount=:discount");
    query->bindValue(":id_allCards", getCardInfo()->getId().toString());
    query->bindValue(":discount", getDiscount().toString());
    query->bindValue(":id_collectionList", getCollection()->getId().toString());
    if(!query->exec())
        query->lastError();
    query->first();

    if(query->isValid()){
        QString id_CollectionCards = query->value(0).toString();
        QString quantity = QString::number(query->value(1).toInt()+ getQuantity().toInt());

        query->prepare("UPDATE collectionCards SET quantity = :quantity WHERE id=:id; ");
        query->bindValue(":id", id_CollectionCards);
        query->bindValue(":quantity", quantity);
        if(query->exec())
            query->lastError();

        query->lastQuery();

    }else{

        query->prepare("INSERT INTO collectionCards(id_allCards, quantity, description, discount, id_collectionList) VALUES(:id_allCards, :quantity, :description, :discount, :id_collectionList) ;");
        query->bindValue(":id_allCards", getCardInfo()->getId().toString());
        query->bindValue(":quantity", getQuantity().toString());
        query->bindValue(":discount", getDiscount().toString());
        query->bindValue(":id_collectionList", getCollection()->getId().toString());
        if(query->exec())
            query->lastError();


        query->lastQuery();
    }
}
void CollectionCard::moveToCollection(QVariant id_collectionList)
{ 
    QSqlQuery *query = new QSqlQuery(*dataBase->db);
    query->prepare("  UPDATE collectionCards SET id_collectionList = :id_collectionList WHERE id = :id ;");
    query->bindValue(":id_collectionList", id_collectionList.toString() );
    query->bindValue(":id",  this->getId().toString());
    query->exec();
}

void CollectionCard::deleteCard()
{
    QSqlQuery *query = new QSqlQuery(*dataBase->db);
    query->prepare("  DELETE FROM collectionCards WHERE id =:id  ;");
    query->bindValue(":id",  this->getId().toString());
    query->exec();
}


void CollectionCard::sellCard(QString quantity)
{
    QSqlQuery *query = new QSqlQuery(*dataBase->db);
    query->prepare(" INSERT INTO soldCards(id_allCards, priceNet, vat, discount, quantity) VALUES (:id_allCards, :priceNet, :vat, :discount,:quantity)   ;");
    query->bindValue(":id_allCards",  this->getCardInfo()->getId().toString());
    query->bindValue(":priceNet",  this->getCardInfo()->getPriceNet().toString());
    query->bindValue(":vat",  this->getCardInfo()->getVat()->getValue().toString());
    query->bindValue(":discount",  this->getDiscount().toString());
    query->bindValue(":quantity",  quantity);

    query->exec();
    qDebug() << query->lastInsertId();
    qDebug() <<query->lastQuery();
}

