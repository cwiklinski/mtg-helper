/****************************************************************************
**
** Copyright (C) 2014 Digia Plc and/or its subsidiary(-ies).
** Contact: http://www.qt-project.org/legal
**
** This file is part of the examples of the Qt Toolkit.
**
** $QT_BEGIN_LICENSE:BSD$
** You may use this file under the terms of the BSD license as follows:
**
** "Redistribution and use in source and binary forms, with or without
** modification, are permitted provided that the following conditions are
** met:
**   * Redistributions of source code must retain the above copyright
**     notice, this list of conditions and the following disclaimer.
**   * Redistributions in binary form must reproduce the above copyright
**     notice, this list of conditions and the following disclaimer in
**     the documentation and/or other materials provided with the
**     distribution.
**   * Neither the name of Digia Plc and its Subsidiary(-ies) nor the names
**     of its contributors may be used to endorse or promote products derived
**     from this software without specific prior written permission.
**
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
** "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
** LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
** A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
** OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
** DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
** THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
** (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
** OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE."
**
** $QT_END_LICENSE$
**
****************************************************************************/

#include <QCoreApplication>
#include <QFile>
#include <QFileInfo>
#include <QList>
#include <QNetworkAccessManager>
#include <QNetworkRequest>
#include <QNetworkReply>
#include <QSslError>
#include <QStringList>
#include <QTimer>
#include <QUrl>
#include <stdio.h>

class QSslError;

QT_USE_NAMESPACE

/**
 * @brief
 * Klasa pobierająca plik za pomoca protokolu http.
 */
class DownloadManager: public QObject
{
    Q_OBJECT
public:
    enum{
        eXmlFile,
        ePicFile
    };
    /**
     * @brief
     * Konstruktor
     * @param parent
     */
    DownloadManager( QObject *parent = 0);
    /**
     * @brief
     * Destruktor
     */
    ~DownloadManager();
    QNetworkAccessManager *manager; /**Manager połączenia */
    QNetworkReply *reply; /**Odpowiedz  http*/
    QFile *file; /**Pobierany plik */
    /**
     * @brief
     * Ustawia plik do pobrania
     * @param fileURL adres pliku do pobrania
     */
    void setFile(QString fileURL, int pFlag);
    void setFileName(QString pFileName){mFileName = pFileName;}
private:
    QString mFileName;

private slots: 
    /**
     * @brief
     * Stan pobierania
     * @param qint64
     * @param qint64
     */
    void onDownloadProgress(qint64,qint64);
    /**
     * @brief
     * Sygnał zakończono
     * @param
     */
    void onFinished(QNetworkReply*);
    /**
     * @brief
     *
     */
    void onReadyRead();
    /**
     * @brief
     *
     */
    void onReplyFinished();

signals:
    /**
     * @brief
     *
     */
     void overDownload(QString file);

};


