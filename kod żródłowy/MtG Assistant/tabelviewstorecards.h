#ifndef TABELVIEWSHOPCARDS_H
#define TABELVIEWSHOPCARDS_H
#include <QSqlQueryModel>
#include "database.h"
/**
 * @brief
 *
 */
class TabelViewStoreCards : public  QSqlQueryModel
{
public:
    /**
     * @brief
     * Konstruktor. Uzupełnia tabele wywołująć funkcje refresh()
     */
    TabelViewStoreCards();
    /**
     * @brief
     *  Odświeża widok tabeli na podstawie bazy danych.
     * @param dataBase
     */
    void refresh(Database &dataBase);
//    QVariant headerData(int section, Qt::Orientation orientation, int role) const;
//int rowCount(const QModelIndex &parent) const;
};

#endif // TABELVIEWSHOPCARDS_H
