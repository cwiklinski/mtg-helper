#ifndef CARDSFROMXMPARSING_H
#define CARDSFROMXMPARSING_H
#include <QVariant>
#include <QVector>

/**
 * @brief
 * Klasa kontenerowa. Zawiera wektrory kart sparsowanych z pliku xml
 */
class CardsFromXmParsing
{
public:
    /**
     * @brief
     *
     */
    CardsFromXmParsing();
    QVector<QVariant> name; /** */
    QVector<QVariant> id; /** */
    QVector<QVariant> edition; /** */
};

#endif // CARDSFROMXMPARSING_H
