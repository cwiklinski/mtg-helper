#ifndef SOCKET_H
#define SOCKET_H
#include <QTcpSocket>

#include <QObject>

/**
 * @brief
 * Klasa do komunikacji z urządeniem po protokole tcp/ip
 */
class Socket : public QObject
{
    Q_OBJECT
public:
    /**
     * @brief
     * Konstruktor
     * @param parent
     */
    explicit Socket(QObject *parent = 0);
/**
 * @brief
 * Połączenie z serwerem.
 * @param IP  adres ip v4
 * @param port port
 */
void connect(QString IP, QString port);

/**
 * @brief
 * Wyślij komende pierwszą, sprawdzająca czy urzadzneie jewst włączone.
 * @return bool true jeśli włączone
 */
bool sendC01();
/**
 * @brief
 *  Wyślij polecenie rozpoczącie skanowania kart.
 * @return bool true jeśli wykonene
 */
bool sendC02();
/**
 * @brief
 * Pobierz link xml ze zeskanowanymi kartami.
 * @return QString Nazwa pliku
 */
QString readLinkToXml();
/**
 * @brief
 * Rozłącz
 */
void disconnect(){socket->close();}
signals:

public slots:
private:
    QTcpSocket *socket; /**Wykorzystzwane gniaydo do połączenia*/

};

#endif // SOCKET_H
