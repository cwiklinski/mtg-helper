#ifndef PROXYMODELSHOPCARDS_H
#define PROXYMODELSHOPCARDS_H

#include <QSortFilterProxyModel>

/**
 * @brief
 * Klasa do filtrowaia tabeli "Store Cards"
 */
class ProxyModelStoreCards : public QSortFilterProxyModel
{
    Q_OBJECT
public:
    /**
     * @brief
     * Jawny kontruktor
     * @param parent
     */
    explicit ProxyModelStoreCards(QObject *parent = 0);

    bool checkW; /**Czy wyświetlić karty białe? */
    bool checkU; /** Czy wyświetlić karty niebieskie?  */
    bool checkG; /**Czy wyświetlić karty zielone?  */
    bool checkR; /**Czy wyświetlić karty czerwone?  */
    bool checkB; /**Czy wyświetlić karty czarne?  */
    bool checkArtifact; /**Czy wyświetlić karty artyfaktyczne?  */
    bool checkLand{true}; /**Czy wyświetlić karty land? */
    bool checkCardInShop; /**Czy wyświetlić karty znajdujące się s sklepie  */
    QString rarity; /**Wyświetlić karty z rarity. */
    QString editionName; /**Wyświetlić karty o nazwie edycji */
    QString editionLongname; /**yświetlić karty o długiej nazwie edycji */
    /**
     * @brief
     * Funckaj tworzy nagłówek kolumn tabeli
     * @param section
     * @param orientation
     * @param role
     * @return QVariant
     */
    QVariant  headerData(int section, Qt::Orientation orientation, int role) const;
protected:
    /**
     * @brief
     * Filtowanie tabeli
     * @param sourceRow
     * @param sourceParent
     * @return bool
     */
    bool filterAcceptsRow(int sourceRow,const QModelIndex &sourceParent) const;
    /**
     * @brief
     * Dane tabeli
     * @param index
     * @param role
     * @return QVariant
     */
    QVariant data(const QModelIndex &index, int role) const;

signals:

public slots:
private:


};

#endif // PROXYMODELSHOPCARDS_H
