#ifndef DETAILSCARD_H
#define DETAILSCARD_H
//#include <QDialog>
#include <QNetworkReply>
#include <QNetworkAccessManager>
#include <QUrl>
#include <QNetworkRequest>
#include <QBuffer>
#include <QSqlDatabase>
#include <QSqlTableModel>
#include "dialogstoreCardSell.h"
#include <QtNetwork>
#include <QMessageBox>
#include  "storecard.h"
#include "database.h"
#include "dialogdetailscard.h"

namespace Ui {
class DialogDetailsCard;
}


/**
 * @brief
 * Klasa tworzy podgląd stanu kast w sklepie. Okno umożliwiwa edycje stanu.
 */
class DialogDetailShopCard : public QDialog
{
    Q_OBJECT
public:
    /**
     * @brief
     * Konstruktor
     */
    DialogDetailShopCard();
    /**
     * @brief
     * Konstruktor uzupełniający okno
     * @param row
     * @param dataBase
     * @param parent
     */
    explicit DialogDetailShopCard(int row, Database &dataBase, QWidget *parent = 0);
    /**
     * @brief
     * Destruktor
     */
    ~DialogDetailShopCard();

    /**
     * @brief
     * Ustawia okno
     */
    void SetWindow();


private slots:

    /**
     * @brief
     * Wciśnięcie przycisku "sell"
     */
    void on_sellCard_clicked();
    /**
     * @brief
     * Zaznaczenie checkBoxa umożliwiającego edycje
     * @param checked
     */
    void on_checkBoxEditable_clicked(bool checked);
    /**
     * @brief
     * Gdy vat zostanie zmieniony.
     */
    void on_lineVAT_editingFinished();
    /**
     * @brief
     * Wciśnięcie przycisku "Save:
     */
    void on_buttonSave_clicked();
    /**
     * @brief
     * Wciśnięcie przycisku "Cancel"
     */
    void on_buttonCancel_clicked();
    /**
     * @brief
     * Zmiana ceny brutto
     * @param arg1
     */
    void on_linePriceGross_textChanged(const QString &arg1);
    /**
     * @brief
     * Zmiana cene netto.
     * @param arg1
     */
    void on_linePriceNet_textEdited(const QString &arg1);

    void setPicture();
private:
    Ui::DialogDetailsCard *ui; /**< TODO */

    /**
     * @brief
     * Czy zaszły zmiany w oknie
     * @return bool
     */
    bool doYouChangedMe();
    StoreCard *shopCard; /**Prezentowana karta*/
    Database *dataBase; /**<  */
    /**
     * @brief
     * Zapisz smiany
     */
    void save();

};

#endif // DETAILSCARD_H
