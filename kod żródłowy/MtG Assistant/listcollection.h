#ifndef LISTBOXES_H
#define LISTBOXES_H
#include "database.h"
#include <QSqlQuery>
#include "collection.h"
#include <QMessageBox>

/**
 * @brief
 * Klasa zawierta liste kolekcji
 */
class ListCollection
{
public:
    /**
     * @brief
     * Konstruktor
     */
    ListCollection();
    /**
     * @brief
     * Kontruktor wpisujący do klasy kolekcje.
     * @param objDatabase
     */
    ListCollection(Database &objDatabase);
    /**
     * @brief
     *
     * @return QStringList lista kolekcji
     */
    QStringList getCollectionList();
    /**
     * @brief
     *
     * @param id id kolekcji z tabali collection
     * @return Collection kolekcja
     */
    Collection &getCollection(QVariant id){return this->listBoxes[id.toInt()-1];}

    /**
     * @brief
     *  Pobiera z bazy danych rozmiar tabeli collection
     */
    void downloadSizeCollectionList();
    /**
     * @brief
     * Odświeża liste kolekcji na podstaie tabeli collection
     */
    void refreshList();
    /**
     * @brief
     *
     * @param name naza kolekcji
     * @return QVariant id kolekcji
     */
    QVariant getIdCollection(QVariant name);
    /**
     * @brief
     * Uzuwa kolekcji
     * @param name nazwa kolekcji
     * @return bool true jesł udało się usunąć kolekcje
     */
    bool dropCollection(QVariant name);
    /**
     * @brief
     * Dodaje nową kolekcje
     * @param name nazwa kolekcji
     * @return bool true w przypadku sukcesu.
     */
    bool newCollection(QVariant name);

private:
    int size;
    Collection *listBoxes; /***/
    Database *dataBase; /***/
};

#endif // LISTBOXES_H
