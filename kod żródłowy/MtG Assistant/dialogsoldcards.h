#ifndef DIALOGSOLDCARDS_H
#define DIALOGSOLDCARDS_H

#include <QDialog>
#include "database.h"
#include "proxymodelsoldcards.h"

namespace Ui {
class DialogSoldCards;
}

/**
 * @brief
 * Klasa odpoweidzialna za wyświetlenie okana kart sprzedanych
 */
class DialogSoldCards : public QDialog
{
    Q_OBJECT

public:
    ProxyModelSoldCards *proxyModel; /**< TODO */
    /**
     * @brief
     * Konstruktor tworzacy okno
     * @param dataBaseObj
     * @param parent
     */
    explicit DialogSoldCards(Database &dataBaseObj, QWidget *parent = 0);
    /**
     * @brief
     * Destruktor
     */
    ~DialogSoldCards();

private:
    Ui::DialogSoldCards *ui; /**< TODO */

private slots:
    /**
     * @brief
     * Gdy zmieniona przedział czasowy wyświetlanych kart.
     */
    void dataIsChanged();
};

#endif // DIALOGSOLDCARDS_H
