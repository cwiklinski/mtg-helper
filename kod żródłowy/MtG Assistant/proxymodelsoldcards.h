#ifndef PROXYMODELSOLDCARDS_H
#define PROXYMODELSOLDCARDS_H


#include <QSortFilterProxyModel>
#include <QDate>

/**
 * @brief
 * Klsa przechowuje karty sprzedane
 */
class ProxyModelSoldCards : public QSortFilterProxyModel
{
    Q_OBJECT
public:
    /**
     * @brief
     * Konstruktor jawny
     * @param parent
     */
    explicit ProxyModelSoldCards(QObject *parent = 0);
    /**
     * @brief
     * Tworzy nagłowki dla kolumn tabeli
     * @param section
     * @param orientation
     * @param role
     * @return QVariant
     */
    QVariant  headerData(int section, Qt::Orientation orientation, int role) const;

    /**
     * @brief
     * Ustawia date od ktorej będa wyświetlane sprzedane karty w tabeli
     * @param date
     */
    void setFilterMinimumDate(const QDate &date);
    /**
     * @brief
     * Ustawia date do ktorej będa wyświetlane sprzedane karty w tabeli
     * @param date
     */
    void setFilterMaximumDate(const QDate &date);
    /**
     * @brief
     *
     * @return QDate zwraca dolną granice daty dal wyświetlanych kart
     */
    QDate filterMinimumDate() const {return minDate;}
    /**
     * @brief
     *
     * @return QDate zwraca górną granice daty dal wyświetlanych kart
     */
    QDate filterMaximumDate() const {return maxDate;}


protected:
    /**
     * @brief
     * Filtruje wyniki
     * @param sourceRow
     * @param sourceParent
     * @return bool
     */
    bool filterAcceptsRow(int sourceRow,const QModelIndex &sourceParent) const;

private:
    QDate minDate; /**Data od której będa prezentowane sprzedane karty. */
    QDate maxDate; /**Data do której będa prezentowane sprzedane karty */
    /**
     * @brief
     * Funkcja sprawdza czy karta miesci się w zakresie datowym.
     * @param date
     * @return bool
     */
    bool dateInRange(const QDate &date) const;

};

#endif // PROXYMODELSOLDCARDS_H
