#ifndef TABLEVIEWSOLDCARDS_H
#define TABLEVIEWSOLDCARDS_H

#include <QObject>
#include<QSqlQueryModel>
#include "database.h"
/**
 * @brief
 * Klasa przygotowująca model tabeli kart sklepowych.
 */
class TableViewSoldCards : public QSqlQueryModel
{
    Q_OBJECT
public:
    /**
     * @brief
     * Odświeża widok tabeli
     * @param dataBase otwarta baza danych
     */
    void refresh(Database &dataBase);
    /**
     * @brief
     * Konstruktor
     * @param parent
     */
    explicit TableViewSoldCards(QObject *parent = 0);

};

#endif // TABLEVIEWSOLDCARDS_H
