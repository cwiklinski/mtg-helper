#include "listeditions.h"

ListEditions::ListEditions(Database &objDatabase):dataBase(&objDatabase)
{
    this->refreshEditionList();
}
bool ListEditions::refreshEditionList()
{
    QSqlQuery *query = new QSqlQuery(*dataBase->db);
    query->exec("SELECT count(*) FROM editions;");
    query->executedQuery();
    query->first();
    this->size = query->value(0).toInt();
    delete query;
    listEditions= new Edition[this->size];
    for(int i =1 ; i <= this->size ; i++){
        listEditions[i-1] = Edition(QString::number(i),*this->dataBase);
    }
}

QStringList ListEditions::getEditionNameList()
{
    QStringList *list = new QStringList;
    for(int i = 0; i<size; i++)
        list->append(this->listEditions[i].getName().toString());
    return *list;
}

QStringList ListEditions::getEditionLongNameList()
{
    QStringList *list = new QStringList;
    for(int i = 0; i<size; i++)
        list->append(this->listEditions[i].getLongname().toString());
    return *list;
}
