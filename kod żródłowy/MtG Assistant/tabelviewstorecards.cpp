#include "tabelviewstorecards.h"

TabelViewStoreCards::TabelViewStoreCards()
{

}

void TabelViewStoreCards::refresh(Database &dataBase)
{

    setQuery("SELECT  allCards.id,"
             "allCards.name,"
             "editions.name,"
             "editions.longname,"
             "storeCards.quantity,"
             "allCards.priceNet,"
             "ROUND(allCards.priceNet + allCards.priceNet*(SELECT value FROM vat WHERE id =1)/100.0,2) ,"
             "storeCards.Description,"
             "rarity.name,"
             "allCards.type,"
             "allCards.manacost,"
             "allCards.text FROM allCards"
             " LEFT JOIN storeCards ON allCards.id=storeCards.id_allCards  LEFT JOIN editions ON allCards.id_edition = editions.id  LEFT JOIN rarity ON allCards.id_rarity=rarity.id;", *dataBase.db);

    while (canFetchMore()) fetchMore();

}

