#include "storecard.h"
#include <memory>
StoreCard::StoreCard()
{
}
StoreCard::StoreCard(QString _id,Database &objDataBase, int flag  ):dataBase(&objDataBase){
    this->chooseQuery(_id,flag);
}
StoreCard::StoreCard(QVariant _id,Database &objDataBase, int flag ):dataBase(&objDataBase){
    this->chooseQuery(_id.toString(), flag);
}
StoreCard::StoreCard(int _id,Database &objDataBase, int flag ):dataBase(&objDataBase){
    this->chooseQuery(QString::number(_id), flag);
}




void StoreCard::chooseQuery(QString _id, int flag)
{
    if(flag == StoreCard::CardInfoID_cardinfo){
        queryID_cardinfo(_id);
    }
    else if(flag == StoreCard::CardInfoID){
        queryID(_id);
    }else{ qDebug()<< "wrong flag for ShopCard Constructor";}
    setCardInfo(_id);
}

bool StoreCard::queryToVariable(QSqlQuery &query){
    int debQueryRespCout(0);
    while (query.next())
    {
        setId(query.value(0));
        setCardInfo(query.value(1));
        setQuantity(query.value(2));
        setDestription(query.value(4));

        debQueryRespCout++;
        if(debQueryRespCout>1){

            qDebug() << "Wiecej niż jedna odpowiedz na zapytanie" << query.executedQuery();
            return false;
        }
    } //end while loop

    if(debQueryRespCout < 1){
        qDebug() << "Zero odopwiedzi na zapytanie" << query.executedQuery();
        return false;
    }
    return true;
}




void StoreCard::queryID(QString id)
{
    QSqlQuery *query = new QSqlQuery(*dataBase->db);
    query->prepare("SELECT id, id_allCards, quantity, Description FROM storeCards WHERE id = :id ;");
    query->bindValue(":id", id);
    queryToVariable(*query);
    delete query;
}

void StoreCard::queryID_cardinfo(QString id_cardinfo)
{
    QSqlQuery *query = new QSqlQuery(*dataBase->db);
    query->prepare("SELECT id, id_allCards, quantity, Description FROM storeCards WHERE id_allCards = :id_allCards ");
    query->bindValue(":id_allCards", id_cardinfo);
    if(query->exec()){
        queryToVariable(*query);
        delete query;
    };
}


void StoreCard::upClassToDb()
{
    QSqlQuery *query = new QSqlQuery(*dataBase->db);
    query->prepare("SELECT count(*) FROM storeCards WHERE id_allCards=:id_allCards ;");
    query->bindValue(":id_allCards", getCardInfo()->getId().toString());

    query->exec();
    query->first();
    bool cardExistInShop = false;
    if(query->value(0).toInt()>0)
        cardExistInShop = true;
    delete query;
    
    if(!cardExistInShop){
        QSqlQuery *query = new QSqlQuery(*dataBase->db);
        qDebug() << "ilosc" <<  getQuantity().toString();
        query->prepare("INSERT INTO storeCards(id_allCards, quantity, description) VALUES(:id_allCards, :quantity,  :description)   ;");
        query->bindValue(":id_allCards", getCardInfo()->getId().toString());
        query->bindValue(":quantity", getQuantity().toString());
        query->bindValue(":description", getDestription().toString());
        query->exec();
        delete query;}
    else{
        QSqlQuery *query = new QSqlQuery(*dataBase->db);
        query->prepare(" UPDATE storeCards SET quantity = :quantity, Description = :description WHERE id_allCards = :id_allCards;");
        query->bindValue(":quantity", getQuantity().toString());
        query->bindValue(":description", getDestription().toString());
        qDebug() << getCardInfo()->getId().toString() << "UPDATE storeCards SET quantity = :quantity, Description = :description WHERE id_allCards = :id_allCards;"<<getQuantity().toString();
        query->bindValue(":id_allCards", getCardInfo()->getId().toString());
        query->exec();
        delete query;
    }
}

void StoreCard::sell(int pNumber, QString pDiscount){
    dataBase->db->transaction();
    setQuantity(getQuantity().toInt() - pNumber);
    upClassToDb();
    std::auto_ptr <QSqlQuery> query( new QSqlQuery(*dataBase->db) );
    query->prepare(" INSERT INTO soldCards( id_allCards, priceNet, vat,discount,quantity) VALUES( :id_allCards, :priceNet, :vat,:discount,:quantity)");
    pDiscount.replace(",",".");
    query->bindValue(":id_allCards", getCardInfo()->getId().toString());
    QVariant priceNetForAll(getCardInfo()->getPriceNet().toFloat() *  pNumber - pDiscount.toFloat()*pNumber* getCardInfo()->getPriceNet().toFloat() );
    query->bindValue(":priceNet",  priceNetForAll.toString());
    query->bindValue(":vat", getCardInfo()->getVat()->getValue().toString());
    query->bindValue(":discount", pDiscount);
    query->bindValue(":quantity", pNumber);
    query->exec();
    dataBase->db->commit();
}
