#include "downloadmanage.h"
#include <QDateTime>
#include <QDir>

DownloadManager::DownloadManager(QObject *parent) :
    QObject(parent)
{
    manager = new QNetworkAccessManager;
}

DownloadManager::~DownloadManager()
{
    manager->deleteLater();
}

void DownloadManager::setFile(QString fileURL, int pFlag)
{
    QString filePath = fileURL;
    QStringList filePathList = filePath.split('/');
//    QString fileName = filePathList.at(filePathList.count() - 1);

    if(DownloadManager::eXmlFile)
        mFileName = QDateTime().currentDateTime().toString("dd.MM.yyyy hh:mm");
//    saveFilePath = QString( fileName );

    QNetworkRequest request;
    request.setUrl(QUrl(fileURL));

    if(DownloadManager::eXmlFile)
    {
        QDir dir(qApp->applicationDirPath().replace(QString("/"),QString("\\"))+"\\device\\xml\\");
        if (!dir.exists()) {
            dir.mkpath(qApp->applicationDirPath().replace(QString("/"),QString("\\"))+"\\device\\xml\\");
        }
        file = new QFile(qApp->applicationDirPath().replace(QString("/"),QString("\\"))+"\\device\\xml\\"+ mFileName + ".xml" );//FilePath;
    }
    if(DownloadManager::ePicFile)
    {
        QString lEditionName = mFileName.left(3);
        mFileName = qApp->applicationDirPath().replace(QString("/"),QString("\\"))+"\\images\\cards\\" + mFileName;
        QDir dir( qApp->applicationDirPath().replace(QString("/"),QString("\\"))+"\\images\\cards\\" + lEditionName);
        if (!dir.exists()) {
            qDebug()<<dir.mkpath( qApp->applicationDirPath().replace(QString("/"),QString("\\"))+"\\images\\cards\\" + lEditionName);
        }
        file = new QFile(mFileName + ".jpg" );//FilePath;

    }
   reply = manager->get(request);
//    qDebug() <<file->symLinkTarget();

    qDebug() << "JAK OTWARCIE? : "<< file->open(QIODevice::WriteOnly);

    connect(reply,SIGNAL(downloadProgress(qint64,qint64)),this,SLOT(onDownloadProgress(qint64,qint64)));
    connect(manager,SIGNAL(finished(QNetworkReply*)),this,SLOT(onFinished(QNetworkReply*)));
    connect(reply,SIGNAL(readyRead()),this,SLOT(onReadyRead()));
    connect(reply,SIGNAL(finished()),this,SLOT(onReplyFinished()));
}

void DownloadManager::onDownloadProgress(qint64 bytesRead,qint64 bytesTotal)
{
    qDebug(QString::number(bytesRead).toLatin1() +" - "+ QString::number(bytesTotal).toLatin1());
}

void DownloadManager::onFinished(QNetworkReply * reply)
{
    switch(reply->error())
    {
        case QNetworkReply::NoError:
        {
            qDebug("file is downloaded successfully.");

        }break;
        default:{
            qDebug(reply->errorString().toLatin1());
        };
    }

    if(file->isOpen())
    {
        file->close();
        file->deleteLater();
    }
    emit overDownload(file->fileName());
}

void DownloadManager::onReadyRead()
{
    file->write(reply->readAll());
}

void DownloadManager::onReplyFinished()
{
    if(file->isOpen())
    {
        file->close();
        file->deleteLater();
    }
}
