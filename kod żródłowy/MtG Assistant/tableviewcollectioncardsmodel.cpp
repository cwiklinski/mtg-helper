#include "tableviewcollectioncardsmodel.h"
#include <QtGui>
#include <QCheckBox>
TableViewCollectionCardsModel::TableViewCollectionCardsModel(QObject *parent)  : QSqlQueryModel(parent)
{

}
void TableViewCollectionCardsModel::refresh(Database &database){
    setQuery("SELECT allCards.name,"
             "editions.name,"
             "editions.longname, "
             "collectionCards.quantity, "
             "collectionList.name, "
             "allCards.priceNet*collectionCards.quantity, "
             "ROUND((allCards.priceNet+allCards.priceNet*(SELECT value FROM vat WHERE id=1)/100.0)*collectionCards.quantity, 2), "
             "ROUND(allCards.priceNet*collectionCards.quantity, 2),"
             "ROUND(collectionCards.quantity*(allCards.priceNet+allCards.priceNet*(SELECT value FROM vat WHERE id=1)/100.0) ,2), "
             "ROUND(collectionCards.discount,2),"
             "allCards.manacost,"
             "rarity.name,"
             "allCards.type, "
             "allCards.pt,"
             "allCards.text,"
             "collectionCards.id "
             "FROM collectionCards LEFT JOIN collectionList ON collectionCards.id_collectionList = collectionList.id LEFT JOIN allCards ON collectionCards.id_allCards = allCards.id    LEFT JOIN editions ON allCards.id_edition = editions.id  LEFT JOIN rarity ON allCards.id_rarity=rarity.id;", *database.db);
    while (canFetchMore()) fetchMore();
}




int	TableViewCollectionCardsModel::columnCount(const QModelIndex & index) const
{
    return 17;
}



