#include "dialogeksporttocsv.h"
#include "ui_dialogeksporttocsv.h"
#include <QSqlRecord>
#include "listcollection.h"
#include <memory>
#include "tableviewsoldcards.h"

DialogEksportToCsv::DialogEksportToCsv(Database &dataBaseObj, QWidget *parent) :
    QDialog(parent), dataBase(&dataBaseObj),
    ui(new Ui::DialogEksportToCsv)
{
    ui->setupUi(this);
    setWindowTitle("Export to csv");
    std::unique_ptr<ListCollection> listBoxes(new ListCollection(*dataBase));
    ui->comboNameBox->addItems(listBoxes->getCollectionList());

}

DialogEksportToCsv::~DialogEksportToCsv()
{
    delete ui;
}


void DialogEksportToCsv::on_buttonBox_accepted()
{


    QString filename = QFileDialog::getSaveFileName(this, "Save as:", "filename.csv", "CSV files (*.csv)", 0, 0); // getting the filename (full path)
    QFile data(filename);
    if(data.open(QFile::WriteOnly |QFile::Truncate))
    {
        QTextStream output(&data);
        if(ui->checkAllCards->isChecked())
        {
           createCsvAllCards(output);

        }

        if(ui->checkCardsInShop->isChecked())
        {
           createCsvShopCards(output);

        }

        if(ui->checkSoldCards->isChecked())
        {
           createCsvSoldCards(output);

        }

        if(ui->checkCardsInBox->isChecked())
        {
            qDebug() <<  ui->comboNameBox->currentText();
           createCsvCardsInBox(output, ui->comboNameBox->currentText() );

        }




    }
}


void DialogEksportToCsv::createCsvAllCards(QTextStream &output)
{
    TabelViewStoreCards *tableViewShopCards = new TabelViewStoreCards();
    tableViewShopCards->refresh(*dataBase);
    for(int i = 0 ; i < tableViewShopCards->rowCount() ; i++){
        for(int j = 0; j<8/*tableViewShopCards->columnCount()*/; j++){
            output <<  tableViewShopCards->record(i).value(j).toString();
            if(j ==(/*tableViewShopCards->columnCount()*/8 -1)  )
                output <<"\n";
            else
                output <<";";

        }
    }

}

void DialogEksportToCsv::createCsvShopCards(QTextStream &output)
{
    TabelViewStoreCards *tableViewShopCards = new TabelViewStoreCards();
    tableViewShopCards->refresh(*dataBase);
    for(int i = 0 ; i < tableViewShopCards->rowCount() ; i++){
        if(tableViewShopCards->record(i).value(4).toInt() > 0 ){
            for(int j = 0; j<8/*tableViewShopCards->columnCount()*/; j++){
                output <<  tableViewShopCards->record(i).value(j).toString();
                if(j ==(/*tableViewShopCards->columnCount()*/8 -1)  )
                    output <<"\n";
                else
                    output <<";";
            }
        }
    }

}


void DialogEksportToCsv::createCsvSoldCards(QTextStream &output)
{

    std::unique_ptr<TableViewSoldCards> tableViewSoldCards ( new TableViewSoldCards());
    tableViewSoldCards->refresh(*dataBase);
    for(int i = 0 ; i < tableViewSoldCards->rowCount() ; i++){
            for(int j = 0; j< tableViewSoldCards->columnCount(); j++){
                output << tableViewSoldCards->record(i).value(j).toString();
                if(j ==( tableViewSoldCards->columnCount() -1)  )
                    output <<"\n";
                else
                    output <<";";
            }
    }
}



void DialogEksportToCsv::createCsvCardsInBox(QTextStream &output, QString nameCollection)
{

    std::unique_ptr< TableViewCollectionCardsModel> tableViewBoxCardsModel ( new  TableViewCollectionCardsModel());
    tableViewBoxCardsModel->refresh(*dataBase);
    for(int i = 0 ; i < tableViewBoxCardsModel->rowCount() ; i++){
        qDebug() << "por z "<< tableViewBoxCardsModel->record(i).value(3).toString();
        if( tableViewBoxCardsModel->record(i).value(4).toString() == nameCollection){
            for(int j = 0; j< 10/* tableViewBoxCardsModel->columnCount()*/; j++){
                output << tableViewBoxCardsModel->record(i).value(j).toString();
                if(j ==( 10/*tableViewBoxCardsModel->columnCount() */-1)  )
                    output <<"\n";
                else
                    output <<";";

            }
        }
    }
}

void DialogEksportToCsv::on_checkSoldCards_clicked(bool checked)
{
    ui->checkAllCards->setChecked(false);
    ui->checkCardsInBox->setChecked(false);
    ui->checkCardsInShop->setChecked(false);
}

void DialogEksportToCsv::on_checkCardsInShop_clicked(bool checked)
{
    ui->checkAllCards->setChecked(false);
    ui->checkCardsInBox->setChecked(false);
    ui->checkSoldCards->setChecked(false);
}

void DialogEksportToCsv::on_checkAllCards_clicked(bool checked)
{
    ui->checkCardsInBox->setChecked(false);
    ui->checkCardsInShop->setChecked(false);
    ui->checkSoldCards->setChecked(false);
}

void DialogEksportToCsv::on_checkCardsInBox_clicked(bool checked)
{
    ui->checkAllCards->setChecked(false);
    ui->checkCardsInShop->setChecked(false);
    ui->checkSoldCards->setChecked(false);
}
