#include "proxymodelsoldcards.h"
#include <QDebug>
ProxyModelSoldCards::ProxyModelSoldCards(QObject *parent) :
     QSortFilterProxyModel(parent)
{
}
bool ProxyModelSoldCards::filterAcceptsRow(int sourceRow, const QModelIndex &sourceParent) const
{

    QModelIndex index9 = sourceModel()->index(sourceRow, 9, sourceParent);

//    if(!checkW){
//        if((sourceModel()->data(index10).toString().contains(QString("W"))))
//            return  false;
//    }  if(!checkG){
//        if((sourceModel()->data(index10).toString().contains(QString("G"))))
//            return false;
//    } if(!checkU){
//        if((sourceModel()->data(index10).toString().contains(QString("U"))))
//            return  false;
//    } if(!checkR){
//        if((sourceModel()->data(index10).toString().contains(QString("R"))))
//            return  false;
//    } if(!checkB){
//        if((sourceModel()->data(index10).toString().contains(QString("B"))))
//            return false;
//    }  if(!checkArtifact){
//        if((sourceModel()->data(index9).toString().contains(QString("Artifact"))))
//            return  false;
//    }if(!checkLand)
//        if((sourceModel()->data(index9).toString().contains(QString("Land"))))
//            return false;
//    if(checkCardInShop)
//        if((sourceModel()->data(index4).toInt() == 0 ))
//            return false;
//    if(!(rarity==""))
//        if((sourceModel()->data(index8).toString().compare(rarity)))
//            return false;
//    if(!(editionName==""))
//        if((sourceModel()->data(index2).toString().compare(editionName)))
//        { return false;}
//    if(!(editionLongname==""))
//        if((sourceModel()->data(index3).toString().compare(editionLongname)))
//        { return false;}
    qDebug() << "filtruje jest a accept row";

    return ( dateInRange( sourceModel()->data(index9).toDate()) );

}


QVariant  ProxyModelSoldCards::headerData(int section, Qt::Orientation orientation, int role) const
{
    if (role != Qt::DisplayRole)
        return QVariant();

    if (orientation == Qt::Horizontal) {
        switch (section) {
        case 0:
            return tr("Name");
        case 1:
            return tr("Edition");
        case 2:
            return tr("Quantity");
        case 3:
            return tr("Vat");
        case 4:
            return tr("Price net");
        case 5:
            return tr("Price gross");
        case 6:
            return tr("Discount");
        case 7:
            return tr("Discount net");
        case 8:
            return tr("Discount gross ");
        case 9:
            return tr("Date");



        default:
            return QVariant();
        }
    }
    return QVariant();
}

bool ProxyModelSoldCards::dateInRange(const QDate &date) const
{
    return (!minDate.isValid() || date >= minDate)
           && (!maxDate.isValid() || date <= maxDate);
}


void ProxyModelSoldCards::setFilterMinimumDate(const QDate &date)
 {
     minDate = date;
     invalidateFilter();
 }

 void ProxyModelSoldCards::setFilterMaximumDate(const QDate &date)
 {
     maxDate = date;
     invalidateFilter();
 }
