#ifndef LISTRARITY_H
#define LISTRARITY_H
#include "listrarity.h"
#include "rarity.h"

/**
 * @brief
 * Klasa zawiera liste rarity
 */
class ListRarity
{
public:
    /**
     * @brief
     * Kontruktor wypełniajacy klase rarity
     * @param objDatabase
     */
    ListRarity(Database &objDatabase);
    /**
     * @brief
     * Odświeża liste edycji
     * @return bool
     */
    bool refreshRarityList();
    /**
     * @brief
     *
     * @return QStringList Zwraac liste rarity
     */
    QStringList getRarityList();
    /**
     * @brief
     *
     * @param id id z tabeli rarity
     * @return Rarity zwraca obiekt Rarity
     */
    Rarity& getRarity(QVariant id){return listRarity[id.toInt()-1];}

private:
    int size; /***/
    Rarity *listRarity; /***/
    Database *dataBase; /***/
};

#endif // LISTRARITY_H
