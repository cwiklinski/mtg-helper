#include "dialogaddnewcardtocollection.h"
#include "ui_dialogaddnewcardtocollection.h"

DialogAddNewCardToCollection::DialogAddNewCardToCollection(Database &dataBaseObj, QWidget *parent) :
    QDialog(parent), dataBase(&dataBaseObj),
    ui(new Ui::DialogAddNewCardToCollection)
{

    ui->setupUi(this);
    setWindowTitle("Add new card(s)");
    ListCollection *listBoxes = new ListCollection(*dataBase);
    QStringList listBoxString = (listBoxes->getCollectionList());
    ui->comboBoxSelectBox->addItems(listBoxString);
    model = new  TabelViewStoreCards();
    model->refresh(*dataBase);
    proxyModel = new  ProxyModelStoreCards(this);
    proxyModel ->setSourceModel(model);
    proxyModel ->setDynamicSortFilter(true);

    ui->tableView->setModel(proxyModel);
    ui-> tableView->setSortingEnabled(true);
    ui->tableView->setSelectionBehavior(QAbstractItemView::SelectRows);
    ui->tableView->horizontalHeader()->setStretchLastSection(true);
    ui->tableView->verticalHeader()->hide();
    ui->tableView->setEditTriggers(QAbstractItemView::NoEditTriggers);
    ui->tableView->setSelectionMode(QAbstractItemView::SingleSelection);
    ui->tableView->horizontalHeader()->setHighlightSections(false);

    ui->tableView->hideColumn(0);
    ui->tableView->hideColumn(2);
    ui->tableView->hideColumn(4);
    ui->tableView->hideColumn(5);
    ui->tableView->hideColumn(6);
    ui->tableView->hideColumn(7);
    ui->tableView->hideColumn(8);
    ui->tableView->hideColumn(9);
    ui->tableView->hideColumn(10);
    ui->tableView->hideColumn(11);

    connect(ui->lineEditLookingFor, SIGNAL(textChanged(QString)),
            this, SLOT(nameFilterChanged() ));
}

DialogAddNewCardToCollection::~DialogAddNewCardToCollection()
{
    delete ui;
}

void DialogAddNewCardToCollection::on_buttFinish_clicked()
{
    close();
}

void DialogAddNewCardToCollection::on_buttAddCard_clicked()
{
    QModelIndex index = ui->tableView->currentIndex();
    QModelIndex index1(proxyModel->mapToSource(index));
    CollectionCard *card = new CollectionCard(*dataBase);

    ListCollection *temp = new ListCollection(*dataBase);

    card->setCardInfo(index1.row()+1);
    card->setCollection( temp->getIdCollection( ui->comboBoxSelectBox->currentText()));
    card->setQuantity(ui->spinBoxQuantity->value());
    card->insertToCollectionTbl();
    delete temp;
    delete card;
    emit dataChanged();
    QMessageBox msgBox(this);
    msgBox.setText("Card(s) succesfully added.");
    msgBox.exec();

}

void DialogAddNewCardToCollection::nameFilterChanged()
{
    QRegExp regExp(ui->lineEditLookingFor->text().toUpper(),  Qt::CaseSensitive , QRegExp::RegExp);
    proxyModel->setFilterRegExp(regExp);

}
