var searchData=
[
  ['rarity',['rarity',['../class_card_info.html#a5130b800eb67f11485256b9f4f3e9548',1,'CardInfo::rarity()'],['../class_proxy_model_collection_cards.html#afdaeab59cdc6eed8cba8deb81d79ecd7',1,'ProxyModelCollectionCards::rarity()'],['../class_proxy_model_store_cards.html#ac5e72e2638fcc00f20b35ddc474e93a4',1,'ProxyModelStoreCards::rarity()']]],
  ['rarityid',['RarityID',['../class_rarity.html#a891f8bf41abb19db18a86cc127271ed3',1,'Rarity']]],
  ['rarityname',['RarityName',['../class_rarity.html#a5a033bdb7817c9323cc639230964a765',1,'Rarity']]],
  ['reply',['reply',['../class_download_manager.html#a08a1f38588d7c7e5a94962be96aa338b',1,'DownloadManager']]],
  ['root',['root',['../class_parser_xml_from_device.html#ad9c71dca67cd9b26b48b98d5a9649e70',1,'ParserXmlFromDevice']]]
];
