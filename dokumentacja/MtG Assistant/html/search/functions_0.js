var searchData=
[
  ['cardinfo',['CardInfo',['../class_card_info.html#a28a04a294b4bef9e7287cb2e11fab9e1',1,'CardInfo::CardInfo()'],['../class_card_info.html#ae2343f2e70794e94db202b49d4ff5f14',1,'CardInfo::CardInfo(QString name, QString edition, Database &amp;objDataBase, int flag=Edition::EditionID)'],['../class_card_info.html#acf1bd3774b09248ba5dbf5a7dd0eb2da',1,'CardInfo::CardInfo(int id, Database &amp;objDataBase)'],['../class_card_info.html#abfdfe6aa9d8bed331bbe10a48261bece',1,'CardInfo::CardInfo(QString id, Database &amp;objDataBase)'],['../class_card_info.html#a067608970c0a06cebe97f126036370c7',1,'CardInfo::CardInfo(QVariant id, Database &amp;objDataBase)']]],
  ['cardsfromxmparsing',['CardsFromXmParsing',['../class_cards_from_xm_parsing.html#a22346da4c2fa369c0651cebd5dbfa032',1,'CardsFromXmParsing']]],
  ['choosequery',['chooseQuery',['../class_edition.html#ae06b09454dc07cddaf2300125e87a6ea',1,'Edition::chooseQuery()'],['../class_rarity.html#ad2e0fac662daa2c159499e059e466f90',1,'Rarity::chooseQuery()'],['../class_store_card.html#a0d1b4ce6f066c78793f6f0155d89746a',1,'StoreCard::chooseQuery()'],['../class_vat.html#a7bd1725ba6e51f3abbf3a08ba71f19cc',1,'Vat::chooseQuery()']]],
  ['cleartable',['clearTable',['../class_table_view_device_cards.html#af070f4494785b65e731a2c4c48b720c4',1,'TableViewDeviceCards']]],
  ['closedb',['closeDB',['../class_database.html#a7f214d72a76d60a3f78cf784cc32aa5d',1,'Database']]],
  ['collection',['Collection',['../class_collection.html#a1fda66d6a2ff4fcecc74fba63abbf52f',1,'Collection::Collection()'],['../class_collection.html#afacfd36eaef4605b5e2e6c070b52340e',1,'Collection::Collection(Database &amp;pDatabase)'],['../class_collection.html#ae5fdb561d538d884fe38880152c7b5e5',1,'Collection::Collection(QVariant id, Database &amp;pDatabase)']]],
  ['collectioncard',['CollectionCard',['../class_collection_card.html#a55c08039f5d8ecbcdc017bc58c666eeb',1,'CollectionCard::CollectionCard()'],['../class_collection_card.html#a44f4f19a2c1fe1cb3fd8552243056003',1,'CollectionCard::CollectionCard(Database &amp;_dataBase)'],['../class_collection_card.html#a2e849d225556859e212c2f499b2e2c97',1,'CollectionCard::CollectionCard(QVariant id, Database &amp;_dataBase)']]],
  ['columncount',['columnCount',['../class_table_view_collection_cards_model.html#a5a5eb87bc7fd4ead5d5393788f9907d5',1,'TableViewCollectionCardsModel::columnCount()'],['../class_table_view_device_cards.html#a8bb36e393877492a21ec045bd6ad81a1',1,'TableViewDeviceCards::columnCount()']]],
  ['columnfilter',['ColumnFilter',['../class_column_filter.html#ae4267478f34cb739b0021939f96c1f4e',1,'ColumnFilter']]],
  ['connect',['connect',['../class_socket.html#ab98ed5ea1c4ac7b92d97f393562e486e',1,'Socket']]],
  ['createcsvallcards',['createCsvAllCards',['../class_dialog_eksport_to_csv.html#a35ae2176afa6a263712a4572c2d69dfe',1,'DialogEksportToCsv']]],
  ['createcsvcardsinbox',['createCsvCardsInBox',['../class_dialog_eksport_to_csv.html#a60b1db771af514b6b801f1af47d36d12',1,'DialogEksportToCsv']]],
  ['createcsvshopcards',['createCsvShopCards',['../class_dialog_eksport_to_csv.html#a958f7f56587596be87459e4625e700ee',1,'DialogEksportToCsv']]],
  ['createcsvsoldcards',['createCsvSoldCards',['../class_dialog_eksport_to_csv.html#a8fb21d8eac1d091e7b62220b49935fe0',1,'DialogEksportToCsv']]]
];
