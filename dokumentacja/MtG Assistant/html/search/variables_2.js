var searchData=
[
  ['edition',['edition',['../class_card_info.html#ad046f7dfa6f7be29e5874dcd0f06088a',1,'CardInfo::edition()'],['../class_cards_from_xm_parsing.html#a1df8f14f2a7164e9b86b013d81580bb3',1,'CardsFromXmParsing::edition()']]],
  ['editionid',['EditionID',['../class_edition.html#a247bd617939b0858a3be650260177a75',1,'Edition']]],
  ['editionlongname',['EditionLongName',['../class_edition.html#affc45a89ab67fe22a9babfecdf9dfd08',1,'Edition::EditionLongName()'],['../class_proxy_model_collection_cards.html#a2a92022c27505f7f86112df56abf03a2',1,'ProxyModelCollectionCards::editionLongname()'],['../class_proxy_model_store_cards.html#a7c6f59790dbad8cb4281f764ee9beb7a',1,'ProxyModelStoreCards::editionLongname()']]],
  ['editionname',['EditionName',['../class_edition.html#a66cc74bfa4f1ea93caab69949fd1deaa',1,'Edition::EditionName()'],['../class_proxy_model_collection_cards.html#a3fd3ebcd0f55e4823123d8c9cb673b09',1,'ProxyModelCollectionCards::editionName()'],['../class_proxy_model_store_cards.html#ab4a561daca3ba4f090c162e4eacd5c46',1,'ProxyModelStoreCards::editionName()']]]
];
