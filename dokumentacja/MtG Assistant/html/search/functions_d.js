var searchData=
[
  ['rarity',['Rarity',['../class_rarity.html#a8867da6d88ab73e9080d0d158921578a',1,'Rarity::Rarity()'],['../class_rarity.html#a5338244f6e67dc5f5db7379d4c35953b',1,'Rarity::Rarity(QString value, Database &amp;objDataBase, int flag=RarityID)'],['../class_rarity.html#ad1529f70ac92067d3b24735d1cd8097f',1,'Rarity::Rarity(QVariant value, Database &amp;objDataBase, int flag=RarityID)'],['../class_rarity.html#a2c046cc07926cfca05a783ecfc688300',1,'Rarity::Rarity(int id, Database &amp;objDataBase)']]],
  ['readdata',['readData',['../class_parser_xml_from_device.html#a3dd98325432507d225e58d3e6b960d8f',1,'ParserXmlFromDevice']]],
  ['readdatacards',['readDataCards',['../class_parser_xml_from_device.html#a0d61b92bc0b37f46b67f331fa2d8f2fd',1,'ParserXmlFromDevice']]],
  ['readfile',['readFile',['../class_parser_xml_from_device.html#a8cd263ad644defcd3a9bb5321791a657',1,'ParserXmlFromDevice']]],
  ['readingdocroot',['readingDocRoot',['../class_parser_xml_from_device.html#aaed3c04594c28285a3cb5623de03e7cb',1,'ParserXmlFromDevice']]],
  ['readlinktoxml',['readLinkToXml',['../class_socket.html#a13d2bf60f7c09fcd3e3214de7bd3f8e2',1,'Socket']]],
  ['refresh',['refresh',['../class_tabel_view_store_cards.html#a45294b4a7a4d91fa3afccd5820fffa4b',1,'TabelViewStoreCards::refresh()'],['../class_table_view_collection_cards_model.html#a99544efaea57bdb553eb4ad788f30809',1,'TableViewCollectionCardsModel::refresh()'],['../class_table_view_sold_cards.html#a94855ffb84464be39c5d256ed1a010d8',1,'TableViewSoldCards::refresh()']]],
  ['refresheditionlist',['refreshEditionList',['../class_list_editions.html#ae61c61b862e0b362edb2e4d6ae2cb5cf',1,'ListEditions']]],
  ['refreshlist',['refreshList',['../class_list_collection.html#a6a5f305194844b1b6369e5ef0b26bff2',1,'ListCollection']]],
  ['refreshraritylist',['refreshRarityList',['../class_list_rarity.html#a5f0333bc6c02c2b3eb95ccc9fd84e36e',1,'ListRarity']]],
  ['replyfinishedforstart',['replyFinishedForStart',['../class_dialog_device.html#a7b15abdd8d34a37b6693a84163ece577',1,'DialogDevice']]],
  ['returndata',['returnData',['../class_parser_xml_from_device.html#a5a689e77da65a690d30a6b4fc377fc85',1,'ParserXmlFromDevice']]],
  ['rowcount',['rowCount',['../class_table_view_device_cards.html#ab99c8530bff0d72465ef381c4e5255c9',1,'TableViewDeviceCards']]]
];
