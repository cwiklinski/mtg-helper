var searchData=
[
  ['maddresstodevice',['mAddressToDevice',['../class_dialog_device.html#af08dcdc6567f6a4d58a090c4e939653d',1,'DialogDevice']]],
  ['manacost',['manacost',['../class_card_info.html#a9ffdaec284aa4672b6518ac6ee334a67',1,'CardInfo']]],
  ['manager',['manager',['../class_dialog_device.html#af2b64f244ad8e7d01780311474bc3926',1,'DialogDevice::manager()'],['../class_download_manager.html#a1fc1e91eb0f00f7b5a6fb625f7df6be6',1,'DownloadManager::manager()']]],
  ['maxdate',['maxDate',['../class_proxy_model_sold_cards.html#aa38f9a1aea0b6fd0979f28464db02fe8',1,'ProxyModelSoldCards']]],
  ['mdiscount',['mdiscount',['../class_collection_card.html#a8dd6c612c0cb65dc01bdbfaf0ec266c1',1,'CollectionCard']]],
  ['mfilename',['mFileName',['../class_download_manager.html#a763ae944a3ca45bf864866717c3d8ab5',1,'DownloadManager']]],
  ['mindate',['minDate',['../class_proxy_model_sold_cards.html#a190dfb978c11ce9a168987c778c18cc4',1,'ProxyModelSoldCards']]],
  ['model',['model',['../class_dialog_add_new_card_to_collection.html#adc2e152a075788bff29a2b8ba836b1ea',1,'DialogAddNewCardToCollection']]],
  ['modelcollection',['modelCollection',['../class_main_window.html#abdada424d6e19d1f1bebd6e0644516f7',1,'MainWindow']]],
  ['modelstore',['modelStore',['../class_main_window.html#a14c821b088cb7dfac7d5ae2ebadf630f',1,'MainWindow']]],
  ['mtableviewdevicecards',['mTableViewDeviceCards',['../class_dialog_device.html#a644bf5bd8324d302a993396fd22096fc',1,'DialogDevice']]]
];
