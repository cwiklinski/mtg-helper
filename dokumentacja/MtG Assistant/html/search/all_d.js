var searchData=
[
  ['rarity',['Rarity',['../class_rarity.html',1,'Rarity'],['../class_rarity.html#a8867da6d88ab73e9080d0d158921578a',1,'Rarity::Rarity()'],['../class_rarity.html#a5338244f6e67dc5f5db7379d4c35953b',1,'Rarity::Rarity(QString value, Database &amp;objDataBase, int flag=RarityID)'],['../class_rarity.html#ad1529f70ac92067d3b24735d1cd8097f',1,'Rarity::Rarity(QVariant value, Database &amp;objDataBase, int flag=RarityID)'],['../class_rarity.html#a2c046cc07926cfca05a783ecfc688300',1,'Rarity::Rarity(int id, Database &amp;objDataBase)'],['../class_card_info.html#a5130b800eb67f11485256b9f4f3e9548',1,'CardInfo::rarity()'],['../class_proxy_model_collection_cards.html#afdaeab59cdc6eed8cba8deb81d79ecd7',1,'ProxyModelCollectionCards::rarity()'],['../class_proxy_model_store_cards.html#ac5e72e2638fcc00f20b35ddc474e93a4',1,'ProxyModelStoreCards::rarity()']]],
  ['rarity_2ecpp',['rarity.cpp',['../rarity_8cpp.html',1,'']]],
  ['rarity_2eh',['rarity.h',['../rarity_8h.html',1,'']]],
  ['rarityid',['RarityID',['../class_rarity.html#a891f8bf41abb19db18a86cc127271ed3',1,'Rarity']]],
  ['rarityname',['RarityName',['../class_rarity.html#a5a033bdb7817c9323cc639230964a765',1,'Rarity']]],
  ['readdata',['readData',['../class_parser_xml_from_device.html#a3dd98325432507d225e58d3e6b960d8f',1,'ParserXmlFromDevice']]],
  ['readdatacards',['readDataCards',['../class_parser_xml_from_device.html#a0d61b92bc0b37f46b67f331fa2d8f2fd',1,'ParserXmlFromDevice']]],
  ['readfile',['readFile',['../class_parser_xml_from_device.html#a8cd263ad644defcd3a9bb5321791a657',1,'ParserXmlFromDevice']]],
  ['readingdocroot',['readingDocRoot',['../class_parser_xml_from_device.html#aaed3c04594c28285a3cb5623de03e7cb',1,'ParserXmlFromDevice']]],
  ['readlinktoxml',['readLinkToXml',['../class_socket.html#a13d2bf60f7c09fcd3e3214de7bd3f8e2',1,'Socket']]],
  ['refresh',['refresh',['../class_tabel_view_store_cards.html#a45294b4a7a4d91fa3afccd5820fffa4b',1,'TabelViewStoreCards::refresh()'],['../class_table_view_collection_cards_model.html#a99544efaea57bdb553eb4ad788f30809',1,'TableViewCollectionCardsModel::refresh()'],['../class_table_view_sold_cards.html#a94855ffb84464be39c5d256ed1a010d8',1,'TableViewSoldCards::refresh()']]],
  ['refresheditionlist',['refreshEditionList',['../class_list_editions.html#ae61c61b862e0b362edb2e4d6ae2cb5cf',1,'ListEditions']]],
  ['refreshlist',['refreshList',['../class_list_collection.html#a6a5f305194844b1b6369e5ef0b26bff2',1,'ListCollection']]],
  ['refreshraritylist',['refreshRarityList',['../class_list_rarity.html#a5f0333bc6c02c2b3eb95ccc9fd84e36e',1,'ListRarity']]],
  ['reply',['reply',['../class_download_manager.html#a08a1f38588d7c7e5a94962be96aa338b',1,'DownloadManager']]],
  ['replyfinishedforstart',['replyFinishedForStart',['../class_dialog_device.html#a7b15abdd8d34a37b6693a84163ece577',1,'DialogDevice']]],
  ['returndata',['returnData',['../class_parser_xml_from_device.html#a5a689e77da65a690d30a6b4fc377fc85',1,'ParserXmlFromDevice']]],
  ['root',['root',['../class_parser_xml_from_device.html#ad9c71dca67cd9b26b48b98d5a9649e70',1,'ParserXmlFromDevice']]],
  ['rowcount',['rowCount',['../class_table_view_device_cards.html#ab99c8530bff0d72465ef381c4e5255c9',1,'TableViewDeviceCards']]]
];
